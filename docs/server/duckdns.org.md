---
tags:
  - Servers
  - Domain Names
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# duckdns.org

DuckDNS is a free and open source service which will point a DNS (sub domains of duckdns.org) to an
IP of your choice.


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Get a domain and configure your internet router](#get-a-domain-and-configure-your-internet-router)
* [Cron script to update your IP associated to your duckdns subdomain](#cron-script-to-update-your-ip-associated-to-your-duckdns-subdomain)

<!-- vim-markdown-toc -->

---
## Get a domain and configure your internet router

Create a free domain on [duckdns.org](https://www.duckdns.org/) <br/> E.g. get the subdomain
`yourhostname.duckdns.org`.

Configure your internet router to get your server a fix public IP address and to forward port 80
and port 443:

* <https://www.wikihow.com/Open-Your-Port-80-Behind-a-Firewall>


---
## Cron script to update your IP associated to your duckdns subdomain

You might not have a fixed IP address. In this case, duckdns will need to be updated whenever your
IP changes, e.g. with the following script:
```console
$ vi ~/bin/update-duckdns-ip
    > #!/bin/bash
    >
    > valid_ifconfig() {
    >     ip=$1
    >
    >     if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    >         return 0 # valid ifconfig response
    >     else
    >         return 1
    >     fi
    > }
    >
    > curl_ipv4=$(curl -4 -s --connect-timeout 5 "ifconfig.co")
    > curl_ipv6=$(curl -6 -s --connect-timeout 5 "ifconfig.co")
    >
    > if valid_ifconfig "$curl_ipv4" && ! curl -s --connect-timeout 5 https://yourhostname.duckdns.org
    > then
    >     # In the below command, replace "{token}" with your duckdns token (see duckdns.org),
    >     # and "yourhostname" with your duckdns domain name:
    >     curl "https://www.duckdns.org/update?domains=yourhostname&token={token}&ip=$curl_ipv4&ipv6=$curl_ipv6&verbose=true"
    > fi
```

Run the above script in your `crontab` e.g. every 5 minutes:
```console
# sudo crontab -e
    > ...
    > */5 * * * * /home/user/bin/update-duckdns-ip # run update-duckdns-ip every 5 mn
    > ...
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
