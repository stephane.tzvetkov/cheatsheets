---
tags:
  - Games
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Controllers

TODO : this cheat sheet is only focused on Gentoo and should be moved to the `gentoo-based`
directory?

For Xbox One, PS 2 - 3 - 4 and Steam controllers.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Sony_DualShock>
    * <https://forums.gentoo.org/viewtopic-t-1090688-start-0.html>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Config


!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig
        
            # General controllers options
            #############################
            # Double check here:
            # <https://wiki.gentoo.org/wiki/Sony_DualShock#Kernel>
            #
            # Device Drivers --->
            #       Input device support --->
            #         <*>   Joystick interface # Symbol: INPUT_JOYDEV [=y]
            #         <*>   Event interface # Symbol: INPUT_EVDEV [=y]
            #       HID support --->
            #         [*]   Battery level reporting for HID devices # Symbol: HID_BATTERY_STRENGTH [=y]
            #         [*]   /dev/hidraw raw HID device support # Symbol: HIDRAW [=y]
            #         <*>   Generic HID driver # Symbol: HID_GENERIC [=y]
            #             USB HID support --->
            #               <*> USB HID transport layer # Symbol: USB_HID [=y]
            #   [*] LED Support ---> # Symbol: NEW_LEDS [=y]
            #         <*>   LED Class Support # Symbol: LEDS_CLASS [=y]
        
            # Specific Steam controller options
            # #################################
            #
            # Device Drivers --->
            #       HID support --->
            #         <*>   Generic HID driver # Symbol: HID_GENERIC [=y]
            #               Special HID drivers --->
            #                 <*> Steam Controller support # Symbol: HID_STEAM [=y]
        
            # Specific PS2/3/4 controller options
            # ###################################
            #
            # Device Drivers --->
            #       HID support --->
            #         <*>   Generic HID driver # Symbol: HID_GENERIC [=y]
            #               Special HID drivers --->
            #                 <*> Sony PS2/3/4 accessories # Symbol: HID_SONY [=y]
            #                 [*]   Sony PS2/3/4 accessories force feedback support # Symbol: SONY_FF [=y]
        
            # Specific Xbox One controller options
            # ###################################
            #
            # Device Drivers --->
            #       Input device support --->
            #         [*]   Joysticks/Gamepads  ---> # Symbol: INPUT_JOYSTICK [=y]
            #                 <*>   X-Box gamepad support # Symbol: JOYSTICK_XPAD [=y]
            #                 [*]     X-Box gamepad rumble support # Symbol: JOYSTICK_XPAD_FF [=y]
            #       HID support --->
            #         <*>   Generic HID driver # Symbol: HID_GENERIC [=y]
            #               Special HID drivers --->
            #                 <*> Microsoft non-fully HID-compliant devices # Symbol: HID_MICROSOFT [=y]
        
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

* Add your user to the input group:
```console
# gpasswd -a user-name input
```

!!! Note "Note"
    After modifying a user groups, if he is currently logged in, don't forget to logout and log
    back in.


---
## Use

Just run steam.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
