---
tags:
  - SystemD
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `systemd-nspawn`

`systemd-nspawn` is like the `chroot` command, but it is a `chroot` on steroids. It may be used to
run a command or OS in a lightweight namespace container. It is more powerful than `chroot` since
it fully virtualizes the file system hierarchy, as well as the process tree, various subsystems and
the host and domain name.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/systemd-nspawn>
    * <https://wiki.debian.org/nspawn>
    * <https://wiki.gentoo.org/wiki/Chroot#systemd-nspawn>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
