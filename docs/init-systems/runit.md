---
tags:
  - Init Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Runit

Runit is a suite of tools which provides an init (`PID 1`) as well as daemon tools compatible
process supervision framework, along with utilities which streamline creation and maintenance of
services.

???+ Note "Reference(s)"
    * <https://web.archive.org/save/https://medium.com/100-days-of-linux/setting-up-a-functional-runit-service-be75b5bc692c>
    * <https://wiki.artixlinux.org/Main/Runit>
    * <http://smarden.org/runit>
    * <https://web.archive.org/web/20210519043617/https://docs.voidlinux.org/config/services/index.html>
    * <https://docs.voidlinux.org/config/services/index.html>
    * <https://wiki.archlinux.org/title/Runit>
    * <https://github.com/vulk-archive/runit>
    * `$ man sv`
    * `$ man runsv`
    * `$ man runit`
    * `$ man runit-init`
    * `$ man runsvdir`
    * `$ man runsvchdir`


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)
* [Programs](#programs)
* [Files](#files)
* [Services](#services)
    * [Available services](#available-services)
    * [Active services](#active-services)
    * [Basic service related commands](#basic-service-related-commands)
    * [Advanced service related commands](#advanced-service-related-commands)
    * [Create a service](#create-a-service)
* [Run levels](#run-levels)

<!-- vim-markdown-toc -->

---
## TODO

Runit service manager CLI tool, a helper to Runit init system. It can be installed as:
```console
# pacman -S rsm
$ rsm -h
```

---
## Programs

Runit is composed of several programs:

* `sv`: used for controlling services, getting status of services, and dependency checking.

* `chpst`: control of a process environment, including memory caps, limits on cores, data segments,
  environments, user/group privileges, and more.

* `runsv`: supervises a process, and optionally a log service for that process.

* `svlogd`: a simple but powerful logger, includes auto rotation based on different methods (time,
  size, etc), post processing, pattern matching, and socket (remote logging) options. Say goodbye
  to `logrotate` and the need to stop your services to rotate logs.

* `runsvchdir`: changes service levels (run levels, see below)

* `runsvdir`: starts a supervision tree

* `runit-init`: PID 1, does almost nothing besides being the init


---
## Files

There are several files that will be installed by Runit.

* `/etc/runit/1`: stage 1, system’s one-time initialization tasks

* `/etc/runit/2`: stage 2, Normally runs `runsvdir`, should not return until the system is going to
  halt or reboot.

* `/etc/runit/3`: stage 3, system’s shutdown tasks

* `/etc/runit/ctrlaltdel`: Runit will execute this when receiving a `SIGINT` signal

* `/etc/runit/runsvdir/*`: Run levels

* `/etc/runit/sv/*`: directory containing sub directories of available service files

* `/run/runit/service`: always symlinked to active run level, `sv` will search for running service
  here

However, since Runit itself depends on `runit-rc`, there will be several extra `rc` files installed,
most contained in `/etc/rc` and `/usr/lib/rc`.


---
## Services

### Available services

Most distros using Runit won't store *available* services in the same directory. So let's define a
`$AVAILABLE_RUNIT_SERVICE_DIR` environment variable holding the path to that directory. Most common
paths are:

* `/etc/sv` (e.g. for Void Linux)
* `/etc/runit/`
* `/etc/runit/sv` (e.g. for Artix Linux)

### Active services

Most distros using Runit won't store *active* services in the same directory. So let's define a
`$ACTIVE_RUNIT_SERVICE_DIR` environment variable holding the path to that directory. Most common
paths are:

* `/service/`
* `/var/service/` (e.g. for Void Linux)
* `/etc/service/`
* `/run/runit/service/` (e.g. for Artix Linux)


### Basic service related commands

* Start the `service_name` service:
  ```console
  # sv start service_name
  ```
  **or**
  ```console
  # sv up service_name
  ```

* Stop the `service_name` service:
  ```console
  # sv stop service_name
  ```
  **or**
  ```console
  # sv down service_name
  ```

* Restart the `service_name` service:
  ```console
  # sv restart service_name
  ```

* Reload `service_name` configuration without stopping the `service_name` service:
  ```console
  # sv reload service_name
  ```

* Check the `service_name` service status:
  ```console
  # sv status service_name
  ```

* Enable the `service_name` service on system boot:
  ```console
  # ln -s $AVAILABLE_RUNIT_SERVICE_DIR/service_name $ACTIVE_RUNIT_SERVICE_DIR
  ```

* Disable the `service_name` service on system boot:
  ```console
  # unlink $ACTIVE_RUNIT_SERVICE_DIR/service_name
  ```

* Check if the `service_name` service is enable or disable on system boot:
  ```console
  # service="service_name" && if [ -d /run/runit/service/"$service" ] && [ ! -f /run/runit/service/"$service"/down ]; then echo "$service is enable on boot"; else echo "$service is disable on boot"; fi
  ```

* Disable logs of `service_name`
  ```console
   # chmod -x $ACTIVE_RUNIT_SERVICE_DIR/servce_name/log/run
  ```

* Check the last logs of `service_name`:
  ```console
   # tail -f $ACTIVE_RUNIT_SERVICE_DIR/servce_name/log/current
  ```

### Advanced service related commands

* List available services:
  ```console
  # ls $AVAILABLE_RUNIT_SERVICE_DIR
  ```

* Prevent a service from starting at boot (i.e. disable it), while allowing Runit to manage it, by
  creating a file named `down` like so:
  ```console
  # touch $AVAILABLE_RUNIT_SERVICE_DIR/service_name/down
  ```

* Check if `service_name` is working correctly when started by the service supervisor, run it once
  before fully enabling it:
  ```console
    # touch $AVAILABLE_RUNIT_SERVICE_DIR/service_name/down
    # ln -s $AVAILABLE_RUNIT_SERVICE_DIR/service_name $ACTIVE_RUNIT_SERVICE_DIR
    # sv once service_name
  ```
  If everything works, remove the `down` file to enable the service.

### Create a service

* Create the active service directory:
  ```console
  # mkdir $AVAILABLE_RUNIT_SERVICE_DIR/service_name/
  ```

* Create a down file in the service directory, until the setup is completed:
  ```console
  # touch $AVAILABLE_RUNIT_SERVICE_DIR/service_name/down
  ```

* Create a run file in the service directory:
  ```console
  # touch $AVAILABLE_RUNIT_SERVICE_DIR/service_name/run
  # chmod +x /service/service/service_name/run
  # vi /service/service/service_name/run
    > #!/bin/sh
    > exec 2>&1 # redirect output of stderr to stdout
    > exec your-service-executable
  ```

* Enable logging:
  ```console
  # mkdir $AVAILABLE_RUNIT_SERVICE_DIR/service_name/log/
  # touch $AVAILABLE_RUNIT_SERVICE_DIR/service_name/log/run
  # chmod +x $AVAILABLE_RUNIT_SERVICE_DIR/log/run
  $ vi $AVAILABLE_RUNIT_SERVICE_DIR/log/run
    > #!/bin/sh
    > exec svlogd -tt .
  ```

* Enable `service_name` and start it:
  ```console
  # ln -s $AVAILABLE_RUNIT_SERVICE_DIR/service_name $ACTIVE_RUNIT_SERVICE_DIR
  # sv start service_name
  ```

---
## Run levels

By default, Runit has 2 run levels, `default` and `single`.

* Switch run levels (this will stop all services that are currently running and will start all
  services in the new run level):
```console
# runsvchdir runlevel
```

* Make your own run level (e.g. `myrunlvl`) by creating a new folder in `/etc/runit/runsvdir` and
  symlinking your desired service to that run level (⚠️ this example is specific to Artix Linux ⚠️ ):
```console
# mkdir -p /etc/runit/runsvdir/myrunlvl
# ln -s /etc/runit/sv/service /etc/runit/runsvdir/myrunlvl
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
