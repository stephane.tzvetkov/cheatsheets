---
tags:
  - Virtualization
  - Containers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Podman

Podman is a utility provided as part of the `libpod` library. It can be used to create and maintain
containers.

!!! Note "Info"
    * See [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://web.archive.org/web/20220510161213/https://www.ibm.com/cloud/blog/containers-vs-vms)

???+ Note "Reference(s)"
    * <https://docs.podman.io/en/latest/index.html>
    * <https://github.com/containers/podman>
    * <https://podman.io/getting-started/>
    * <https://github.com/containers/podman/blob/main/troubleshooting.md>
    * <https://wiki.archlinux.org/title/Podman>
    * <https://wiki.gentoo.org/wiki/Podman>
    * <https://www.howtoforge.com/getting-started-with-podman-on-rocky-linux/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apt add podman
        ```

    === "apt"
        ```console
        # apt install podman
        ```

    === "dnf"
        ```console
        # dnf install podman
        ```

    === "emerge"
        ```console
        # emerge -a app-containers/podman
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.podman
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.podman
            ```

    === "pacman"
        ```console
        # pacman -S podman
        ```

    === "yum"
        ```console
        # yum install podman
        ```

    === "xbps"
        ```console
        # xbps-install -S podman
        ```

    === "zypper"
        ```console
        # zypper install podman
        ```

---
## Config

Configuration files for configuring how containers behave are located at `/usr/share/containers/`.
You must copy necessary files to `/etc/containers` before edit. To configure the network bridge
interface used by Podman see `/etc/cni/net.d/87-podman-bridge.conflist`.

```console
# /etc/containers/registries.conf
[[registry]]
location = "docker.io"

[[registry]]
location = "registry.fedoraproject.org"

[[registry]]
location = "registry.access.redhat.com"
```

---
## Use

```console
$ podman info
```

**TODO**

### Troubleshooting

!!! Note ""
    See <https://github.com/containers/podman/blob/main/troubleshooting.md>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
