---
tags:
  - Programming Languages
  - Guidelines
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Coding guidelines

This cheat sheet gathers coding recommendations.

**TODO**: split this cheat sheet into dedicated ones (one per programming language).

???+ Note "Reference(s)"
    * <https://firefox-source-docs.mozilla.org/code-quality/coding-style/index.html>
    * <https://google.github.io/styleguide/>

!!! Note "See also"
    * How to conduct a code review: <https://www.jesuisundev.com/code-review/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [C](#c)
* [C++](#c-1)
* [Rust](#rust)
* [Java](#java)
* [JavaScript](#javascript)
* [Python](#python)
* [Bash](#bash)

<!-- vim-markdown-toc -->

---
## C

* [Suckless Coding style](https://suckless.org/coding_style/)


---
## C++

* [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines)
* [Mozilla C++ Coding style](https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_cpp.html)
* [Mozilla Formatting C++ Code With `clang-format`](https://firefox-source-docs.mozilla.org/code-quality/coding-style/format_cpp_code_with_clang-format.html)
* [Mozilla Using C++ code](https://firefox-source-docs.mozilla.org/code-quality/coding-style/using_cxx_in_firefox_code.html)
* [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)


---
## Rust

* [Mozilla Writing Rust code](https://firefox-source-docs.mozilla.org/writing-rust-code/index.html)

---
## Java

* [Mozilla Java Coding style](https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_java.html)


---
## JavaScript

* [Mozilla JavaScript Coding style](https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_js.html)
* [Airbnb JavaScript Coding Style](https://github.com/airbnb/javascript)


---
## Python

* [PEP 8 - Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
* [Mozilla Python Coding style](https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_python.html)
* [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)


---
## Bash

* [Bash Style Guide](https://www.daveeddy.com/bash/)
* [Writing Safe Shell Scripts](https://sipb.mit.edu/doc/safe-shell/)
* [Google Shell Style Guide](https://google.github.io/styleguide/shellguide.html)
* [`shellcheck`](https://www.shellcheck.net/)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
