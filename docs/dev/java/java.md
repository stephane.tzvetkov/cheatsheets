---
tags:
  - Programming Languages
  - Java
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Java

Java is a programming language created by Sun Microsystems. This company was bought out by Oracle
Corporation, which continues to keep it up to date.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Java#Setting_a_default>
    * <https://wiki.archlinux.org/index.php/Java#OpenJDK>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
    * [Managing multiple JDK](#managing-multiple-jdk)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../../shells/xdg.md)

See [how to handle Java related
dotfiles](../../admin/avoid_dotfile_madness.md#java-openjdk-javafonts-javauserprefs).

---
## Install

Install a JDK (e.g. `v11`):

!!! Note ""

    === "emerge"
        ```console
        # emerge -a dev-java/openjdk-bin-11
        ```

    === "pacman"
        ```console
        # pacman -S jdk11-openjdk
        ```

    === "apt"
        ```console
        # apt install openjdk-11-jdk
        ```

    === "yum"
        ```console
        # yum install java-11-openjdk
        ```

    === "dnf"
        ```console
        # dnf install java-11-openjdk
        ```

---
## Config

### Managing multiple JDK

!!! Note ""

    === "Gentoo based"
        ```console
        $ eselect java-vm list
        # eselect java-vm set system 1 # for the whole system
        $ eselect java-vm set user 1 # for the current user
        ```
        Check the result:
        ```console
        $ java -version
        ```

    === "Arch based"
        ```console
        $ archlinux-java status
        # archlinux-java set java-11-openjdk
        ```
        Check the result:
        ```console
        $ java -version
        ```

    === "Debian and Redhat based"
        List the different alternatives you have:
        ```console
        $ update-alternatives --list java
            > /usr/lib/jvm/java-11-openjdk-amd64/bin/java
            > /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
        ```
        Select an alternative:
        ```console
        $ sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
        ```
        Check the result:
        ```console
        $ java -version
        ```

---
## Use

Compile:
```console
# javac main.java
```

Run:
```console
$ java main.class
```

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
