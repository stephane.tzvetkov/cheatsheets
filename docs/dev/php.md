---
tags:
  - Programming Languages
  - PHP
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# PHP

PHP is a widely used general-purpose scripting language that is especially suited for Web
development and can be embedded into HTML.

!!! Warning "Warning"
    This cheat sheet only describe PHP maintaining on **Gentoo**.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/PHP>
    * <https://wiki.archlinux.org/index.php/PHP>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Minor update procedure](#minor-update-procedure)
    * [Major update procedure](#major-update-procedure)

<!-- vim-markdown-toc -->

---
## Install

```console
# emerge -a dev-lang/php
```


---
## Config

To install extensions, first decide which versions of PHP to compile the extensions for. This is
done by setting the `PHP_TARGETS` variable of your `make.conf` file (e.g. with version 5.6):

```console
$ vi /etc/portage/make.conf
    > ...
    > PHP_TARGETS="php5-6"
    > ...
```

!!! Warning "Warning"
    After modifying the above USE flag configuration, it is necessary to update the system so the
    changes take effect: `# emerge --ask --update --changed-use --deep @world`


---
## Use

* List versions of the PHP `cli` module:
```console
$ eselect php list cli
```

* List versions of the PHP `fpm` module:
```console
$ eselect php list fpm
```

### Minor update procedure

E.g. update from 7.3 to 7.4:
```console
# rc-service php-fpm stop # stop any running php

# eselect php set cli php7.4
$ eselect php list cli

# eselect php set fpm php7.4
$ eselect php list fpm

$ vi /etc/portage/make.conf
    > ...
    > PHP_TARGETS="php7-4"
    > ...
```

!!! Warning "Warning"
    After modifying the above USE flag configuration, it is necessary to update the system so the
    changes take effect: `# emerge --ask --update --changed-use --deep @world`

Then start the service:
```console
# rc-service php-fpm start
```

Quick check that PHP doesn't throw any warnings or errors:
```console
$ php -v
```

**If** the update seems stable, **and** that `php:7.3` is in the world set, **then** you might
consider replacing `php:7.3` by `php:7.4` in the world set:
```console
# vi /var/lib/portage/world
    > ...
    > dev-lang/php
    > dev-lang/php:7.4
    > ...
```

### Major update procedure

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
