---
tags:
  - Programming Languages
  - Python
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Python

Python is an interpreted, high-level, general-purpose programming language, first released in 1991.
Python's design philosophy emphasizes code readability with its notable use of significant white
space. Its language constructs and object oriented approach aim to help programmers write clear,
logical code for small and large-scale projects.

???+ Note "Reference(s)"
    * <https://cjolowicz.github.io/posts/hypermodern-python-01-setup/>
    * <https://www.python.org/>
    * <https://devguide.python.org/>
    * <https://docs.python.org/3/>
    * <https://wiki.gentoo.org/wiki/Python>
    * <https://wiki.archlinux.org/index.php/Python>
    * <https://en.wikipedia.org/wiki/Python_%28programming_language%29>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

    * [Install](#install)
    * [Config](#config)
* [Managing multiple Python versions](#managing-multiple-python-versions)
    * [Use](#use)
        * [How to compile explicitly](#how-to-compile-explicitly)
        * [Packaging](#packaging)
            * [How to package a Python program into a single file or folder with its dependencies](#how-to-package-a-python-program-into-a-single-file-or-folder-with-its-dependencies)
        * [Python virtual environment (`venv`) good practices](#python-virtual-environment-venv-good-practices)
        * [`pyenv`](#pyenv)
        * [Poetry](#poetry)
        * [Python packaging](#python-packaging)
        * [Conda (with Miniconda)](#conda-with-miniconda)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a dev-lang/python
        ```

    === "pacman"
        ```console
        # pacman -S python
        ```

    === "apt"
        ```console
        # apt install python3
        ```

    === "yum"
        ```console
        # yum install python
        ```

    === "dnf"
        ```console
        # dnf install python
        ```


---
## Config

TODO

# Managing multiple Python versions

!!! Note ""

    === "Gentoo based"
        TODO (with `eselect`?)

    === "Arch based"
        TODO

    === "Debian and Redhat based"
        List the different alternatives you have:
        ```console
        $ update-alternatives --list
            > python                  auto    /usr/libexec/no-python
            > ...
            > python3                 auto    /usr/bin/python3.6
            > ...
        ```
        Select an alternative:
        ```console
        $ sudo update-alternatives --config python
            There are 5 programs which provide 'python'.

              Selection    Command
            -----------------------------------------------
            *+ 1           /usr/libexec/no-python
               2           /usr/bin/python3
               3           /usr/bin/python3.8
               4           /usr/bin/python3.9
               5           /usr/bin/python2
            
            Enter to keep the current selection[+], or type selection number:
        ```
        Check the result:
        ```console
        $ python --version
        ```


---
## Use

TODO

!!! Warning "Warning"
    Try to not use `$ pip install ...` system wide. Prefer your system package manager instead
    whenever possible.

!!! Tip "Tip"
    If `$ pip install ...` is needed: then use it in a virtual environment (e.g. with `venv`).

!!! Tip "Tip"
    If `$ pip insall ...` is needed outside a virtual environment: then use the `--user` flag, in
    order to not install system wide but only user wide! This is important because a system wide
    installation could conflict with some packages.

!!! Warning "Warning"
    Never update `pip` without your package manager, i.e. never run `$ pip install --upgrade pip`
    or `$ python -m pip install --upgrade pip` outside a virtual environment.

!!! Tip "Tip"
    If you did updated `pip` outside a virtual environment, then you can recover a working `pip`
    with: `$ sudo python -m pip uninstall pip`, and then uninstall/re-install the `pip` package
    with your package manager (e.g. `$ sudo apt --reinstall install python-pip`). See
    <https://github.com/pypa/pip/issues/5599>.


* List `pip` installed modules
```console
$ python -m pip list
```

* Uninstall package
```console
$ python -m pip uninstall package-name
```

### How to compile explicitly

* Compile explicitly, `.pyc` compiled files will land in `__pycache__`:
```console
$ python -m compileall -f .
```

* Compile explicitly, `.pyc` compiled files will land next to their `.py` source files:
```console
$ python -m compileall -f -b .
```

* The major advantage with the last compilation method is that you can easily run the `.pyc`
  compilation files:
```console
$ python -m compileall -f -b . && python main.pyc
```

### Packaging

* <https://packaging.python.org/>

#### How to package a Python program into a single file or folder with its dependencies

See [`pyinstaller`](./pyinstaller.md) cheat sheet.

### Python virtual environment (`venv`) good practices

!!! Tip "Tip"
    It's a good practice to have a dedicated `venv` for a project needing python dependencies.

* Create and enter a `venv`:
```console
$ python -m venv pyenv
$ source ./pyenv/bin/activate
```

* Update pip:
```console
(pyenv) $ python -m pip install --upgrade pip
```

* Exit the `pyenv`:
```console
$ deactivate
```

### `pyenv`

`pyenv` lets you easily switch between multiple versions of Python:
<https://github.com/pyenv/pyenv>.

### Poetry

See [poetry cheat sheet](./poetry.md).

### Python packaging

TODO

???+ Note "Reference(s)"
    * <https://blog.engineering.publicissapient.fr/2020/06/04/packaging-python-setup-py-et-setuptools>


### Conda (with Miniconda)

See [Conda cheat sheet](./conda.md).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
