---
tags:
  - Programming Languages
  - Python
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `pyinstaller`

`pyinstaller` bundles a Python application and all its dependencies into a single package. The user
can run the packaged app without installing a Python interpreter or any modules. `pyinstaller`
supports Python 3.6 or newer, and correctly bundles the major Python packages such as `numpy`,
`pyqt`, and others.

???+ Note "Reference(s)"
    * <https://pyinstaller.readthedocs.io/en/latest/index.html>
    * <https://www.pyinstaller.org/>

!!! Note "Prerequisite(s)"
    * <https://pyinstaller.readthedocs.io/en/latest/requirements.html>


---
## Table of contents


<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Warning "Warning"
    Try to not use `$ pip install ...` system wide. Prefer your system package manager instead, or
    `pip`+`venv` (see [`pip`](./pip.md) and [`venv`](./venv.md)).

!!! Tip "Tip"
    If `$ pip insall ...` is needed outside a `venv`: then use the `--user` flag.

!!! Note ""

    === "pip"
        ```console
        $ pip install pyinstaller
        ```

    === "poetry"
        ```console
        $ poetry add --dev pyinstaller
        ```

    === "conda"
        TODO

---
## Use

Package your Python program into a single executable file with the following command:
```console
$ pyinstaller --clean --onefile main.py
```

### Troubleshooting

If you encounter the following error:
```console
ModuleNotFoundError: No module named 'epics.clibs'
```

Then the missing module might be hidden, re try like so:
```console
$ pyinstaller --clean --onefile --hidden-import epics.clibs main.py
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
