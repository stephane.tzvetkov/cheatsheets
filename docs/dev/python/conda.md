---
tags:
  - Programming Languages
  - Python
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Conda

!!! Note ""
    Conda is an alternative to `pip` + `venv`.

???+ Note "Reference(s)"

    * <https://docs.conda.io/en/latest/miniconda.html>
    * <https://conda.io/projects/conda/en/latest/index.html>
    * <https://conda.io/projects/conda/en/latest/user-guide/cheatsheet.html>
    * <https://www.anaconda.com/blog/conda-configuration-engine-power-users>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
    * [Uninstall](#uninstall)
* [Config](#config)
* [Use](#use)
        * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).


---
## Install

???+ Note "Reference(s)"
    * <https://conda.io/projects/conda/en/latest/user-guide/install/index.html>

!!! Warning "Prerequisite(s)"
    * [XDG](../../shells/xdg.md)
    * [Conda dotfiles](../../admin/avoid_dotfile_madness.md#conda-conda)

Download [the latest Miniconda
installer](https://docs.conda.io/en/latest/miniconda.html#linux-installers) and run it like so:
```console
$ bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda/miniconda3
$ mkdir -p $HOME/.config/conda
$ touch $CONDARC
$ echo "auto_activate_base: false" >> $CONDARC
$ conda config --system --set auto_activate_base false
$ rm -rf $HOME/.conda
```

### Uninstall

```console
$ sudo rm -rf /opt/conda

$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
  - > # CONDA
  - > export CONDA_ROOT=/opt/conda
  - > export CONDA_ENVS_PATH=/opt/conda/envs
  - > export CONDA_PKGS_DIRS=/opt/conda/pkgs
  - > export CONDARC="${XDG_CONFIG_HOME:-${HOME/.config}}/conda/condarc"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

---
## Config

If you prefer Conda base environment not activated on startup, set the `auto_activate_base`
parameter to false:
```console
$ conda config --set auto_activate_base false
```

* Activate base Conda:
```console
$ conda activate
```

* Deactivate Conda:
```console
$ conda deactivate
```

TODO

---
## Use

* Create a Conda environment for a project with a (previously configured) `.yml` file:
```console
$ cd /path/to/projectname
$ conda env create -f environment.yml
```

TODO

#### Troubleshooting

If you are using Zsh, when using `conda activate ...`, you might encounter the following error:
`/home/user/miniconda3/envs/projectname/etc/conda/activate.d/toolchain_activate.sh:3: = not found`

You can try to solve this error by modifying your `.zshrc` file like bellow (see
<https://github.com/conda-forge/toolchain-feedstock/issues/49>):
```console
$ vi ${ZDOTDIR:-${HOME}}/.zshrc # or wherever
    > ...
    > # >>> conda initialize >>>
    > # !! Contents within this block are managed by 'conda init' !!
    > __conda_setup="$('/home/user/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    > if [ $? -eq 0 ]; then
    >     eval "$__conda_setup"
    > else
    >     if [ -f "/home/user/miniconda3/etc/profile.d/conda.sh" ]; then
    >         # The following line might not work with zsh:
    >         . "/home/user/miniconda3/etc/profile.d/conda.sh"
    >         # see <https://github.com/conda-forge/toolchain-feedstock/issues/49>
    >         #emulate bash -c ". /home/user/miniconda3/etc/profile.d/conda.sh"
    >     else
    >         export PATH="/home/user/miniconda3/bin:$PATH"
    >     fi
    > fi
    > unset __conda_setup
    > # <<< conda initialize <<<
    > ...
```

You can also try to replace every `==` by `=` in the following files (in order to be POSIX
compliant):

* `/home/user/miniconda3/envs/anf-ml/etc/conda/activate.d/toolchain_activate.sh`
* `/home/user/miniconda3/envs/anf-ml/etc/conda/deactivate.d/toolchain_deactivate.sh`


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
