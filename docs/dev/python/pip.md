---
tags:
  - Programming Languages
  - Python
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Pip

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

**TODO**

---
## Use

How to export/import dependencies for offline installation:

* export:
```console
$ cd /path/to/online-python/project
$ . ./venv/bin/activate
$ pip freeze | grep -v "pattern-to-exclude" > /path/to/requirements.txt
$ pip download -d /path/to/offline-dependencies -r /path/to/requirements.txt
$ deactivate
```

* import:
```console
$ cd /path/to/offline-python/project
$ python -m venv otherenv
$ . ./otherenv/bin/activate
$ pip install --no-index --find-links /path/to/offline-dependencies -r /path/to/requirements.txt
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
