---
tags:
  - Programming Languages
  - Tools
  - GNU
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# GNU Make

GNU Make is a tool which controls the generation of executables and other non-source files of a
program from the program's source files.

???+ Note "Reference(s)"
    * <https://www.gnu.org/software/make/>
    * <https://www.gnu.org/software/make/manual/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a sys-devel/make
        ```

    === "pacman"
        ```console
        # pacman -S make
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.gnumake
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.gnumake
            ```

    === "apt"
        ```console
        # apt install make
        ```

    === "yum"
        ```console
        # yum install make
        ```

    === "dnf"
        ```console
        # dnf install make
        ```

---
## Config

TODO

---
## Use

WIP

```console
$ make clean && make CFLAGS="-fPIC" CXXFLAGS="-fpermissive -fPIC".
```

* `c++98` or `c++03` for "ISO C++ 1998 with amendments" standard
* `gnu++98` or `gnu++03` for "ISO C++ 1998 with amendments and GNU extensions" standard
* `c++11` for "ISO C++ 2011 with amendments" standard
* `gnu++11` for "ISO C++ 2011 with amendments and GNU extensions" standard
* `c++14` for "ISO C++ 2014 with amendments" standard
* `gnu++14` for "ISO C++ 2014 with amendments and GNU extensions" standard
* `c++17` for "ISO C++ 2017 with amendments" standard
* `gnu++17` for "ISO C++ 2017 with amendments and GNU extensions" standard
* `c++20` for "ISO C++ 2020 DIS" standard
* `gnu++20` for "ISO C++ 2020 DIS with GNU extensions" standard
* `c89`, `c90`, or `iso9899:1990` for "ISO C 1990" standard
* `iso9899:199409` for "ISO C 1990 with amendment 1" standard
* `gnu89` or `gnu90` for "ISO C 1990 with GNU extensions" standard
* `c99` or `iso9899:1999` for "ISO C 1999" standard
* `gnu99` for "ISO C 1999 with GNU extensions" standard
* `c11` or `iso9899:2011` for "ISO C 2011" standard
* `gnu11` for "ISO C 2011 with GNU extensions" standard
* `c17`, `iso9899:2017`, `c18`, or `iso9899:2018` for "ISO C 2017" standard
* `gnu17` or `gnu18` for "ISO C 2017 with GNU extensions" standard

E.g.
```console
$ make clean && make CFLAGS="-std=gnu90 -fPIC" CXXFLAGS="-std=c++14 -fpermissive -fPIC"
```

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
