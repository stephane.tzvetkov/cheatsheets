---
tags:
  - Programming Languages
  - Rust
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Rust

Rust is a multi paradigm system programming language focused on safety, especially safe
concurrency. Rust is syntactically similar to C++, but is designed to provide better memory safety
while maintaining high performance.

???+ Note "Reference(s)"
    * <https://practice.rs/why-exercise.html>
    * <https://practice.rs/elegant-code-base.html>
    * <https://www.rust-lang.org/>
    * <https://wiki.archlinux.org/index.php/Rust>
    * <https://wiki.gentoo.org/wiki/Project:Rust>
    * <https://dystroy.org/blog/how-not-to-learn-rust/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Update (only after online rust install way)](#update-only-after-online-rust-install-way)
    * [Uninstall rust (only after online rust install way)](#uninstall-rust-only-after-online-rust-install-way)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../../shells/xdg.md)

See [how to handle Rust related dotfiles](../../admin/avoid_dotfile_madness.md#rust-cargo).


---
## Install

!!! Note ""

    === "emerge"
        Install `rust-bin`, the last ("testing" on gentoo but stable according to rust)
        pre-compiled (binary) version:
        ```console
        # echo "dev-lang/rust-bin ~amd64" >> /etc/portage/package.accept_keywords
        # emerge -a dev-lang/rust-bin
        ```
        <br/>
        **Or** install and compile the last ("testing"...) rust version from sources (this
        can take a long time):
        ```console
        # echo "dev-lang/rust ~amd64" >> /etc/portage/package.accept_keywords
        # emerge -a dev-lang/rust
        ```

    === "pacman"
        ```console
        # pacman -S rust
        ```

    === "apt"
        ```console
        # apt install rust
        ```

    === "yum"
        ```console
        # yum install rust
        ```

    === "dnf"
        ```console
        # dnf install rust
        ```

    === "online rust installer"
        ```console
        $ sudo curl https://sh.rustup.rs -sSf | sh #then enter "1" when asked for a default install
        ```
        <br/>
        Check if rust is well installed
        ```console
        # rustc --version
        ```

---
## Config

TODO


---
## Use

* Install package:
```console
$ cargo install package-name
```

* Update all cargo packages
  (<https://stackoverflow.com/questions/34484361/does-cargo-install-have-an-equivalent-update-command>):
```console
$ cargo install cargo-update
$ cargo install-update -a
```

* Prints package metadata:
```console
$ cargo show package-name
```

* List all installed packages:
```console
$ cargo install --list
```

### Update (only after online rust install way)

Update rust:
```console
# rustup update
```

### Uninstall rust (only after online rust install way)

If you want to uninstall rust (e.g. you finally prefer to install rust with portage or apt or yum
or whatever):
```console
# rustup self uninstall
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
