---
tags:
  - Networks
  - Monitoring Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `tcpdump`

`tcpdump` is a command line network monitoring and data acquisition tool. It is capable of sniffing
packets and "dumping" information.


???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Tcpdump>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge --ask net-analyzer/tcpdump
        ```

    === "pacman"
        ```console
        # pacman -S tcpdump
        ```

    === "apt"
        ```console
        # apt install tcpdump
        ```

    === "yum"
        ```console
        # yum install tcpdump
        ```

    === "dnf"
        ```console
        # dnf install tcpdump
        ```


---
## Config

!!! Note ""

    === "Gentoo-based distros"
        In order for normal users to run `tcpdump` the program should be built with the `suid` flag
        enabled and the user(s) should be added to the `tcpdump` group:
        ```console
        # USE="suid" emerge -a --changed-use tcpdump
        # usermod -a -G tcpdump <username>
        ```

    === "Other distros"
        In order for normal users to run `tcpdump` user(s) should be added to the `tcpdump` group:
        ```console
        # usermod -a -G tcpdump <username>
        ```


---
## Use

* List available interfaces:
  ```console
  $ tcpdump --list-interfaces
  $ tcpdump -D
  ```

* Listen to a specific interface:
  ```console
  $ tcpdump -i <interface_name>
  ```

* Write output to a file:
  ```console
  $ tcpdump -w /tmp/output
  ```

* Read input from file:
  ```console
  $ tcpdump -r /tmp/input
  ```

* Capture for the next 42 packets only:
  ```console
  $ tcpdump -c 42
  ```

* Print packets in ASCII format:
  ```console
  $ tcpdump -A
  ```

* Print packets (header AND DATA) in HEX and ASCII:
  ```console
  $ tcpdump -XX
  ```

* Print IP address not names
  ```console
  $ tcpdump -n
  ```

* Capture only TCP packets:
  ```console
  $ tcpdump tcp
  ```

* Capture only UDP packets:
  ```console
  $ tcpdump udp
  ```

* Capture only from source IP (e.g. 50.116.66.139):
  ```console
  $ tcpdump src 50.116.66.139
  ```

* Capture only packets to destination IP (e.g. 50.116.66.139):
  ```console
  $ tcpdump dst 50.116.66.139
  ```

* Capture packets from a specific port (e.g. 22):
  ```console
  $ tcpdump port 22
  ```

* Capture packets from a specific port range (e.g. `5064-5065`):
  ```console
  $ tcpdump portrange 5064-5065
  ```

* Capture packets from a specific host and port (e.g. 10.0.0.0 and 22):
  ```console
  $ tcpdump host 10.0.0.0 and port 22
  ```

* Print an optional packet number at the beginning of the line:
  ```console
  $ tcpdump --number
  ```

* Print less protocol information so output lines are shorter:
  ```console
  $ tcpdump -q
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
