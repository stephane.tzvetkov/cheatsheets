---
tags:
  - Networks
  - Firewalls
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `ufw`

`ufw` stands for uncomplicated firewall, and is just that. It uses logs such as those obtained by
`syslog-ng` for monitoring, and uses `iptables` as a back end. `ufw` supports both IPv4 and IPv6.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Ufw>
    * <https://wiki.archlinux.org/index.php/Uncomplicated_Firewall#Rate_limiting_with_ufw>
    * <https://askubuntu.com/questions/54771/potential-ufw-and-fail2ban-conflicts#85424>
    * <https://www.c-rieger.de/nextcloud-16-installation-guide-debian-apache2/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
* [Notes](#notes)

<!-- vim-markdown-toc -->

---
## Install


!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # IP v4 settings
            # Double check here: <https://wiki.gentoo.org/wiki/Ufw#Kernel>
            #
            > [*] Networking support  ---> # Symbol: NET [=y]
            >   Networking options  --->
            >     [*] Network packet filtering framework (Netfilter)  ---> # Symbol: NETFILTER [=y]
            >       Core Netfilter Configuration  --->
            >         <*> NetBIOS name service protocol support # Symbol: NF_CONNTRACK_NETBIOS_NS [=y]

            # IP v6 settings
            # Double check here: <https://wiki.gentoo.org/wiki/Ufw#Kernel>
            #
            > [*] Networking support  ---> # Symbol: NET [=y]
            >   Networking options  --->
            >     [*] Network packet filtering framework (Netfilter)  ---> # Symbol: NETFILTER [=y]
            >       IPv6: Netfilter Configuration  --->
            >         <*>   "rt" Routing header match support # Symbol: IP6_NF_MATCH_RT [=y]
            >         <*>   "HL" hoplimit target support # Symbol: IP6_NF_TARGET_HL [=y]

            # Iptables
            # Double check here: <https://wiki.gentoo.org/wiki/Iptables#Kernel>
            #
            ⚠️ TODO⚠️
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!


!!! Note ""

    === "emerge"
        ```console
        # emerge -a ufw
        ```

    === "pacman"
        ```console
        # pacman -S ufw
        ```

    === "apt"
        ```console
        # apt install ufw
        ```

---
## Config

Add `ufw` to the default boot level and start it:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add ufw default
        # rc-service ufw start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/ufw /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/ufw /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/ufw /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up ufw
        ```

    === "SysVinit"
        ```console
        # service ufw start
        # chkconfig ufw on
        ```

    === "SystemD"
        ```console
        # systemctl enable ufw
        # systemctl start ufw
        ```

---
## Use

* Enable `ufw` (only required one time after the package has been installed)
  ```console
  # ufw enable
  ```

* Allow ssh (if wanted) which is blocked by default:
  ```console
  # ufw allow ssh
  ```

* Create a simple configuration (just an example):
  ```console
  # ufw default deny
  # ufw allow from 192.168.0.0/24
  ```

* Get a list of possible applications to add:
  ```console
  # ufw app list
  ```

* Add an application:
  ```console
  # ufw allow application-name
  ```

---
## Notes

For Nextcloud?:
```console
$ sudo ufw allow 80,443/tcp
$ sudo ufw allow nextcloud
```



---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
