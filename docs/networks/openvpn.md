---
tags:
  - Networks
  - VPN
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OpenVPN

OpenVPN (Open Virtual Private Network) is software that enables the creation of secure
point to point (or site to site) connections.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/OpenVPN>
    * <https://wiki.gentoo.org/wiki/OpenVPN>
    * <https://www.youtube.com/watch?v=gxpX_mubz2A>
    * <https://openvpn.net>
    * <https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage>
    * <https://forums.gentoo.org/viewtopic-t-538662.html>
    * <https://github.com/Angristan/OpenVPN-install>
    * <https://github.com/Nyr/openvpn-install>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [OpenVPN server](#openvpn-server)
    * [OpenVPN client](#openvpn-client)
* [Use](#use)
    * [DNS leak](#dns-leak)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # OpenVPN requires TUN/TAP support
            # Double check here: <https://wiki.gentoo.org/wiki/OpenVPN#Kernel>
            #
            > Device Drivers  --->
            >   [*] Network device support  --->
            >       [*] Network core driver support
            >           <*>   Universal TUN/TAP device driver support
        ```

    !!! Warning "Warning"
        After configuring the kernel don't forget to do a [kernel make and
        rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-vpn/openvpn
        ```

    === "pacman"
        ```console
        # pacman -S openvpn
        ```

    === "apt"
        ```console
        # apt install openvpn
        ```

    === "yum"
        TODO

    === "dnf"
        TODO


---
## Config

### OpenVPN server

The [`openvpn-install`](https://github.com/Angristan/OpenVPN-install) script will let you setup your
own OpenVPN server in just a few seconds, with a state-of-the-art security configuration.

!!! Note "Note"
    This [`Angristan` `openvpn-install`](https://github.com/Angristan/OpenVPN-install) script is
    based on the work of the [`Nyr` `openvpn-install`](https://github.com/Nyr/openvpn-install)
    script. The `Angristan`'s script try to have a enhanced security, but is compatible with recent
    distributions only. So, if using an old server or client, the `Nyr`'s script is better suited.

!!! Warning "Warning"
    * This script only support the following architectures and distros (and distros versions):
      <https://github.com/Angristan/OpenVPN-install#compatibility>.
    * This script requires SystemD.
    * This script is regularly tested against AMD64 only.


* First, get the script, make it executable and run it:
```console
$ curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
$ chmod +x openvpn-install.sh
$ sudo ./openvpn-install.sh
```

* Then, follow the assistant and answer a few questions to setup the VPN server.

* At the end, you will be asked for a client name: enter it (a passwordless client is OK). The
  associated configuration file will be located in your home directory: `$HOME/client-name.ovpn`

* After running `openvpn-install` for the first time, you can run the script again, and you will get
  the choice to:
    * Add a client
    * Remove a client
    * Uninstall OpenVPN

* Optionally, you can disable OpenVPN logs:
```console
$ sudo vi /etc/openvpn/server.conf # or /etc/openvpn/server/server.conf
    > ...
  ~ > verb 0
    > ...

$ sudo systemctl restart openvpn-server@server
```

### OpenVPN client

The client should retrieve the `.ovpn` file, e.g. with `scp`:
```console
$ scp server-user@123.1.2.3:/home/server-user/client-name.ovpn .
```


---
## Use

* Run OpenVPN client side, in order to route all internet traffic through the OpenVPN server:
  ```console
  $ sudo openvpn /path/to/client-name.ovpn
  ```

* Run OpenVPN client side, in order to only route the traffic of some applications through the
  OpenVPN server: see [`vpnify`](./vpnify.md).

### DNS leak

After setting up your VPN connection for the first time, you might want to check for DNS leaks. In
order to do this, Go to <https://www.browserleaks.com> or <https://www.ipleak.net> (both perform
IPv4 and IPv6 check) with your browser. Only your server IP address should show up (test without
and without the VPN), not your client IP address. The DNS servers should be the ones you selected
during the setup, not your client IP address nor your ISP's DNS servers' addresses.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
