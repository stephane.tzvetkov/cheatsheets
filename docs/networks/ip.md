---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `ip`

`ip` is a program used to show / manipulate routing, network devices, interfaces and tunnels.

???+ Note "Reference(s)"
    * `curl cheat.sh/ip`

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Practical use case](#practical-use-case)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**WiP**

* Display all interfaces with addresses:
  ```
  $ ip addr
  ```

* Take down / up the wireless adapter:
  ```
  $ ip link set dev wlan0 {up|down}
  ```

* Set a static IP and netmask:
  ```
  $ ip addr add 192.168.1.100/32 dev eth0
  ```

* Remove a IP from an interface:
  ```
  $ ip addr del 192.168.1.100/32 dev eth0
  ```

* Remove all IPs from an interface:
  ```
  $ ip address flush dev eth0
  ```

* Display all routes:
  ```
  $ ip route
  ```

* Display all routes for IPv6:
  ```
  $ ip -6 route
  ```

* Add default route via gateway IP:
  ```
  $ ip route add default via 192.168.1.1
  ```

* Add route via interface:
  ```
  $ ip route add 192.168.0.0/24 dev eth0
  ```

* Get the route used for an destination
  ```
  $ ip route get to 8.8.8.8
  ```

* Change your mac address :
  ```
  $ ip link set dev eth0 address aa:bb:cc:dd:ee:ff
  ```

* View neighbors (using ARP and NDP):
  ```
  $ ip neighbor show
  ```

### Practical use case

Let's say you have a network, e.g. `192.168.1.0/24`,
so with the following IP range: `192.168.1.42` to `192.168.1.254`.

Now you want to connect to this network with the following address: `192.168.1.42`.

Here is how to do it with `ip`:

* Stop the DHCP service:
  ```
  $ sudo systemctl stop dhcpcd.service
  ```

* Check your interfaces:
  ```
  $ ip a
  ```

* Choose an available interface, e.g. `enp1s1`.

* Add an IP address (`192.168.1.42`) to your interface:
  ```
  $ ip addr add "192.168.1.42/24" dev enp1s1
  ```

* Add a gateway (e.g. `192.168.1.254`), if your network has one:
  ```
  $ ip route add default via "192.168.1.254" dev enp1s1
  ```

* Add a DNS (e.g. `192.168.1.1`), if your network has one:
  ```
  $ echo "nameserver 192.168.1.1" >> /etc/resolv.conf
  ```

* Check you connectivity:
  ```
  $ ping duckduckgo.com
  ```

---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
