---
tags:
  - Networks
  - Timing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `chrony`

**TODO**

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Chrony>
    * <https://wiki.gentoo.org/wiki/Chrony>
    * <https://chrony.tuxfamily.org/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apt add chrony
        ```

    === "apt"
        ```console
        # apt install chrony
        ```

    === "dnf"
        ```console
        # dnf install chrony
        ```

    === "emerge"
        ```console
        # emerge -a sys-process/chrony
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.chrony
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.chrony
            ```

    === "pacman"
        ```console
        # pacman -S chrony
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S chrony chrony-dinit
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S chrony chrony-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S chrony chrony-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S docker chrony-s6
            ```

    === "yum"
        ```console
        # yum install chrony
        ```

    === "xbps"
        ```console
        # xbps-install -S chrony
        ```

    === "zypper"
        ```console
        # zypper install chrony
        ```

---
## Config

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add chrony default
        # rc-service chrony start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/chrony /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/chrony /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/chrony /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up chrony
        ```

    === "SysVinit"
        ```console
        # service chrony start
        # chkconfig chrony on
        ```

    === "SystemD"
        ```console
        # systemctl enable chrony
        # systemctl start chrony
        ```

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
