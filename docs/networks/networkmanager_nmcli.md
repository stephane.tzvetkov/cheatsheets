---
tags:
  - Networks
  - Network Managers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# NetworkManager `nmcli`

A network management software for Ethernet, WiFi, DSL, VPN, etc, and mobile broadband network
connections. NetworkManager is a program for providing detection and configuration for systems to
automatically connect to networks (wired and wireless). `nmcli` is it's official command line
interface, about which we will focus here.

!!! Tip "Tip"
    If you don't want to use a CLI tool, then choose a GUI one: a lot of GUI front-ends are
    available](https://wiki.archlinux.org/title/NetworkManager#Front-ends). In addition,
    [`nmtui`](https://man.archlinux.org/man/nmtui.1) is very nice and simple TUI alternative.

???+ Note "Reference(s)"
    * <https://linux.goffinet.org/administration/configuration-du-reseau/gestion-du-reseau-linux-avec-networkmanager/>
    * <https://wiki.archlinux.org/title/NetworkManager>
    * <https://man.archlinux.org/man/nmcli.1>
    * <https://man.archlinux.org/man/nmcli-examples.7.en>
    * <https://wiki.gentoo.org/wiki/NetworkManager>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Status](#status)
    * [Connection](#connection)
    * [Wi-Fi](#wi-fi)
    * [Editor](#editor)
    * [MAC address](#mac-address)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        **TODO**
        ```console
        ```

    === "pacman"
        ```console
        # pacman -S networkmanager
        ```

        !!! Tip "For Artix users"
            * **If** using `openrc`:
            ```console
            # pacman -S cronie networkmanager-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S cronie networkmanager-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S cronie networkmanager-s6
            ```

    === "apt"
        **TODO**
        ```console
        ```

    === "yum"
        **TODO**
        ```console
        ```

    === "dnf"
        **TODO**
        ```console
        ```

---
## Config

Now, add `NetworkManager` to your init manager:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add NetworkManager default
        # /etc/init.d/NetworkManager start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/NetworkManager /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/NetworkManager /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/NetworkManager /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up NetworkManager
        ```

    === "SysVinit"
        ```console
        # service NetworkManager start
        # chkconfig NetworkManager on
        ```

    === "SystemD"
        ```console
        # systemctl enable NetworkManager
        # systemctl start NetworkManager
        ```


---
## Use

### Status

* Get NetworkManager global status:
```console
$ nmcli
$ nmcli device status
$ nmcli dev status
```

* Print whether NetworkManager is running or not:
```console
$ nmcli -t -f RUNNING general
```

* Print whether NetworkManager general state:
```console
$ nmcli -t -f STATE general
```

* Show all configured connections (active connections are green and inactive ones are white):
```console
$ nmcli con show
$ nmcli connection show
```

* Show all configured connections in multi line mode:
```console
$ nmcli -p -m multiline -f all con show
```

* List all currently active connections:
```console
$ nmcli connection show --active
```

* Show static configuration details of a specific connection:
```console
$ nmcli -f profile con s "SSID or BSSID"
```

### Connection

* Activate a specific connection:
```console
$ nmcli con up "SSID or BSSID"
```

* Activate a specific connection showing the progress of the activation:
```console
$ nmcli -p con up "SSID or BSSID"
```

* Activate a specific connection with interactive password:
```console
$ nmcli --ask con up "SSID or BSSID"
```

* Deactivate a specific connection:
```console
$ nmcli con down "SSID or BSSID"
```

* Show  details for a specific connection:
```console
nmcli -p connection show "SSID or BSSID"
```

### Wi-Fi

* Switch Wi-Fi off and on:
```console
$ nmcli radio wifi off
$ nmcli radio wifi on
```

* List available Wi-Fi:
```console
$ nmcli device wifi list
$ nmcli dev wifi list
```

* Connect to a password protected Wi-Fi network:
```console
$ nmcli dev wifi connect "SSID or BSSID" password "your password"
```

* Connect to a password protected Wi-Fi network interactively:
```console
$ nmcli --ask dev wifi connect "SSID or BSSID"
```

> After your first connection, the password used will be saved, so the next time you can just run:
> `nmcli con up "SSID or BSSID"`.

* shows details for "SSID or BSSID" connection profile with all passwords:
```console
$ nmcli --show-secrets con show "SSID or BSSID"
```

* Showing general information and properties for a Wi-Fi interface (e.g. `wlan0`):
```console
$ nmcli -p -f general,wifi-properties dev show wlan0
```

* Create a hotspot profile and connects it (also print the hotspot password the user should use to
  connect to the hotspot from other devices) **WIP**:
```console
$ nmcli -s dev wifi hotspot con-name QuickHotspot
```

### Editor

**WIP**

* Edit existing "SSID or BSSID" connection in the interactive editor:
```console
nmcli connection edit ethernet-em1-2
```

* Add a new Ethernet connection in the interactive editor:
```console
nmcli connection edit type ethernet con-name "yet another Ethernet connection"
```

**TODO**

### MAC address

??? Note "Reference(s)"
    * <https://askubuntu.com/questions/1121523/how-do-i-get-networkmanager-to-assign-a-fixed-mac-address-to-eth0>.

Change the MAC address of an interface (e.g. interface `enp1s0` with the new MAC address
`00:12:34:56:78:9a`):
```console
$ sudo nmcli connection modify enp1s0 ethernet.cloned-mac-address 00:12:34:56:78:9a
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
