---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# DNS

???+ Note "Reference(s)"
    * <https://www.bortzmeyer.org/google-dns.html>
    * <https://www.justgeek.fr/liste-des-serveurs-dns-les-plus-rapides-et-securises-44705/>
    * <https://www.bortzmeyer.org/serveur-dns-faisant-autorite.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [DNS crypt proxy](#dns-crypt-proxy)

<!-- vim-markdown-toc -->


---
## DNS crypt proxy

Host his own DNS:

* <https://github.com/DNSCrypt/dnscrypt-proxy>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
