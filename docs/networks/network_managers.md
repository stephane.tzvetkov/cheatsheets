---
tags:
  - Networks
  - Network Managers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Network managers

A network manager lets you manage network connection settings in so called network profiles to
facilitate switching networks. There are many solutions to choose from, but remember that all of
them are mutually exclusive; you should not run two daemons simultaneously. This cheat sheets will
just link you to some of the most known solutions.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Network_management>
    * <https://wiki.archlinux.org/index.php/Category:Network_managers>
    * <https://wiki.archlinux.org/index.php/Network_configuration>
    * <https://wiki.archlinux.org/title/Network_configuration#Network_managers>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Solutions](#solutions)

<!-- vim-markdown-toc -->

---
## Solutions

* Wired and wireless network managers (recommended):
    * [NetworkManager](./networkmanager_nmcli.md)
    * [`systemd-networkd`](../init-systems/systemd/systemd_networkd.md) (⚠️ only for
      [SystemD](../init-systems/systemd.md)-based distros ⚠️)
    * [ConnMan](./connman.md)
    * `netctl` : **TODO** (see <https://wiki.archlinux.org/title/Netctl>).
    * `netifrc`: **TODO** (⚠️ only for Gentoo-based distros ⚠️)
    * [`dhcpcd`](./dhcpcd.md)

* Wireless only network managers:
    * `iwd` (iNet wireless daemon): **TODO** (see <https://wiki.archlinux.org/title/Iwd>).
    * [`wpa_supplicant`](./wpa_supplicant.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
