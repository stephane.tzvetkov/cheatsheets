---
tags:
  - Networks
  - VPN
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# WireGuard

WireGuard is an extremely simple yet fast and modern VPN that utilizes state-of-the-art
cryptography. It aims to be faster, simpler, leaner, and more useful than IPsec, while avoiding the
massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed
as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many
different circumstances.

**TODO**: <https://docs.linuxserver.io/images/docker-wireguard>

???+ Note "Reference(s)"
    * <https://www.wireguard.com/>
    * <https://docs.linuxserver.io/images/docker-wireguard>
    * <https://www.wireguard.com/performance/>
    * <https://wiki.archlinux.org/index.php/WireGuard>
    * <https://wiki.gentoo.org/wiki/Wireguard>
    * <https://github.com/angristan/wireguard-install>
    * <https://github.com/Nyr/wireguard-install>
    * <https://philipdeljanov.com/posts/2019/03/21/setting-up-a-wireguard-vpn/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [WireGuard server](#wireguard-server)
    * [WireGuard client](#wireguard-client)
        * [With the GUI Android client or GUI Windows client application](#with-the-gui-android-client-or-gui-windows-client-application)
        * [With the CLI Linux client](#with-the-cli-linux-client)
* [Use](#use)
    * [With the GUI Android client or GUI Windows client application](#with-the-gui-android-client-or-gui-windows-client-application-1)
    * [With the CLI Linux client](#with-the-cli-linux-client-1)
    * [IP routing](#ip-routing)
    * [App routing](#app-routing)
    * [Avoid DNS leak](#avoid-dns-leak)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`
        
            # WireGuard requirements
            # Double check here: <https://wiki.gentoo.org/wiki/Wireguard#Kernel>
            #
            >     Device Drivers  --->
            >       [*] Network device support  ---> # Symbol: NETDEVICES [=y]
            >           [*] Network core driver support # Symbol: NET_CORE [=y]
            >           <*>   WireGuard secure network tunnel # Symbol: WIREGUARD [=y]

            > [*] Networking support  ---> # Symbol: NET [=y]
            >       Networking options  --->
            >           [*] TCP/IP networking # Symbol: INET [=y]
            >           <*>     The IPv6 protocol  ---> # Symbol: IPV6 [=y]
            >               [*]    IPv6: Multiple Routing Tables # Symbol: IPV6_MULTIPLE_TABLES [=y]
            >           [*] Network packet filtering framework (Netfilter) # Symbol: NETFILTER [=y]
            >               Core Netfilter Configuration
            >                   -*- Netfilter Xtables support # Symbol: NETFILTER_XTABLES [=y]
            >                   <*>   "CONNMARK" target support # Symbol: NETFILTER_XT_TARGET_CONNMARK [=y]
            >               IP: Netfilter Configuration
            >                   <*> IP tables support # Symbol: IP_NF_IPTABLES [=y]
            >                   <*>   raw table support # Symbol: IP_NF_RAW [=y]
            >               IPv6: Netfilter Configuration
            >                   <*> IP6 tables support # Symbol: IP6_NF_IPTABLES [=y]
            >                   <*>   raw table support # Symbol: IP6_NF_RAW [=y]
        ```

    !!! Warning "Warning"
        After configuring the kernel don't forget to do a [kernel make and
        rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

Install WireGuard (see <https://www.wireguard.com/install/>):

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-vpn/wireguard-tools
        ```

    === "pacman"
        ```console
        # pacman -S wireguard-tools
        ```
        also if using lts kernel:
        ```console
        # pacman -S wireguard-lts
        ```

    === "apt"
        ```console
        # apt install wireguard
        ```

    === "yum"
        ```console
        # sudo yum wireguard-tools
        ```

    === "dnf"
        ```console
        # sudo dnf wireguard-tools
        ```


---
## Config

### WireGuard server

The [WireGuard install](https://github.com/angristan/wireguard-install) script will let you setup
your own WireGuard VPN server in just a few seconds, with a state-of-the-art security
configuration.

!!! Note "Note"
    This [`Angristan` `wireguard-install`](https://github.com/angristan/wireguard-install) script is
    based on the work of the [`Nyr` `wireguard-install`](https://github.com/Nyr/wireguard-install)
    script. The `Angristan`'s script try to have a enhanced security, but is compatible with recent
    distributions only. So, if using an old server or client, the `Nyr`'s script is better suited.

!!! Warning "Warning"
    * This script only support the following architectures and distros (and distros versions):
      <https://github.com/Angristan/OpenVPN-install#compatibility> and
      <https://github.com/angristan/wireguard-install#requirements>.
    * This script requires SystemD.
    * This script is regularly tested against AMD64 only.


* First, get the script, make it executable and run it:
```console
$ curl -O https://raw.githubusercontent.com/angristan/wireguard-install/master/wireguard-install.sh
$ chmod +x wireguard-install.sh
$ sudo ./wireguard-install.sh
```

* Then, follow the assistant and answer a few questions to setup the VPN server.

* At the end, you will be asked for a client name: enter it. The associated configuration file will
  be located in your home directory: `$HOME/wg0-client-name.conf`

* After running `wireguard-install` for the first time, you can run the script again, and you will
  get the choice to:
    * Add a client
    * Remove a client
    * Uninstall WireGuard

### WireGuard client

#### With the GUI Android client or GUI Windows client application

* After adding a client, a QR code should have been generated - server side - in the shell console.
  If the client application supports it, this QR code can be used to setup the client VPN.

#### With the CLI Linux client

* Alternatively, the client can retrieve the `.conf` file, e.g. with `scp`:
  ```console
  $ mkdir -p $HOME/.config/wireguard
  $ scp server-user@123.1.2.3:/home/server-user/wg0-client-name.conf $HOME/.config/wireguard/wg0.conf
  ```

!!! Note "Note"
    The configuration must be renamed to the target interface name, with the `.conf` suffix, in
    this case `wg0.conf`.


---
## Use

### With the GUI Android client or GUI Windows client application

* Just open the WireGuard application and activate the VPN.

### With the CLI Linux client

* Start (routing all internet traffic through the WireGuard VPN server):
  ```console
  $ sudo wg-quick up $HOME/.config/wireguard/wg0.conf
  ```

* Stop (routing all internet traffic through the WireGuard VPN server):
  ```console
  $ sudo wg-quick down $HOME/.config/wireguard/wg0.conf
  ```

* Start/Stop (routing the traffic of some applications through the WireGuard VPN server): see
  [`vpnify`](./vpnify.md)?

### IP routing
> IP based split tunneling

* allow communication to a specific destination IP address, or IP range, to not be routed through
  your WireGuard VPN server (e.g. allow `123.123.1.2` and `123.123.3.0/24`), you might want to use
  [WireGuard allowed IPs
  calculator](https://www.procustodibus.com/blog/2021/03/wireguard-allowedips-calculator/) or
  [`netie`](https://gitlab.com/stephane.tzvetkov/netie):
  ```console
  $  netie -i "0.0.0.0/0, ::/0" -e "123.123.1.2, 123.123.3.0/24"
    > 123.123.2.0/24, 123.123.1.3/32, 123.123.1.0/31, 123.123.1.4/30, 123.123.1.8/29, 123.123.1.16/28, 123.123.1.32/27, 123.123.1.64/26, 123.123.1.128/25, 123.123.0.0/24, 123.123.4.0/22, 123.123.8.0/21, 123.123.16.0/20, 123.123.32.0/19, 123.123.64.0/18, 123.123.128.0/17, 123.122.0.0/16, 123.120.0.0/15, 123.124.0.0/14, 123.112.0.0/13, 123.96.0.0/12, 123.64.0.0/11, 123.0.0.0/10, 123.128.0.0/9, 122.0.0.0/8, 120.0.0.0/7, 124.0.0.0/6, 112.0.0.0/5, 96.0.0.0/4, 64.0.0.0/3, 0.0.0.0/2, 128.0.0.0/1, ::/0
  ```
  ```console
  $ sudo vi $HOME/.config/wireguard/wg0.conf
    > ...
    > [Peer]
    > ...
    > AllowedIPs = 123.123.2.0/24, 123.123.1.3/32, 123.123.1.0/31, 123.123.1.4/30, 123.123.1.8/29, 123.123.1.16/28, 123.123.1.32/27, 123.123.1.64/26, 123.123.1.128/25, 123.123.0.0/24, 123.123.4.0/22, 123.123.8.0/21, 123.123.16.0/20, 123.123.32.0/19, 123.123.64.0/18, 123.123.128.0/17, 123.122.0.0/16, 123.120.0.0/15, 123.124.0.0/14, 123.112.0.0/13, 123.96.0.0/12, 123.64.0.0/11, 123.0.0.0/10, 123.128.0.0/9, 122.0.0.0/8, 120.0.0.0/7, 124.0.0.0/6, 112.0.0.0/5, 96.0.0.0/4, 64.0.0.0/3, 0.0.0.0/2, 128.0.0.0/1, ::/0
    > ...
  ```

### App routing
> Application based split tunneling

See:

* <https://www.reddit.com/r/selfhosted/comments/b5pymz/split_tunnel_in_wireguard/>
* <https://www.wireguard.com/netns/>

### Avoid DNS leak

After setting up your VPN connection for the first time, you might want to check for DNS leaks. In
order to do this, Go to <https://www.browserleaks.com> or <https://www.ipleak.net> (both perform
IPv4 and IPv6 check) with your browser. Only your server IP address should show up (test without
and without the VPN), not your client IP address. The DNS servers should be the ones you selected
during the setup, not your client IP address nor your ISP's DNS servers' addresses.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
