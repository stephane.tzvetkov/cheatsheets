---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `dhcpcd`

`dhcpcd` (DHCP Client Daemon) is a DHCP (and DHCP v6) client. It is currently the most feature-rich
open source DHCP client. `dhcpcd` (DHCP client daemon) is not to be confused with Internet Systems
Consortium's `dhcpd` (DHCP (server) Daemon).

???+ Note "Reference(s)"
    * <https://roy.marples.name/projects/dhcpcd/>
    * <https://wiki.archlinux.org/title/Dhcpcd>
    * <https://wiki.gentoo.org/wiki/Dhcpcd>
    * <https://wiki.gentoo.org/wiki/Network_management_using_DHCPCD>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

!!! Note ""

    === "OpenRC"
        ```console
        # pacman -S dhcpcd-openrc
        # rc-update add dhcpcd
        ```

    === "Runit"
        ```console
        # pacman -S dhcpcd-runit
        # ln -s /etc/runit/sv/dhcpcd /run/runit/service
        ```

    === "SysVinit"
        ```console
        # pacman -S dhcpcd-s6
        # s6-rc-bundle -c /etc/s6/rc/compiled add default dhcpcd
        ```

    === "SystemD"
        **TODO**
        ```console
        ```

**TODO** : `dhcpcd` for complete network stack management (see
<https://wiki.gentoo.org/wiki/Network_management_using_DHCPCD>).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
