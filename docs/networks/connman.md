---
tags:
  - Networks
  - Network Managers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# ConnMan

ConnMan is a CLI network manager designed for use with embedded devices and fast resolve times. It
is modular through a plugin architecture, but has native DHCP and NTP support.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/ConnMan>
    * <https://www.linuxsecrets.com/archlinux-wiki/wiki.archlinux.org/index.php/Connman.html>
    * <https://git.kernel.org/pub/scm/network/connman/connman.git/tree/doc>
    * <https://wiki.archlinux.org/index.php/ConnMan#Troubleshooting>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)
* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [WiFi](#wifi)
    * [Ethernet](#ethernet)
    * [Troubleshooting](#troubleshooting)
    * [ConnMan `dmenu`](#connman-dmenu)

<!-- vim-markdown-toc -->

---
## TODO

* pure CLI interfaces definition (e.g. `eduroam`:
  <https://wiki.archlinux.org/index.php/ConnMan#Connecting_to_eduroam_(802.1X))>

* `ncurses` client: <https://github.com/eurogiciel-oss/connman-json-client>

* GUI client: `connman-gtk`


---
## Install

Very useful optional dependencies to install:

!!! Tip ""
    * [`wpa_supplicant`](./wpa_supplicant.md)
    * [`bluez`](./bluez.md)
    * [`openvpn`](./openvpn.md)
    * [`wireguard`](./wireguard.md)?

Then, install `connman`:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-misc/connman
        ```

    === "pacman"
        ```console
        # pacman -S connman
        ```

        !!! Tip "For Artix users"
            * **If** using `openrc`:
            ```console
            # pacman -S cronie connman-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S cronie connman-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S cronie connman-s6
            ```

    === "apt"
        ```console
        # apt install connman
        ```

    === "yum"
        ```console
        # yum install connman
        ```

    === "dnf"
        ```console
        # dnf install connman
        ```

The previous installation step allow to control `connman` with `connmanctl`, but you might want a
user interface and not just a `cli` tool:

!!! Tip ""
    * [`connman-gtk`](https://github.com/jgke/connman-gtk) (GTK client for `connman`)
    * [`cmst`](https://github.com/andrew-bibb/cmst) (Qt GUI for `connman`)
    * [`connman-ncurses`](https://github.com/eurogiciel-oss/connman-json-client) (Simple `ncurses`
      UI for `connman`)
    * [`ConnMan-UI`](https://github.com/tbursztyka/connman-ui) (GTK 3 client applet for `connman`)
    * [`connman_dmenu`](https://github.com/taylorchu/connman_dmenu) (Client/front-end for `dmenu`)
    * [`LXQt-Connman-Applet`](https://github.com/lxqt/lxqt-connman-applet) (LXQt desktop panel
      applet for `connman`)
    * [`gnome-extension-connman`](https://github.com/jgke/gnome-extension-connman) (Gnome 3
      extension for `connman`)

Now, add `connman` to your init manager:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add connmand default
        # /etc/init.d/connmand start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/connmand /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/connmand /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/connmand /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up connmand
        ```

    === "SysVinit"
        ```console
        # service connmand start
        # chkconfig connmand on
        ```

    === "SystemD"
        ```console
        # systemctl enable connmand
        # systemctl start connmand
        ```


---
## Config

* If you want to prefer Ethernet over wireless:
  ```console
  # vi /etc/connman/main.conf
      > ...
      > [General]
      > ...
    ~ > PreferredTechnologies = ethernet,wifi
      > ...
  ```

* If you don't want to allow to be connected to both Ethernet and wireless at the same time (in
  order to have only a single unambiguous connection active at a time):
  ```console
  # vi /etc/connman/main.conf
      > ...
      > [General]
      > ...
    ~ > SingleConnectedTechnology=true
      > ...
  ```

* If you don't want to let `connman` change the transient host name on a per network basis (which
  can cause problems with X authority):
  ```console
  # vi /etc/connman/main.conf
    > ...
    > [General]
    > ...
  ~ > AllowHostnameUpdates=false
    > ...
  ```

*   If something like Docker is creating virtual interfaces, `connman` may attempt to connect to
    one of these instead of your physical adapter if the connection drops. A simple way of avoiding
    this is to blacklist the interfaces you do not want to use.

    `connman` will by default blacklist interfaces starting with `vmnet`, `vboxnet`, `virbr` and
    `ifb`, so those need to be included in the new blacklist as well. Blacklisting interface names
    is also useful to avoid a race condition where `connman` may access `eth#` or `wlan#` before
    `systemd`/`udev` can change it to use a Predictable Network Interface Names like `enp4s0`.
    Blacklisting the conventional (and unpredictable) interface prefixes makes `connman` wait until
    they are renamed.

    ```console
    # vi /etc/connman/main.conf
      > ...  [General] ...
    ~ > NetworkInterfaceBlacklist=vmnet,vboxnet,virbr,ifb,ve-,vb-,docker,veth,eth,wlan
      > ...
    ```


---
## Use

* Shows a list of all technology types existing on the system (e.g. `ethernet`, `wifi`,
  `bluetooth`...) , their properties and their status:
  ```console
  $ connmanctl technologies
  ```

### WiFi

* Enable `wifi`, note that it powers on to the WiFi, but doesn't connect unless there is a service
  with auto connect set to True:
  ```console
  $ connmanctl enable wifi
  ```

* Connect to a non password protected WiFi access point:
  ```console
  $ connmanctl scan wifi && connmanctl services
    > ...
  $ connmanctl connect wifi_dc85de828967_4d6568657272696e_managed_none
  ```

* Connect to a password protected WiFi access point:
  ```console
  $ connmanctl scan wifi && connmanctl services
    > ...
  $ connmanctl # enter interactive mode
    > agent on
    > connect wifi_dc85de828967_38303944616e69656c73_managed_psk
    > exit
  ```

* After the first connection, a `wifi` service is saved in `/var/lib/connamn/wifi_*`. It's settings
  can be modified, e.g. the auto connection setting:
  ```console
  # vi /var/lib/connamn/wifi_.../settings
    > ...
  ~ > AutoConnect=true
    > ...
  ```

* Disconnect:
  ```console
  $ connmanctl disconnect wifi_dc85de828967_38303944616e69656c73_managed_psk
  ```

* Note that some WiFi connection points may need additional steps, like for
  [`eduroam`](https://wiki.archlinux.org/index.php/ConnMan#Connecting_to_eduroam_(802.1X)).

### Ethernet

* Ethernet connections are setup automatically. After the first connection, an `ethernet` service
  is saved in `/var/lib/connamn/ethernet_*`. It's settings can be modified, e.g. the
  auto connection setting:
  ```console
  # vi /var/lib/connamn/ethernet_.../settings
    > ...
  ~ > AutoConnect=true
    > ...
  ```

* Sometimes, multiple `ethernet` interfaces are needed with a single `ethernet` mac address
  (typically depending on the network you want to connect to). This can be achieved like so:
  ```console
  $ connmanctl disable ethernet
  $ cd /var/lib/connman
  ```
  Create two network interfaces:
  ```console
  $ sudo cp ethernet_00249b2c785a_cable ethernet_00249b2c785a_cable_network_name_1
  $ sudo mv ethernet_00249b2c785a_cable ethernet_00249b2c785a_cable_network_name_2
  ```
  Change the settings of those network interfaces the way you want:
  ```console
  $ sudo vi /var/lib/connamn/ethernet_00249b2c785a_cable_network_name_1/settings
    > ...
  $ sudo vi /var/lib/connamn/ethernet_00249b2c785a_cable_network_name_2/settings
    > ...
  ```
  Select one of those interfaces with a symbolic link:
  ```console
  $ sudo ln -s ethernet_00249b2c785a_cable_network_name_1 ethernet_00249b2c785a_cable
  $ connmanctl enable ethernet
  ```

### Troubleshooting

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/ConnMan#Troubleshooting>

### ConnMan `dmenu`

* TODO: `connman` `dmenu` for Ethernet interfaces
* TODO: `connman` `dmenu` for WiFi selection
* see <https://github.com/march-linux/connman_dmenu>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
