---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# cron

cron is a daemon that runs scheduled tasks based on input from the command `crontab`. It
accomplishes this task by waking up every minute and checking to see if there are any cron jobs to
run in any of the user `crontabs`.

???+ Note "Reference(s)"
    * <https://crontab.guru/>
    * <https://wiki.gentoo.org/wiki/Cron>
    * <https://wiki.archlinux.org/index.php/Cron>
    * <https://wiki.archlinux.org/index.php/Cron#Cronie>
    * <https://wiki.archlinux.org/index.php/Cron#Cronie_2>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [`anacron`](#anacron)

<!-- vim-markdown-toc -->

---
## Install

There are multiple cron implementations, in this cheat sheet I will use `cronie` (fork of
`vixie-cron`, because according to the [Gentoo Wiki](https://wiki.gentoo.org/wiki/Cron#cronie):
`vixie-cron` is a bit outdated while still maintained by Fedora).

First, check if `cronie` is already installed:

!!! Note ""

    === "emerge"
        ```console
        $ emerge -p cronie
        ```

    === "pacman"
        ```console
        $ pacman -Q | grep cron
        ```

    === "nix"
        ```console
        $ nix-env -q cron
        ```

        !!! Warning "Warning to NixOS users"
            There is no `cronie` nix package, only `cron` (which is `vixie-cron`), so for the rest
            of this cheat sheet: remember to replace any `cronie` reference by `cron`. But be
            warned that the following sections may not correctly apply to `vixie-cron`.


    === "apt"
        ```console
        $ apt -qq list cronie
        ```

    === "yum"
        ```console
        $ yum list installed cronie
        ```

    === "dnf"
        ```console
        $ dnf list installed cronie
        ```

If not, install it ...

!!! Note ""

    === "apk"
        ```console
        # apk add cronie
        ```

    === "apt"
        ```console
        # apt install cronie
        ```

    === "dnf"
        ```console
        # dnf install cronie
        ```

    === "emerge"
        ```console
        # emerge -a sys-process/cronie
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.cron
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.cron
            ```

    === "pacman"
        ```console
        # pacman -S cronie
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S cronie cronie-dinit
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S cronie cronie-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S cronie cronie-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S cronie cronie-s6
            ```

    === "yum"
        ```console
        # yum install cronie
        ```

    === "xbps"
        ```console
        # xbps-install -S cronie
        ```

    === "zypper"
        ```console
        # zypper install cronie
        ```

Then, add it to your init system and start it:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add cronie default
        # rc-service cronie start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/cronie /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/cronie /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/cronie /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up cronie
        ```

    === "SysVinit"
        ```console
        # service cronie start
        # chkconfig cronie on
        ```

    === "SystemD"
        ```console
        # systemctl enable cronie
        # systemctl start cronie
        ```


---
## Config

`cronie` doesn't need to be configured anymore.

Old `cronie` config:
```console
# vi /etc/crontab
    > # Global variables
    > SHELL=/bin/bash
    > PATH=/sbin:/bin:/usr/sbin:/usr/bin
    > MAILTO=root
    > HOME=/
    >
    > # check scripts in cron.hourly, cron.daily, cron.weekly and cron.monthly
    > 59  *  * * *    root    rm -f /var/spool/cron/lastrun/cron.hourly
    > 9  3  * * *     root    rm -f /var/spool/cron/lastrun/cron.daily
    > 19 4  * * 6     root    rm -f /var/spool/cron/lastrun/cron.weekly
    > 29 5  1 * *     root    rm -f /var/spool/cron/lastrun/cron.monthly
    > */10  *  * * *  root    test -x /usr/sbin/run-crons && /usr/sbin/run-crons
```


---
## Use

* Edit your `crontab` (`crontabs` are located in `/var/spool/cron/crontabs`):
  ```console
  $ crontab -e # edit the crontab of the current user
  $ sudo crontab -e # edit the crontab of the root user
  ```

* Add a line to the wanted `crontab` according to this format:
  ```console
  * * * * *  command_or_script_to_execute
  - - - - -
  | | | | |
  | | | | +- day of week (0 - 7) (where sunday is 0 and 7)
  | | | +--- month (1 - 12)
  | | +----- day (1 - 31)
  | +------- hour (0 - 23)
  +--------- minute (0 - 59)
  ```

* E.g. execute a command every ...
  ```console
  */15 * * * * /home/user/command.sh  # ... every 15 min
  0 0 * * * /home/user/command.sh     # ... every midnight
  5 8 * * 6 /home/user/command.sh     # ... every Saturday at 8:05 AM
  ```

* Check the file syntax of your `crontab`:
  ```console
  $ crontab -T /var/spool/cron/username
  ```

* Delete your `crontab`:
  ```console
  $ crontab -r
  ```

* By default one can check cron executions by reading the logs in `/var/log/messages` and searching
  for `cron`.


### `anacron`

`anacron` is not a cron daemon, it is something that usually works in conjunction with one.  It
executes commands at intervals specified in days and it does not assume that the system is running
continuously; it will run jobs that were missed while the system was down. `anacron` usually relies
on a cron daemon to run it each day.

By default `cronie` includes `anacron` functionalities (note to Gentoo users: `cronie` includes
`anacron` functionalities when the default `anacron` use flag is set).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
