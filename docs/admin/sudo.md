---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `sudo`

`sudo` allows a system administrator to delegate authority to give certain users - or groups of
users - the ability to run commands as root or another user while providing an audit trail of the
commands and their arguments.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Sudo>
    * <https://wiki.archlinux.org/index.php/Sudo>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-admin/sudo
        ```

    === "pacman"
        ```console
        # pacman -S sudo
        ```

    === "apt"
        ```console
        # apt install sudo
        ```

    === "yum"
        ```console
        # yum install sudo
        ```

    === "dnf"
        ```console
        # dnf install sudo
        ```


---
## Config

* Configure `sudo`, e.g. to give the user `resu` the same privileges as root:
```console
# sudo EDITOR=vi visudo # edit the sudoer file
    > ...
    > # time sudo will remember a password (-1 for infinite):
    > Defaults:resu timestamp_timeout=15
    >
    > # Root privilege specification
    > root ALL=(ALL:ALL) ALL
    >
    > # Members of the admin group may gain root privileges
    > %admin ALL=(ALL) ALL
    >
    > # Allow members of group sudo to execute any command
    > %sudo ALL=(ALL:ALL) ALL
    >
    > # give a user the same privileges as root:
    > resu ALL=(ALL) ALL
    > ...
```

* Add a user to the `sudo` group
```console
$ su
# grep "sudo" /etc/group || groupadd sudo # create the sudo group if it doesn't exists
# usermod -a -G sudo resu # add "resu" to sudo group ("resu" will have to logout and log back in)
# exit
```

* Print the current `sudo` configuration:
```console
$ sudo -ll
$ sudo -lU resu # print sudo config for a specific user
```

* Give a user `sudo` rights, without password check, for a specific directory (e.g. `~/bin/`):
```console
$ sudo EDITOR=vi visudo
    > ...
    > resu ALL=(ALL) ALL
  + > resu ALL=(ALL) NOPASSWD: /home/resu/bin/*
    > ...
```

* Give a user `sudo` rights, without password check, anywhere:
```console
$ sudo EDITOR=vi visudo
    > ...
  ~ > resu ALL=(ALL) NOPASSWD: ALL
    > ...
```


---
## Use

Now `sudo` can be prepend to any command in order for this command to be executed as root.

* Create a file as root:
```console
$ sudo touch /tmp/test
```

* Enter the root shell as if you logged in with the root user:
```console
$ sudo -i
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
