---
tags:
  - System Administration
  - Sysadmin
  - Monitoring
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Cockpit

Cockpit is a system administration tool (sponsored by Red Hat) that provides a user interface for
monitoring and administering servers through a web browser. It allows you to monitor current values
and adjust limits on system resources, control life cycle on container instances, and manipulate
container images.

???+ Note "Reference(s)"
    * <https://cockpit-project.org/>
    * <https://wiki.archlinux.org/title/Cockpit>
    * <https://cockpit-project.org/documentation.html>
    * <https://web.archive.org/web/20220814124833/https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html-single/getting_started_with_cockpit/index>
    * <https://www.youtube.com/watch?v=mjhgR6BKFvQ>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
