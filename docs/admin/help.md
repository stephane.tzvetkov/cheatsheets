---
tags:
  - System Administration
  - Sysadmin
  - Help Tools
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# help

Here is a list of popular tools in order to get some help about any CLI commands.

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`man`](#man)
* [`tldr`](#tldr)
    * [Install](#install)
    * [Use](#use)
* [`cheat.sh`](#cheatsh)
    * [`cht.sh` client](#chtsh-client)
    * [(Neo)vim plugin](#neovim-plugin)
* [`cheatsheets`](#cheatsheets)
* [`navi`](#navi)
* [`cheat`](#cheat)
* [`kb`](#kb)
* [`eg`](#eg)

<!-- vim-markdown-toc -->


---
## `man`

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Man_page>
    * <https://man.archlinux.org/>
    * <https://man7.org/linux/man-pages/index.html>
    * <https://linux.die.net/man/>
    * <https://wiki.gentoo.org/wiki/Man_page>
    * <https://en.wikipedia.org/wiki/Man_page>

Just run the following command in order to know everything there is to know about `man` (the
default interface to your Linux system reference manuals): `$ man man`.


---
## `tldr`

The `tldr-pages` project is a collection of community-maintained help pages for command-line tools,
that aims to be a simpler, more approachable complement to traditional man pages.

???+ Note "Reference(s)"
    * <https://github.com/tldr-pages/tldr>
    * <https://tldr.sh/>


### Install

!!! Note ""

    === "emerge (with overlay)"
        ```console
        # emerge --ask --noreplace eselect-repository
        # eselect repository enable guru
        # emaint sync -r guru
        # echo '*/*::guru ~amd64' >> /etc/portage/package.accept_keywords
        # emerge -a app-text/tldr
        ```

    === "pacman"
        ```console
        # pacman -S tldr
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.tldr
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.tldr
            ```

    === "apt"
        ```console
        # apt install tldr
        ```

    === "yum"
        ⚠️ **[Install the EPEL repository](../distros/redhat-based/epel.md) as a prerequisite!** ⚠️
        ```console
        # yum install tldr
        ```

    === "dnf"
        ```console
        # dnf install tldr
        ```

### Use

* Get typical usages of a command, e.g. the `tar` command:
```console
$ tldr tar
```

* Show the `tar` `tldr` page for Linux:
```console
$ tldr -p linux tar
```

* Get help for a Git sub command (e.g. the `git checkout` sub command):
```console
$ tldr git-checkout
```

* Update local pages (if the client supports caching):
```console
$ tldr -u
```


---
## `cheat.sh`

A unified access to community driven cheat sheets repositories.

???+ Note "Reference(s)"
    * <https://cheat.sh/>
    * <https://github.com/chubin/cheat.sh>

* Presentation of `cheat.sh`
```console
$ curl cheat.sh
```

* How to use `cheat.sh`:
```console
$ curl cheat.sh/:help
```

* More detailed introduction to `cheat.sh`:
```console
$ curl cheat.sh/:intro
```

* Each programming language topic has the following subtopics:
```console
$ curl cheat.sh/lua/:learn
$ curl cheat.sh/lua/:list
```

* Search for a specific keyword (e.g. `Battery`) in a specific cheat sheet (e.g. `MegaCli`):
```console
$ curl cheat.sh/MegaCli~Battery
    > # Battery backup information
    > MegaCli -AdpBbuCmd -aALL
```

* Search for a specific keyword (e.g. `Battery`) across all cheat sheets:
```console
$ curl cheat.sh/~Battery
    > ...
```

### `cht.sh` client

Though it's perfectly possible to access `cheat.sh` using `curl` (or any other HTTP/HTTPS client)
alone, there is a special client, that has several advantages comparing to plain curling: `cht.sh`.

To install the client in `~/bin`:
```console
$ curl https://cht.sh/:cht.sh > ~/bin/cht.sh
$ chmod +x ~/bin/cht.sh
```

Queries look the same, but you can separate words in the query with spaces, instead of `+` as when
using curl, what looks more natural:
```console
$ cht.sh python zip lists
```

### (Neo)vim plugin

See `$ curl cheat.sh/:vim` (and <https://github.com/dbeniamine/cheat.sh-vim>).


---
## `cheatsheets`

TL;DR for developer documentation - a nice collection of cheat sheets to read on
[`devhints.io`](https://devhints.io/).

???+ Note "Reference(s)"
    * <https://devhints.io>
    * <https://github.com/rstacruz/cheatsheets>


---
## `navi`

???+ Note "Reference(s)"
    * <https://github.com/denisidoro/navi>

**TODO**


---
## `cheat`

???+ Note "Reference(s)"
    * <https://github.com/cheat/cheat>

**TODO**


---
## `kb`

???+ Note "Reference(s)"
    * <https://github.com/gnebbia/kb>

**TODO**


---
## `eg`

???+ Note "Reference(s)"
    * <https://github.com/srsudar/eg>

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
