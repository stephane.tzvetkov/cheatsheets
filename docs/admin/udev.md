---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `udev` - `eudev`

TODO

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Udev>
    * <https://wiki.gentoo.org/wiki/Eudev>
    * <https://wiki.archlinux.org/index.php/Udev>
    * <https://bbs.archlinux.org/viewtopic.php?pid=1329375#p1329375>
    * <https://www.youtube.com/watch?v=nSI7M93uTEc>


---
## Table of content


<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)

<!-- vim-markdown-toc -->

---
## TODO

* `send-notify` alert on low power
* `send-notify` alert on screen connection


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
