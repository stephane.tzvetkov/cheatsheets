---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# sysctl

`sysctl` is a tool for examining and changing kernel parameters at runtime (package
[`procps-ng`](https://repology.org/project/procps-ng/versions)). `sysctl` is implemented in
`procfs`, the virtual process file system at `/proc/`.

???+ Note "Reference(s)"
    * <https://manned.org/sysctl.8>
    * <https://wiki.archlinux.org/title/sysctl>
    * <https://en.wikipedia.org/wiki/Sysctl>
    * <https://wiki.linuxquestions.org/wiki/Sysctl>
    * <https://www.kernel.org/doc/html/latest/admin-guide/sysctl/index.html>
    * <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/reference_guide/s3-proc-sys-net>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Config

Possible locations for `sysctl` configuration files are:

* `/etc/sysctl.d/*.conf`
* `/run/sysctl.d/*.conf`
* `/usr/local/lib/sysctl.d/*.conf`
* `/usr/lib/sysctl.d/*.conf`
* `/lib/sysctl.d/*.conf`
* `/etc/sysctl.conf`


---
## Use

* List all current kernel parameters:
    ```console
    $ sysctl -a
    ```
    > See <https://www.kernel.org/doc/html/latest/admin-guide/sysctl/index.html> for the
    > documentation about all parameters.

* List all current kernel parameters of a specific parameters group (e.g. `net.ipv6`):
    ```console
    $ sysctl -a --pattern '^net.ipv6'
    ```
    > See <https://www.kernel.org/doc/html/latest/admin-guide/sysctl/net.html> for the
    > documentation about the whole `net` group.

* Print a single/specific parameter (e.g. `fs.file-max`):
    ```console
    $ sysctl fs.file-max
        > fs.file-max = 3270749
    ```

* Apply changes from `sysctl` config file:
    ```console
    $ sudo sysctl -p
    ```

* Modify a specific kernel parameter (e.g. `vm.swappiness`):
    ```console
    $ sysctl -w vm.swappiness=10
    ```

    The change takes effect immediately, but it is not persistent. After a system reboot, the
    default value is loaded.

    To set a parameter permanently, you will need to write the settings to the configuration file.

    Another way to change parameters is to write the settings to the files in the `/proc/sys`
    directory, e.g.:
    ```console
    $ echo 1 > /proc/sys/net/ipv4/ip_forward
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
