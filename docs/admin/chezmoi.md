---
tags:
  - System Administration
  - Sysadmin
  - dotefiles management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# chezmoi

chezmoi helps you manage your personal configuration files
(dotfiles, like ~/.gitconfig)
across multiple machines.

chezmoi provides many features beyond symlinking
or using a bare git repo
including: templates (to handle small differences between machines),
password manager support (to store your secrets securely),
importing files from archives (great for shell and editor plugins),
full file encryption (using gpg or age),
and running scripts (to handle everything else).


???+ Note "Reference(s)"
    * <https://www.chezmoi.io/>
    * <https://github.com/twpayne/chezmoi/tree/master>
    * <https://www.chezmoi.io/user-guide/command-overview/>


---
## Table of contents

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Working across multiple machines](#working-across-multiple-machines)
    * [Working with templates](#working-with-templates)


---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add chezmoi
        ```

    === "apt"
        TODO

    === "dnf"
        TODO

    === "emerge"
        TODO

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.chezmoi
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.chezmoi
            ```

    === "pacman"
        ```console
        # pacman -S chezmoi
        ```

    === "yum"
        TODO

    === "xbps"
        ```console
        # xbps-install -S chezmoi
        ```

    === "zypper"
        ```console
        # zypper install chezmoi
        ```


---
## Config

* Initialize chezmoi:

    ```console
    $ chezmoi init
    ```

* Create yourself a dedicated `dotfile` repository on a Git server,
    and link your local chezmoi to it:

    ```console
    $ chezmoi cd  # change directory to `$HOME/.local/share/chezmoi`
    $ git add .
    $ git commit -m "initial commit"
    
    $ git remote add origin https://gitserver.xyz/$USERNAME/dotfiles.git
    $ git branch -M main
    $ git push -u origin main
    
    $ exit
    ```


---
## Use

* Get help if needed:

    ```console
    $ chezmoi help
    ```

* Check common problems.
    If you encounter something unexpected, run this first.

    ```console
    $ chezmoi doctor
    ```

* Add dotfiles:

    ```console
    $ chezmoi add $HOME/path/to/any/dotfile
    ```

* Edit dotfiles with chezmoi
    (you have 4 different options for doing it):

    1. Use `$ chezmoi edit $FILE`.
        This will open the source file for `$FILE` in your editor.
        Optionally,
        you can use `$ chezmoi edit --apply $FILE`
        to apply the changes when you quit your editor,

    2. Use `chezmoi cd`
        and edit the files in the source directory directly.
        Then run `$ chezmoi apply`
        to make the changes.

    3. Edit the file in your home directory,
        and then either re-add it 
        by running `$ chezmoi add $FILE`
        or `$ chezmoi re-add`.

    4. Edit the file in your home directory,
        and then merge your changes with source state
        by running `$ chezmoi merge $FILE`
        (invoking a merge tool,
        by default `vimdiff`) 

* Show a quick summary of what files would change
    if you ran chezmoi apply:

    ```console
    $ chezmoi status
    ```

* Shows changes that `$ chezmoi apply` would make
    to your home directory:

    ```console
    $ chezmoi diff
    ```

* Apply changes locally
    (i.e. apply what's in `$HOME/.local/share/chezmoi`
    to your local dotfiles):

    ```console
    $ chezmoi apply -v
    ```

* Automatically apply changes whenever a file is saved
    (after editing the file with chezmoi):

    ```console
    $ chezmoi edit --watch $FILE`
    ```

### Working across multiple machines

* Initialize chezmoi from another machine with your dotfiles repository:

    ```console
    $ chezmoi init https://gitserver.xyz/$USERNAME/dotfiles.git
    ```

    (then, just apply changes locally)

* Initialize chezmoi and apply changes
    from another machine with your dorfile repository,
    in a single command:

    ```console
    $ chezmoi init --apply https://gitserver.xyz/$USERNAME/dotfiles.git
    ```

* Pull and apply the latest changes,
    on any machine,
    from your repository:

    ```console
    $ chezmoi update -v
    ```

* Pull the latest changes from your repository
    and see what would change,
    without actually applying the changes:

    ```console
    $ chezmoi git pull -- --autostash --rebase && chezmoi diff
    ```


### Working with templates

* Print the available template data
    (including the full list of variables specified for templates):

    ```console
    $ chezmoi data 
    ```

* Add a file as a template:

    ```console
    $ chezmoi add --template $FILE 
    ```

 * Makes an existing file a template:

    ```console
    $ chezmoi chattr +template $FILE 
    ```

* Print the target contents of a file, without changing the file:

    ```console
    $ chezmoi cat $FILE 
    ```

* Test and debugg templates:

    ```console
    $ chezmoi execute-template '{{ .chezmoi.sourceDir }}'
    $ chezmoi execute-template '{{ .chezmoi.os }}' / '{{ .chezmoi.arch }}'
    $ echo '{{ .chezmoi | toJson }}' | chezmoi execute-template
    $ chezmoi execute-template --init --promptString email=me@home.org < ~/.local/share/chezmoi/.chezmoi.toml.tmpl
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
