---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# System time

**TODO**

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/System_time>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->


---
## Use

**TODO**

* Default way of changing time zone on SystemD:
    ```console
    $ sudo timedatectl set-timezone Region/City
    ```

* Default way of changing time zone on non-SystemD:
    ```console
    $ sudo ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
    ```

* Network time sync:
    * See the [network time synchronization cheat
      sheet](../networks/time/network_time_synchronization.md)

* Set time manually:
    ```console
    $ date # print date
    $ sudo date MMJJhhmmAAAA # set date and time manually (if needed)
    ```

* Set hardware clock (not needed on a VM):
    ```console
    # vi /etc/conf.d/hwclock # make sure that the system clock is indeed UTC
        > clock="UTC"

    # hwclock --show
    # hwclock --systohc # generate /etc/adjtime
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
