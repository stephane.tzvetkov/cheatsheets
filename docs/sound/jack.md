---
tags:
  - Sound Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Jack

TODO

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/JACK_Audio_Connection_Kit>
    * <https://github.com/jackaudio/jackaudio.github.com/wiki>
    * <https://jackaudio.org/>
    * <https://jackaudio.org/faq/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
