---
tags:
  - Sound Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Sound system

WIP

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Sound_system>

---
**ALSA** (The Advanced Linux Sound Architecture) is a part of the kernel that provides sound card
drivers, moreover it's a software framework for high level API development.

**ALSA** Clients:

* `alsamixer`
* `amixer`
* <https://github.com/opensrc/alsa/blob/master/lib/md/AlsaMixers.md>

!!! Note "ALSA reference(s)"
    * <https://www.alsa-project.org/main/index.php/Main_Page>
    * <https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture>
    * <https://wiki.gentoo.org/wiki/ALSA>
    * <https://github.com/opensrc/alsa/blob/master/lib/md/AlsaMixers.md>

**TODO**: dedicated [ALSA cheat sheet](./alsa.md)

---
**PA** (PulseAudio) is a sound server that provides a lot of features on top of the low-level
audio interface ALSA.

**PA** Clients:

* `PulseMixer`
* `PavuControl`
* <https://wiki.archlinux.org/index.php/PulseAudio#Front-ends>

!!! Note "PulseAudio reference(s)"
    * <https://www.freedesktop.org/wiki/Software/PulseAudio/>
    * <https://wiki.archlinux.org/index.php/PulseAudio>
    * <https://wiki.gentoo.org/wiki/PulseAudio>
    * <https://wiki.archlinux.org/index.php/PulseAudio#Front-ends>

!!! Tip "Note"
    `ALSA` can be used alone without `PulseAudio`. For applications only supporting `PulseAudio`:
    [`apulse`](https://repology.org/project/apulse/versions) enable ALSA to be used instead.

**TODO**: dedicated [PulseAudio cheat sheet](./pulseaudio.md)

---
**JACK** TODO

!!! Note "Jack reference(s)"
    * TODO

**TODO**: dedicated [JACK cheat sheet](./jack.md)

---
**GStreamer** TODO

!!! Note "GStreamer reference(s)"
    * TODO

**TODO**: dedicated [GStreamer cheat sheet](./gstreamer.md)

---
**PipeWire** PipeWire is a low-level multimedia framework. It aims to offer capture and playback
for both audio and video with minimal latency and support for PulseAudio, JACK, ALSA and
GStreamer-based applications. TODO

!!! Note "PipeWire reference(s)"
    * <https://pipewire.org/>
    * <https://github.com/PipeWire>
    * <https://github.com/PipeWire/pipewire>
    * <https://wiki.archlinux.org/title/PipeWire>
    * <https://wiki.gentoo.org/wiki/PipeWire>

**TODO**: dedicated [PipeWire cheat sheet](./pipewire.md)

On Arch Linux, installing PipeWire is as easy as:
```console
$ sudo pacman -S pipewire pipewire-pulse pipewire-jack wireplumber
$ systemctl --user start pipewire-pulse
$ pactl info && echo OK || echo KO
```

!!! Tip "Why `wireplumber` and not `pipewire-media-session`?"
    According to Debian wiki: `wireplumber` and `pipewire-media-session` are two possible session
    managers for PipeWire. The one often used by default, `pipewire-media-session`, is primitive,
    and is best when using PipeWire just for its basic functionality like screen sharing. When
    using PipeWire as your system’s sound server, the maintainer recommends installing the more
    advanced `wireplumber` instead.

!!! Tip ""
    You can use [PulseAudio front-ends](https://wiki.archlinux.org/title/PulseAudio#Front-ends)
    (e.g. like `pulsemixer`) for managing PulseAudio thanks to `pipewire-pulse`.



---

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/ALSA>
    * <https://wiki.gentoo.org/wiki/PulseAudio>
    * <https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture>
    * <https://wiki.archlinux.org/index.php/PulseAudio>
    * <https://github.com/opensrc/alsa/blob/master/lib/md/AlsaMixers.md>
    * <https://wiki.archlinux.org/index.php/PulseAudio#Front-ends>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [Headphone Jack not working](#headphone-jack-not-working)
        * [No HDMI sound](#no-hdmi-sound)
        * [Crackeling/popping/stuttering/chopping/skipping/hiccups sound issues](#crackelingpoppingstutteringchoppingskippinghiccups-sound-issues)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        In order to find your audio driver, run this command and search for the "Audio device"
        section:
        ```console
        $ hwinfo | less
        ```
        (E.g. my audio driver is `snd_hda_intel`.)

        Now, a correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # ALSA
            ######
            # Double check here: <https://wiki.gentoo.org/wiki/ALSA#Kernel>
            #
            > Device Drivers --->
            >     <*> Sound card support # Symbol: SOUND [=y]
            >         <*> Advanced Linux Sound Architecture ---> # Symbol: SND [=y]
            >             [*] PCI sound devices  ---> # Symbol: SND_PCI [=y]
            #
            # Here enable the driver for your audio controller ###

            > Device Drivers --->
            >     <*> Sound card support # Symbol: SOUND [=y]
            >         <*> Advanced Linux Sound Architecture ---> # Symbol: SND [=y]
            >             HD-Audio  --->
            >                 <*> HD Audio PCI # Symbol: SND_HDA_INTEL [=y]
            #
            #                 Select a codec or enable all (every option starting with "build")
            #                 and let the generic parse choose the right one:
            #
            >                 [*] Build ...
            >                 [*] ...
            >                 ...
            >                 -*- Enable generic HD-audio codec parser # Symbol: SND_HDA_GENERIC [=y]
            >
            > General setup --->
            >     [*] System V IPC # Symbol: SYSVIPC [=y]

            # If the system has more than 8 sound outputs (Each HDMI output on a GPU will count as one),
            # the max number of sound cards will need to be increased:
            #
            > Device Drivers --->
            >     <*> Sound card support # Symbol: SOUND [=y]
            >         <*> Advanced Linux Sound Architecture ---> # Symbol: SND [=y]
            >             [*] Dynamic device file minor numbers # Symbol: SND_DYNAMIC_MINORS [=y]
            >             (32) Max number of sound cards # Symbol: SND_MAX_CARDS [=32]
            ######

            # PA
            ####
            # Double check here: <https://wiki.gentoo.org/wiki/PulseAudio#Kernel>
            # And here: <https://wiki.gentoo.org/wiki/PulseAudio#Permissions>

            # For motherboards containing Intel HDA sound cards, use the following kernel option for
            # improved power-saving:
            #
            > Device Drivers  --->
            >     <*> Sound card support # Symbol: SOUND [=y]
            >         <*> Advanced Linux Sound Architecture ---> # Symbol: SND [=y]
            >             (2048) Pre-allocated buffer size for HD-audio driver # Symbol: SND_HDA_PREALLOC_SIZE [=2048]

            # CONFIG_HIGH_RES_TIMERS is needed to avoid (snd_pcm_recover) underrun errors and degraded audio
            # when some applications are using PA:
            #
            > General setup  --->
            >     Timers subsystem  --->
            >         [*] High Resolution Timer Support # Symbol: HIGH_RES_TIMERS [=y]

            # PA uses udev and ConsoleKit to dynamically give access to the soundcards to the
            # currently "active" nr. When running Systemd this will be handled without needing ConsoleKit.
            # To make this possible, ACLs (Access Control Lists) are required:
            #
            > File systems  --->
            >    Pseudo filesystems  --->
            >       [*] Tmpfs virtual memory file system support (former shm fs) # Symbol: TMPFS [=y]
            >       [*]    Tmpfs POSIX Access Control Lists # Symbol: TMPFS_POSIX_ACL [=y]
            ####
        ```

        !!! Warning ""
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "portage"
        Append `pulseaudio` to the `USE` variable in your `make.conf`:
        ```console
        # euse -E lm_sensors
        # vi /etc/portage/make.conf # check that the use flag "pulseaudio" has been append:
            > ...
            > USE="... pulseaudio"
            > ...
        ```

        !!! Warning ""
            After modifying the `USE` variable in your `make.conf`, don't forget to update the
            system using the following command so the changes take effect: `# emerge --ask
            --changed-use --deep @world`


Install [`alsa-utils`](https://repology.org/project/alsa-utils/versions) (e.g. for `alsamixer`,
`amixer` and other ALSA utility tools):

!!! Note ""

    === "emerge"
        ```console
        # emerge -a alsa-utils
        ```

    === "pacman"
        ```console
        # pacman -S alsa-utils
        ```

    === "apt"
        ```console
        # apt install alsa-utils
        ```

    === "yum"
        ```console
        # yum install alsa-utils
        ```

    === "dnf"
        ```console
        # dnf install alsa-utils
        ```

Optionally install [`PulseAudio`](https://repology.org/project/pulseaudio/versions):

!!! Note ""

    === "emerge"
        ```console
        # emerge -a media-sound/pulseaudio
        ```

    === "pacman"
        ```console
        # pacman -S pulseaudio
        ```

    === "apt"
        ```console
        # apt install pulseaudio
        ```

    === "yum"
        ```console
        # yum install pulseaudio
        ```

    === "dnf"
        ```console
        # dnf install pulseaudio
        ```


---
## Config

!!! Note ""

    === "gentoo-based"
        Check that consolekit is installed with the `acl` use flag:
        ```console
        $ emerge -pv consolekit
            > [ebuild   R    ] sys-auth/consolekit-1.2.1::gentoo  USE="acl ...
        ```

        Check that `pambase` is installed with the "consolekit" use flag:
        ```console
        $ emerge -pv pambase
            > [ebuild   R    ] sys-auth/pambase-20150213-r2::gentoo  USE="consolekit ...
        ```

        ConsoleKit should be running:
        ```console
        # rc-service consolekit status
            > * status: started
        ```

        **If** ConsoleKit isn't running, enable it at boot time:
        ```console
        # rc-update add consolekit default
        ```

        Add this command to your `.xinitrc` (really necessary?):
        ```console
        > ...
        > start-pulseaudio-x11 &
        > ...
        ```

---
## Use

### Troubleshooting

* ALSA troubleshooting
    * <https://www.alsa-project.org/wiki/Documentation>
    * <https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture/Troubleshooting>
    * <https://wiki.gentoo.org/wiki/ALSA#Troubleshooting>
* PulseAudio troubleshooting
    * <https://www.freedesktop.org/wiki/Software/PulseAudio/FAQ/>
    * <https://wiki.archlinux.org/title/PulseAudio/Troubleshooting>
    * <https://wiki.gentoo.org/wiki/PulseAudio#Troubleshooting>
* JACK troubleshooting
    * TODO
* GStreamer troubleshooting
    * TODO
* PipeWire troubleshooting
    * <https://docs.pipewire.org/>
    * <https://wiki.archlinux.org/title/PipeWire#Troubleshooting>
    * <https://wiki.gentoo.org/wiki/PipeWire#Fixes_and_workarounds>

#### Headphone Jack not working

See <https://askubuntu.com/questions/132440/headphone-jack-not-working>.

```console
$ alsactl restore
```

#### No HDMI sound

If you have an HDMI port but no HDMI driver appears when running `$ aplay -L` (or in `alsamixer`)
then this fix might help:
<https://devtalk.nvidia.com/default/topic/1024022/linux/gtx-1060-no-audio-over-hdmi-only-hda-intel-detected-azalia/post/5211273/>.

Create the following script in `locale.d` (see <https://wiki.gentoo.org/wiki//etc/local.d>):
```console
# sudo vi /etc/locale.d/fix-hdmi-audio-with-pci-bit.start
    > #!/bin/sh
    >
    > setpci -s 01:00.0 0x488.l=0x2000000:0x2000000
    > echo 1 > /sys/bus/pci/devices/0000:01:00.0/remove
    > echo 1 > /sys/bus/pci/devices/0000:00:01.0/rescan
```

Then make it executable and add it to OpenRC:
```console
# chmod +x /etc/locale.d/fix-hdmi-audio-with-pci-bit.start
# rc-update add local default
```
Now after reboot you should see your HDMI sound driver in `$ aplay -l`.

#### Crackeling/popping/stuttering/chopping/skipping/hiccups sound issues

Here are a few interesting links:

* <https://askubuntu.com/a/859830>
* <https://ubuntuforums.org/showthread.php?p=4928900>
* <https://ubuntuforums.org/showthread.php?t=766860&p=4816308#post4816308>
* <https://unix.stackexchange.com/a/604756>

Most of this kind of issues can be solved by playing with some options in `/etc/pulse/daemon.conf`,
e.g. like so:
```console
$ sudo vi /etc/pulse/daemon.conf
    > ...
    > ; resample-method = speex-float-1
    > resample-method = src-sinc-best-quality
    >
    > ; realtime-scheduling = yes
    > realtime-scheduling = no # if disabling `tsched` like described below
    > ...
```

And/or by disabling `tsched` in `/etc/pulse/default.pa`:
```console
$ sudo vi /etc/pulse/default.pa
    > ...
    > #load-module module-udev-detect
    > load-module module-udev-detect tsched=0
    > ...
```

After saving changes in those files, don't forget to stop `pulseaudio` (it will restart
automatically whenever an application requires it):
```console
$ sudo pulseaudio --kill
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
