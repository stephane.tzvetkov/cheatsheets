---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# ripgrep-all

**TODO**

```console
$ rga -h
$ rga --help
$ rga --rg-help
```

```console
$ rga -s 'ripgrep-all search with case insensitivity'
```

---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
