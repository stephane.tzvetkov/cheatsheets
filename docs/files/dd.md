---
tags:
  - Files
  - Images
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `dd`

`dd` is a core utility whose primary purpose is to convert and copy a file. Similarly to `cp`, by
default `dd` makes a bit-to-bit copy of the file, but with lower-level I/O flow control features.

!!! Warning "Warning"
    Incorrect use of the `dd` command can wipe any drive connected to the system. Always backup all
    important data.

??? Note "Reference(s)"
    * [`$ man dd`](https://www.linux.org/docs/man1/dd.html)
    * <https://www.cyberciti.biz/faq/unix-linux-dd-create-make-disk-image-commands/>
    * <https://wiki.archlinux.org/title/Dd>
    * <https://wiki.gentoo.org/wiki/Dd>
    * <https://www.gnu.org/software/coreutils/manual/html_node/dd-invocation.html#dd-invocation>
    * <https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/>
    * <https://wiki.archlinux.org/title/Securely_wipe_disk#dd>
    * <https://wiki.archlinux.org/title/Disk_cloning>
    * <https://wiki.archlinux.org/title/Dd#Disk_cloning_and_restore>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Make sure some drives are not mounted](#make-sure-some-drives-are-not-mounted)
* [Installing/cloning an ISO image on a target drive](#installingcloning-an-iso-image-on-a-target-drive)
* [Cloning a disk or a partition from a source drive to a target drive](#cloning-a-disk-or-a-partition-from-a-source-drive-to-a-target-drive)
* [Creating and restoring a disk (or a partition) image](#creating-and-restoring-a-disk-or-a-partition-image)
    * [Creating a disk (or a partition) image from a source drive](#creating-a-disk-or-a-partition-image-from-a-source-drive)
    * [Restoring a disk (or a partition) image to a target drive](#restoring-a-disk-or-a-partition-image-to-a-target-drive)
* [Securely wipe a drive](#securely-wipe-a-drive)
* [Benchmark read and write speed](#benchmark-read-and-write-speed)

<!-- vim-markdown-toc -->


---
## Make sure some drives are not mounted

A lot of `dd` commands require a "source" and/or a "target" drive to **not** be mounted. Here,
source and target drives refers to the disks you want to run `dd` upon.

* Let's say that your system is on the `/dev/sda` device, that the source drive is on the
  `/dev/sds` device and the target drive is on the `/dev/sdt` device, then you might see something
  like bellow when running `lsblk`:
  ```console
  $ lsblk
    > NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
    > sda      8:0    0 123.5G  0 disk
    > ├─sda1   8:1    0   512M  0 part /boot
    > └─sda2   8:2    0 123.0G  0 part /
    > sds      8:16   1  12.3G  0 disk
    > └─ ...
    > sdt      8:32   1  12.3G  0 disk
    > └─ ...
  ```

* Now, depending on what `dd` command you are using, you might have to make sure that the source
  drive (on device `/dev/sds`) and/or the target drive (on device `/dev/sdt`), are not mounted:
  ```console
  $ sudo umount /dev/sds # un-mount source drive
  $ sudo umount /dev/sdt # un-mount target drive
  ```

---
## Installing/cloning an ISO image on a target drive

!!! Tip
    See
    <https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/>

* [Make sure your target drive (e.g. on `/dev/sdt`) is **not**
  mounted](#make-sure-some-drives-are-not-mounted).

* Then, use `dd` to install/clone the ISO image on the target drive:
  ```console
  $ sudo dd if=/path/to/image.iso of=/dev/sdt bs=4M conv=fdatasync status=progress
  ```

!!! Warning "Warning"
    Be extremely careful when using the `dd` command, in particular pay attention to the
    `of=/dev/some-device` part! You really don't want to get the wrong device there or you will end
    up overwriting the wrong disk!

???+ Tip
    A better way to visualize the progression of `dd`, is to use
    [`pv`](https://repology.org/project/pv/versions) (see
    <https://askubuntu.com/questions/215505/how-do-you-monitor-the-progress-of-dd>), e.g. for an
    `.iso` of 42 Go:
    ```console
    $ sudo -i
    # dd if=/path/to/image.iso bs=4M conv=fdatasync | pv --size 42G | dd of=/dev/sdt
    ```
    > (do not try with:
    > ```console
    > $ sudo dd if=/path/to/image.iso bs=4M conv=fdatasync | sudo pv --size 42G | sudo dd of=/dev/sdt
    > ```
    > or the output will look bad)

???+ Tip
    Another way to see the progression of `dd`, is to run:
    ```console
    $ sudo kill -USR1 $(pgrep ^dd$)
    ```

    This will display the progress in the associated `dd` terminal window *without halting the
    process* (by printing to its `stderr` stream).

    If you would like to get regular updates of the dd progress, then enter:
    ```console
    $ watch -n5 'sudo kill -USR1 $(pgrep ^dd$)'
    ```

!!! Note "Note"
    **The dd command might take a long time to complete in order to "sync". Be patient.**

---
## Cloning a disk or a partition from a source drive to a target drive

* [Make sure your source drive (e.g. on `/dev/sds`) *AND* target drive (e.g. on `/dev/sdt`) are
  **not** mounted](#make-sure-some-drives-are-not-mounted).

* Then, use `dd` to clone a source disk (e.g. on device `/dev/sds`) to a target drive (e.g. on
  device `/dev/sdt`):
  ```console
  $ sudo dd if=/dev/sds of=/dev/sdt bs=4M conv=fdatasync status=progress
  ```
  Or to clone a source partition (e.g. on device `/dev/sds2`) to a target partition (e.g. on device
  `/dev/sdt6`):
  ```console
  $ sudo dd if=/dev/sds2 of=/dev/sdt6 bs=4M conv=fdatasync status=progress
  ```

!!! Warning "Warning"
    Be extremely careful when using the `dd` command, in particular pay attention to the
    `of=/dev/some-device` part! You really don't want to get the wrong device there or you will end
    up overwriting the wrong disk!

!!! Note "Note"
    **The dd command might take a long time to complete in order to "sync". Be patient.**


---
## Creating and restoring a disk (or a partition) image

!!! Tip
    See <https://www.cyberciti.biz/faq/unix-linux-dd-create-make-disk-image-commands/>

### Creating a disk (or a partition) image from a source drive

* [Make sure your source drive (e.g. on `/dev/sds`) is **not**
  mounted](#make-sure-some-drives-are-not-mounted).

* Then use `dd` to create a disk image for `/dev/sds`, i.e cloning it to a file (`sds-image.gz`):
  ```console
  $ sudo -i
  # dd if=/dev/sds conv=sync,noerror bs=4M | gzip -c > sds-image.gz
  ```
  Or to create a partition image for `/dev/sds2`, i.e. cloning it to a file (`sds2-image.gz`):
  ```console
  $ sudo -i
  # dd if=/dev/sds2 conv=sync,noerror bs=4M | gzip -c > sds2-image.gz
  ```

* The previous command just cloned the entire hard drive, including the MBR, bootloader, all
  partitions, UUIDs, and data.

### Restoring a disk (or a partition) image to a target drive

* [Make sure your target drive (e.g. on `/dev/sdt`) is **not**
  mounted](#make-sure-some-drives-are-not-mounted).

* In order to restore a disk image (`/path/to/sds-image.gz`) to a target drive:
  ```console
  $ sudo -i
  # gunzip -c /path/to/sds-image.gz | dd of=/dev/sdt
  ```
  Or to restore a partition image (`/path/to/sds2-image.gz`) to a target drive (e.g. `/dev/sdt6`):
  ```console
  $ sudo -i
  # gunzip -c /path/to/sds2-image.gz | dd of=/dev/sdt6
  ```

!!! Warning "Warning"
    Be extremely careful when using the `dd` command, in particular pay attention to the
    `of=/dev/some-device` part! You really don't want to get the wrong device there or you will end
    up overwriting the wrong disk!

!!! Note "Note"
    **The dd command might take a long time to complete in order to "sync". Be patient.**


---
## Securely wipe a drive

!!! Tip
    See <https://wiki.archlinux.org/title/Securely_wipe_disk#dd>


---
## Benchmark read and write speed

!!! Tip
    See <https://wiki.archlinux.org/title/Securely_wipe_disk#dd>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
