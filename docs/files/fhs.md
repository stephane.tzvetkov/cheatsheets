---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# File system Hierarchy Standard

???+ Note "Reference(s)"
    * <https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard>
    * <https://www.geeksforgeeks.org/linux-file-hierarchy-structure/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
