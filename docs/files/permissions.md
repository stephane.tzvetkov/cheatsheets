---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# File permissions

> with `chmod`, `chown`, `chgrp`, `getfacl`, `umask`, ...

WIP

???+ Note "Reference(s)"
    * <https://www.baeldung.com/linux/advanced-file-permissions#setgid>
    * <https://www.thegeekdiary.com/linux-interview-questions-special-permissions-suid-sgid-and-sticky-bit/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Tips](#tips)

<!-- vim-markdown-toc -->

---
## Tips

* recursive folder (only folders and not files) group rights inheritance:
```console
$ find /your/path/here -type d -exec chmod g+s {} \;
```

```console
$  chmod ugo+rwx
```
same as
```console
$  chmod a+rwx
```
same as
```console
$  chmod +rwx
```


```console
$  chmod -R ug+rw
```

TODO

TODO:

* how to
    * `chmod ug+rw && chmod o+r && chmod o-w` recursively if no execution right
    * `chmod ug+rwx && chmod o+rx && chmod o-w` recursively if execution right


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
