---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# shred

`shred` is a tool that overwrite the specified file(s) repeatedly, in order to make it harder for
even very expensive hardware probing to recover the data.


???+ Note "Reference(s)"
    * <https://www.geeksforgeeks.org/shred-command-in-linux-with-examples/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
