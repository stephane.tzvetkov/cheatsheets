---
tags:
  - Files
  - Archives
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Archives commands

**TODO**: split this cheat sheet in dedicated ones (one per command)

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`zip`](#zip)
* [`unzip`](#unzip)
* [`tar`](#tar)
* [auto extraction](#auto-extraction)

<!-- vim-markdown-toc -->

---
## `zip`

```console
$ zip -r directorie_to_zip ziped_directorie
$ zip file_to_zip ziped_file
```

---
## `unzip`

```console
unzip file_to_unzip.zip
```

---
## `tar`

* Extract `.tar.gz`:
```console
$ tar -zxvf data.tar.gz
```

* Extract `.tar.bz2`:
```console
$ tar -jxvf data.tar.bz2
```

* Compress to `.tar.gz`
```console
$ tar -czvf name-of-archive.tar.gz /path/to/directory-or-file
```

---
## auto extraction

* Script automating extraction:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > # archive extraction (usage: `$ ex <file-archive-to-extract>`)
    > #
    > # prerequisites:
    > #   * tar: <https://repology.org/project/tar/versions>
    > #   * unrar: <https://repology.org/project/unrar/versions>
    > #   * unzip: <https://repology.org/project/unzip/versions>
    > ex ()
    > {
    >   if [ -f $1 ] ; then
    >     case $1 in
    >       *.tar.bz2)   tar xjf $1   ;;
    >       *.tar.gz)    tar xzf $1   ;;
    >       #*.bz2)       bunzip2 $1   ;;
    >       *.rar)       unrar x $1   ;;
    >       #*.gz)        gunzip $1    ;;
    >       *.tar)       tar xf $1    ;;
    >       *.tbz2)      tar xjf $1   ;;
    >       *.tgz)       tar xzf $1   ;;
    >       *.zip)       unzip $1     ;;
    >       #*.Z)         uncompress $1;;
    >       #*.7z)        7z x $1      ;;
    >       #*.deb)       ar x $1      ;;
    >       *.tar.xz)    tar xf $1    ;;
    >       #*.tar.zst)   unzstd $1    ;;      
    >       *)           echo "'$1' cannot be extracted via ex()" ;;
    >     esac
    >   else
    >     echo "'$1' is not a valid file"
    >   fi
    > }
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
