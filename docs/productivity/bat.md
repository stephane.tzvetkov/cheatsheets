---
tags:
  - Productivity
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# bat

A cat clone with syntax highlighting and Git integration.

???+ Note "Reference(s)"
    * <https://github.com/sharkdp/bat>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install


!!! Note ""

    === "apk"
        ```console
        # apk add bat
        ```

    === "apt"
        ```console
        # apt install rust-bat
        ```

    === "dnf"
        ```console
        # dnf install rust-bat
        ```

    === "emerge"
        ```console
        # emerge -a sys-apps/bat
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.bat
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.bat
            ```

    === "pacman"
        ```console
        # pacman -S eza
        ```

    === "yum"
        TODO
        ```console
        ```

    === "xbps"
        ```console
        # xbps-install -S bat
        ```

    === "zypper"
        ```console
        # zypper install bat
        ```


---
## Config

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever

    > ...
  + >
  + > # cat/bat
  + > #
  + > # prerequisite:
  + > #   * bat: https://repology.org/project/bat/versions
  + > #
  + > export BAT_THEME="ansi" # change bat theme
  + > #export MANPAGER="sh -c 'col -bx | bat -l man -p'" # colorize pager for man, DEPRECATED
  + > alias cat="bat"
  + > alias fh="find . -exec bat {} +" # "find here" with bat preview
  + > alias fr="find / -exec bat {} +" # "find root" with bat preview

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

TODO:
* <https://github.com/eth-p/bat-extras/blob/master/doc/batgrep.md>
* <https://github.com/eth-p/bat-extras/blob/master/doc/batman.md>
* <https://github.com/eth-p/bat-extras/blob/master/doc/prettybat.md>

---
## Use

```console
$ bat test.md
$ bat *.md
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
