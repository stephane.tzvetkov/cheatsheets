---
tags:
  - Productivity
  - E-mails
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


!!! Warning "Warning"
    **This cheat sheet is DEPRECATED!**

# ProtonMail bridge

The ProtonMail Bridge is an application that encrypts and decrypts email as it enters and leaves
your computer. It allows for full integration of your ProtonMail account with any program that
supports IMAP and SMTP.

!!! Note "Prerequisite(s)"
    * optionally [`tmux`](./tmux.md) (if you want a workaround for a headless way to daemonize the
      bridge)

???+ Note "Reference(s)"
    * <https://protonmail.com/bridge/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [OpenRC ProtonMail bridge CLI service](#openrc-protonmail-bridge-cli-service)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "portage + emerge"
        * Install protonmail bridge:
        ```console
        # echo "mail-mta/protonmail-bridge-bin ~amd64" >> /etc/portage/package.accept_keywords
        # echo "mail-mta/protonmail-bridge-bin protonmail-bridge-EULA" >> /etc/portage/package.license
        # emerge -a protonmail-bridge-bin
        ```
        * Install `pass` for `protonmail-brige` to store secrets:
        ```console
        # emerge -a pass
        ```
        * Also install `gnupg` and `rng-tools` if not already present:
        ```console
        # emerge -a app-crypt/gnupg sys-apps/rng-tools
        ```

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/protonmail-bridge.git
        $ cd protonmail-bridge
        $ makepkg -is # --syncdeps to auto-install deps, --install to install after building
        ```

    === "apt"
        **TODO**
        ```console
        ```

    === "yum"
        **TODO**
        ```console
        ```

    === "dnf"
        **TODO**
        ```console
        ```


---
## Config

Generate a key with your ProtonMail address:
```console
# rngd -r /dev/urandom # start rngd in background to gain enough entropy
$ gpg2 --full-generate-key
# killall rngd # stop rngd

$ gpg2 --list-secret-keys # check the key uid
```

Initiate pass with the key UID (without the "[ultimate]" part or whatever):
```console
$ pass init "your uid"
```

Login into your ProtonMail account and get your `IMAP` and `SMTP` passwords:
```console
$ protonmail-bridge --cli

    > login # log into your protonmail account
    > ...

    > info # print account settings, get your IMAP and SMTP pwd here
    > ...

    > exit
```

There currently is no headless way to daemonize the `protonmail-bridge`, but here's a workaround
using `tmux`:
```console
$ tmux new-session -d -s mail 'protonmail-bridge --cli'
```
(One can add it to his `.login` or `.zlogin` for example)

<br/>

But if a graphic server is running, the `protonmail-bridge` can be daemonize this way:
```console
$ protonmail-bridge --no-window &
```
(One can add it to his `.xinitrc` for example)

### OpenRC ProtonMail bridge CLI service

If you want you can create an OpenRC service in order to start the `protonmail-bridge` daemon (the
headless way with the previous workaround) at startup before login or starting x. If you created
your GnuPG key with the user "username", here is how to do:
```console
# vi /etc/init.d/protonmail-bridge-cli # create an openrc service
    > #!/sbin/openrc-run
    >
    > depend() {
    >     need net
    >     before smartd # optional, here I just want smartd to be able to use neomutt
    > }
    >
    > start() {
    >     ebegin "Starting ProtonMail Bridge CLI Daemon"
    >     /usr/bin/tmux new-session -d -s mail 'protonmail-bridge --cli'
    >     start-stop-daemon --start --exec "/home/username/bin/pm-bridge-cli-daemon" --pidfile /var/run/pm-bridge-cli-daemon.pid --make-pidfile --name="ProtonMail Bridge CLI Daemon"
    >
    >    sleep 2 # wait a moment for the protonmail brigde cli to launch
    >
    >     eend $?
    > }
    >
    > stop() {
    >    ebegin "Stopping ProtonMail Bridge CLI Daemon"
    >    start-stop-daemon --stop --name="ProtonMail Bridge CLI Daemon"
    >    /usr/bin/killall protonmail-bridge
    >    einfo "ProtonMail Bridge CLI Daemon has been well stopped"
    >    eend $?
    > }

# chmod +x /etc/init.d/protonmail-bridge-cli

# vi /home/username/bin/pm-bridge-cli-daemon
    > #!/bin/sh
    >
    > # auto-log to protonmail-bridge:
    > echo "login
    > account.name@protonmail.com
    > [your protonmail account password]
    > [your protonmail mailbox password (optional)]
    > " | protonmail-bridge --cli
    >
    > # run a protonmail-bridge headless daemon:
    > su - username -c "/usr/bin/tmux new-session -d -s mail \"protonmail-bridge --cli\""

# chmod +x /home/username/bin/pm-bridge-cli-daemon

# rc-update add protonmail-bridge-cli default
```


---
## Use

Run the following command for the GUI:
```console
$ protonmail-bridge
```

Or run the following for the CLI:
```console
$ protonmail-bridge -c
```

Once you get the server started and you get the IMAP and SMTP passwords, you should use a email
client with it (like `neomutt`, `mailutils`, `evolution`, `thunderbird`, etc).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
