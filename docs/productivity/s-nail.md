---
tags:
  - Productivity
  - E-mails
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `s-nail`

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/S-nail>
    * <https://serverfault.com/questions/59658/how-do-i-setup-mail-mailx-in-linux-to-mail-to-outside-domain>
    * <https://superuser.com/questions/137461/does-mailx-send-mail-using-an-smtp-relay-or-does-it-directly-connect-to-the-targ>
    * <https://www.binarytides.com/linux-mail-command-examples/>
    * <https://www.binarytides.com/linux-mailx-command/>
    * <https://www.gmass.co/blog/smtp-server-linux/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

`sendmail` is needed ? Not if using external SMTP.


```console
sudo dnf install mailx

sudo yum install mailx

sudo apt install s-nail
```

Then (optional) :
```console
# ln -s /usr/bin/s-nail /usr/bin/mail
```

```console
sudo pacman -S s-nail

sudo ufw allow 25/tcp (open smtp port, 25 by default)
```


On CentOS? <https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8>

On Gentoo?


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
