---
tags:
  - Productivity
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# eza

eza is a modern, maintained replacement for the venerable file-listing command-line program `ls`,
giving it more features and better defaults. It uses colours to distinguish file types and
metadata. It knows about symlinks, extended attributes, and Git. And it’s small, fast, and just one
single binary.

???+ Note "Reference(s)"
    * <https://github.com/eza-community/eza>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)

<!-- vim-markdown-toc -->


---
## Install


!!! Note ""

    === "apk"
        TODO
        ```console
        ```

    === "apt"
        TODO
        ```console
        ```

    === "dnf"
        TODO
        ```console
        ```

    === "emerge"
        ```console
        # emerge -a sys-apps/eza
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.eza
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.eza
            ```

    === "pacman"
        ```console
        # pacman -S eza
        ```

    === "yum"
        TODO
        ```console
        ```

    === "xbps"
        ```console
        # xbps-install -S eza
        ```

    === "zypper"
        ```console
        # zypper install eza
        ```


---
## Config

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever

    > ...
  + >
  + > # ls/eza
  + > #
  + > # prerequisite:
  + > #   * eza https://repology.org/project/eza/versions
  + > #
  + > alias ls="eza -l -F --group-directories-first"
  + > alias l="eza -h -la -F --git --group-directories-first"
  + > alias ll="eza -h -lagi -SF --git --group-directories-first"
  + > alias llt="eza -h -lag -F --git -T -L 2 --group-directories-first"
  + > alias lllt="eza -h -lag -F --git -T -L 3 --group-directories-first"
  + > alias llllt="eza -h -lag -F --git -T -L 4 --group-directories-first"
  + > #alias l="ls -FAXgoh"
  + > alias lss="stat -c '%a %n' *"

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
