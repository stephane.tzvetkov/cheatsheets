---
tags:
  - Productivity
  - Mail User Agent
  - E-mails
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Neomutt

Neomutt is a text based CLI MUA. Here I will detail how to configure it with ProtonMail (with the
`protonmail-bridge`).

**TODO**

!!! Note "Prerequisite(s)"
    * [ProtonMail bridge](./protonmail_bridge.md)

???+ Note "Reference(s)"
    * <https://neomutt.org/guide/>
    * <https://wiki.gentoo.org/wiki/Mutt>
    * <https://wiki.archlinux.org/index.php/Mutt>
    * <https://spaceandtim.es/code/protonmail_mutt/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Neomutt with outlook](#neomutt-with-outlook)
    * [Root and system wild](#root-and-system-wild)
    * [Neomutt as default MTA ?](#neomutt-as-default-mta-)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "portage + emerge"
        Add the `sasl` use flag to neomutt and install neomutt:
        (`sasl`: support for the Simple Authentication and Security Layer)
        ```console
        # vi /etc/portage/package.use/neomutt
            > mail-client/neomutt sasl
        
        # emerge -a neomutt
        ```

    === "pacman"
        ```console
        # pacman -S neomutt
        ```

    === "apt"
        ```console
        # apt install neomutt
        ```

    === "yum"
        **TODO**
        ```console
        ```

    === "dnf"
        **TODO**
        ```console
        ```

**TODO** also install `sendmail` (at least on Ubuntu).


---
## Config

Find your IMAP and SMTP credentials, e.g. with ProtonMail bridge:
```console
$ protonmail-bridge --cli
    > login # login to your protonmail-bridge account
    >   ...
    >
    > info # print your protonmail-bridge account info
    >   ...
    >   Address: 127.0.0.1
    >   IMAP port: 1143
    >   Username: <user.name@protonmail.com>
    >   Password: <protonmail-bridge-password>
    >
    >   SMTP Settings
    >   Address: 127.0.0.1
    >   SMTP port: 1025
    >   Username: <user.name@protonmail.com>
    >   Password: <protonmail-bridge-password>
```

Create a dedicated file to store your email and bridge password:
```console
$ mkdir ~/.config/neomutt
$ vi ~/.config/neomutt/keys
    > set my_user=<user.name@protonmail.com>
    > set my_pass="<protonmail-bridge-password>"
```

Minimal working `neomuttrc` configuration:
```console
$ vi ~/.config/neomutt/neomuttrc

    > source ~/.config/neomutt/keys # sources $my_user and $my_pass variables
    >
    >
    > # Char encoding #
    > # ------------- #
    >
    > # Character set on sent messages:
    > set send_charset="utf-8"
    > # If there is no char set given on incoming messages, it is probably windows:
    > set assumed_charset="iso-8859-1"
    >
    >
    > # IMAP settings #
    > # ------------- #
    >
    > # ("+" substitutes for `folder`)
    > set mbox_type     = Maildir
    > set folder        = imap://127.0.0.1:1143/
    > set record        = +Sent
    > set postponed     = +Drafts
    > set trash         = +Trash
    > set spoolfile     = +INBOX
    > mailboxes         = +INBOX
    > set mail_check    = 5 # seconds
    >
    > # Local protonmail-bridge host server: 127.0.0.1
    > # Protonmail-bridge imap port: 1143
    > set imap_user = $my_user
    > set imap_pass = $my_pass
    >
    > # Store message headers locally to speed things up.
    > # If hcache is a folder, Mutt will create sub cache folders for each account
    > # which may speeds things up even more:
    > set header_cache = ~/.cache/mutt
    >
    > # Store messages locally to speed things up, like searching message bodies.
    > # Can be the same folder as header_cache.
    > # This will cost important disk usage according to your e-mail amount.
    > set message_cachedir = "~/.cache/mutt"
    >
    > # Specify where to save and/or look for postponed messages.
    > set postponed = +[Protonmail]/Drafts
    >
    > # Allow Mutt to open a new IMAP connection automatically.
    > unset imap_passive
    >
    > # Keep the IMAP connection alive by polling intermittently (time in seconds).
    > set imap_keepalive = 300
    >
    > # How often to check for new mail (time in seconds).
    > set mail_check = 120
    >
    >
    > # SMTP settings #
    > # ------------- #
    >
    > set smtp_pass = $my_pass
    > set realname  ='Stéphane Tzvetkov'
    > set from      = $my_user
    > set use_from  = yes
    >
    > # Local protonmail-bridge host server: 127.0.0.1
    > # Protonmail-bridge smtp port: 1025
    > set smtp_url = smtp://$my_user:$smtp_pass@127.0.0.1:1025
    > set ssl_force_tls = yes
    > set ssl_starttls = yes
```
For my full `neomuttrc` config file:
<https://gitlab.com/stephane.tzvetkov/config/blob/master/home/user/config/neomutt/neomuttrc>

### Neomutt with outlook

???+ Note "Reference(s)"
    * <https://www.reddit.com/r/linuxquestions/comments/7dxjcs/using_neomutt_with_outlookcom/>

!!! Warning "Warning"
    Don't forget to set the `certificate_file` option!

### Root and system wild

Note that if you want to use Neomutt as root, you will have to copy your `neomuttrc` file and key
file into `/root/.config/neomutt`.

<br/>

Also note that if you want to use Neomutt system wild (e.g. for some other users) you will have to
copy your `neomuttrc` file and key file into `/etc/neomutt`.

### Neomutt as default MTA ?

Is there a proper way the select Neomutt as a proper MTA ?


---
## Use

To enter the Neomutt CLI, Simply run:
```console
$ neomutt
```

The first time you have to accept (`"a"ccept always`) the SSL Certificate, quit and re open it in
order for all the mailbox to load.

<br/>

One can also easily send an email this way:
```console
$ echo "This is a test." | neomutt -s "Test subject" recipient.name@eg.com
```

### Troubleshooting

If you get the following error when testing Neomutt:
```console
$ echo "This is a test." | neomutt -s "Test subject" recipient.name@eg.com
    > Could not connect to 127.0.0.1 (Connection refused).
    > Could not send the message.
```
It probably means that ProtonMail bridge is not running.

<br/>

If you get the following error when testing Neomutt:
```console
$ echo "This is a test." | neomutt -s "Test subject" recipient.name@eg.com
    > SASL authentication failed
    > Could not send the message.
```
It probably means that you are not logged-in/connected in ProtonMail bridge.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
