---
tags:
  - Productivity
  - Terminal Multiplexers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `tmux`

`tmux` (terminal multiplexer) is a program that enables a number of terminals (or windows), each
running a separate program, to be created, accessed, and controlled from a single screen or
terminal window.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Tmux>
    * <https://wiki.archlinux.org/index.php/Tmux>
    * <https://danielmiessler.com/study/tmux/>
    * <https://leanpub.com/the-tao-of-tmux/read>
    * <https://github.com/tmux-python/tmuxp>
    * <https://gist.github.com/spicycode/1229612> ("the best and greatest `tmux.conf` ever")


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Window management](#window-management)
    * [`tmuxp`](#tmuxp)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a tmux
        ```

    === "pacman"
        ```console
        # pacman -S tmux
        ```

    === "apt"
        ```console
        # apt install tmux
        ```

    === "yum"
        ```console
        # yum install tmux
        ```

    === "dnf"
        ```console
        # dnf install tmux
        ```


---
## Config

If like me, you have some commands and personal scripts in your `.zlogin` or `.login` (e.g. like
 `startx`), you should modify it like so:
```console
# vi ~/.zlogin

    > pgrep 'tmux|startx' || (
    >   some-command &
    >   some-personal-script &
    >   ...
    >   startx &
    > )
```


---
## Use

* Start `tmux`:
  ```console
  $ tmux
  ```

* Detach from `tmux` (without killing session):
  ```console
  > Ctrl-b d
  ```

* Restore `tmux` session:
  ```console
  $ tmux attach
  ```

* Detach an already attached session (great if you are moving devices with different screen
  resolutions)
  ```console
  > tmux attach -d
  ```

* Display session:
  ```console
  > tmux ls
  ```

* Rename session:
  ```console
  > Ctrl-b $
  ```

* Switch session:
  ```console
  > Ctrl-b s
  ```

* List sessions:
  ```console
  $ tmux ls # or `$ tmux list-sessions`
  ```

* Start a shared session:
  ```console
  $ tmux -S /tmp/your_shared_session
  $ chmod 777 /tmp/your_shared_session
  ```

* Help screen (Q to quit):
  ```console
  > Ctrl-b ?
  ```

* Scroll in window:
  ```console
  > Ctrl-b PageUp/PageDown
  ```

* Enter scroll mode:
  ```console
  > Ctrl-b [
  ```
  (then up up/down arrow keys and/or pageup/pagedown)

* Exit scroll mode:
  ```console
  > q
  ```

* Reload configuration file
  ```console
  > Ctrl-b : source-file /path/to/file
  ```

### Window management

* Create window:
  ```console
  > Ctrl-b c
  ```

* Destroy window:
  ```console
  > Ctrl-b x
  ```

* Switch between windows:
  ```console
  > Ctrl-b [0-9]
  or
  > Ctrl-b Arrows
  ```

* Split windows horizontally:
  ```console
  > Ctrl-b %
  ```

* Split windows vertically:
  ```console
  > Ctrl-b "
  ```

* Swap windows
  ```console
  > Ctrl-b :swap-window -s [0-9] -t [0-9]
  ```

### `tmuxp`

`tmuxp` is a `tmux` session manager: see <https://github.com/tmux-python/tmuxp>.

**TODO**

### Troubleshooting

* If you get the following error message after running `$ tmux ls`: `error connecting to
  /tmp/tmux-1000/default (No such file or directory)`, then this is probably because no `tmux`
  session is running (in this case `$ pgrep tmux` should return an error, i.e. a number superior to
  0).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
