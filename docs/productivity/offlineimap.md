---
tags:
  - Productivity
  - E-mails
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `offlineimap`

`offlineimap` is a Python utility to locally sync emails from IMAP servers.

After trying it, I do not use it for those reasons:

* [`offlineimap` brings dotfile madness](https://0x46.net/thoughts/2019/02/01/dotfile-madness/)
  with a `~/.offlineimap` folder and a `~/.offlineimaprc` file (I did not found any way to avoid
  it).

* If you are using `offlineimap` with a cyphered mailbox (like a ProtonMail one) your emails won't
  be cyphered locally after syncing.

* The current available version (`v6.5.6`) has still some annoying bugs.

???+ Note "Reference(s)"
    * <https://spaceandtim.es/code/protonmail_mutt/>
    * <https://wiki.archlinux.org/index.php/OfflineIMAP>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a offlineimap
        ```

    === "pacman"
        ```console
        # pacman -S offlineimap
        ```

    === "apt"
        ```console
        # apt install offlineimap
        ```

    === "yum"
        ```console
        # yum install offlineimap
        ```

    === "dnf"
        ```console
        # dnf install offlineimap
        ```


---
## Config

Configure `offlineimap` (pay attention to the `remoteport`, `remotehost`, `remoteuser` and
`remotepass` settings):
```console
$ vi ~/.offlineimaprc

    > [general]
    > accounts = main
    >
    >
    > [Account main]
    > localrepository = main-local
    > remoterepository = main-remote
    >
    > # full refresh, in min
    > autorefresh = 0.2
    >
    > # quick refreshs between each full refresh
    > quick = 10
    >
    > # update notmuch index after sync
    > #postsynchook = notmuch new # if you intend to use notmuch
    >
    >
    > [Repository main-local]
    > type = Maildir
    > localfolders = ~/.mail
    >
    > # delete remote mails that were deleted locally
    > sync_deletes = no
    >
    >
    > [Repository main-remote]
    > type = IMAP
    > remoteport = 1143 # imap port for protonmail-bridge
    > remotehost = 127.0.0.1 # host for local protonmail-bridge
    > remoteuser = <your-user-email-address>
    > remotepass = <your-user-pass> # if any, otherwise comment this line
    > keepalive = 60
    > holdconnectionopen = yes
    >
    > # delete local mails that were deleted on the remote server
    > expunge = yes
    >
    > # sync only these folders
    > folderfilter = lambda foldername: foldername in ['INBOX', 'Archive', 'Sent']
    >
    > # is broken, but connecting locally to bridge so should be ok
    > ssl = no
```


---
## Use

Simply run:
```console
$ offlineimap
```

### Troubleshooting

Known bug in `offlineimap` `v6.5.6`:

* <http://silas.net.br/tech/apps/offlineimap.html>

If you get the following error: `OperationalError: unable to open database file`

* In my case, it happened because the status file of the folder on `~/.offlineimap` directory was
  corrupted because of a system crash. Deleting this file and setting `status_backend = plain` in
  `~/.offlineimaprc` didn't help.

* The solution I found (very bad, though) was to delete the status file (e.g.:,
  `~/.offlineimap/Account-XXX/LocalStatus/folder_name` and the entire folder in $MAIL directory and
  re sync everything again.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
