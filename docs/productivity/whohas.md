---
tags:
  - Productivity
  - Cross Platform Software Search
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `whohas`

???+ Note "Reference(s)"
    * <https://repology.org/>
    * <https://manpages.debian.org/buster/devscripts/namecheck.1.en.html>
    * <https://pkgs.org/>
    * <http://chocolatey.org/>
    * <http://brew.sh/>
    * <http://www.sabayon.org/>
    * <http://www.angstrom-distribution.org/>
    * <http://www.openembedded.org/>
    * <http://openmoko.org/>
    * <http://openwrt.org>
    * <http://www.emdebian.org/>
    * <http://elinux.org/Embedded_Linux_Distributions>
    * <https://wayback.archive.org/web/http://www.linuxdevices.com/articles/AT2760742655.html>
    * <http://freecode.com/>
    * <http://directory.fsf.org/>
    * <http://en.wikipedia.org/wiki/Comparison_of_free_software_hosting_facilities>
    * <http://www.sisyphus.ru/>
    * <http://savannah.gnu.org>
    * <http://gna.org>
    * <http://sourceforge.net>
    * <http://www.berlios.de>
    * <http://github.com>
    * <http://gitorious.org>
    * <http://www.openhub.net>
    * <http://code.google.com>
    * <http://www.launchpad.net>
    * <http://bitbucket.org>
    * <http://transifex.com>
    * <http://www.perzl.org/aix/>
    * <http://hpux.connect.org.uk/>
    * <https://www.opencsw.org/>
    * <http://gnuwin32.sourceforge.net/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
