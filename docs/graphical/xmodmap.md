---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `xmodmap`

`xmodmap` is a program used to edit and display the keyboard modifier map and key map/table that
are used by client applications to convert event keycodes into keysyms.

???+ Note "Reference(s)"
    - `$ man xmodmap`
    - <https://www.generation-nt.com/reponses/probleme-terminal-entraide-213516.html>
    - <https://wiki.archlinux.org/index.php/Xmodmap>

!!! Note "Note"
    A modifier key is a special key (or combination) on a computer keyboard that temporarily
    modifies the normal action of another key when pressed together. By themselves, modifier keys
    usually do nothing; that is, pressing any of the ++shift++, ++alt++, or ++ctrl++ keys alone
    does not (generally) trigger any action from the computer.

!!! Note "Note"
    There are two types of keyboard values in Xorg: keycodes and keysyms.

    keycode: The keycode is the numeric representation received by the kernel when a key or a mouse
    button is pressed.

    keysym: The keysym is the value assigned to the keycode. For example, pressing ++a++ generates
    the keycode `38`, which is mapped to the keysym `0×61`, which matches a in the ASCII table. The
    keysyms are managed by Xorg in a table of keycodes defining the keycode to keysym relations,
    which is called the key map table. This can be shown by running `xmodmap`.

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Examples](#examples)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a xmodmap
        ```

    === "pacman"
        ```console
        # pacman -S xmodmap
        ```

    === "apt"
        ```console
        # apt install xmodmap
        ```

    === "yum"
        ```console
        # yum install xmodmap
        ```

    === "dnf"
        ```console
        # yum install xmodmap
        ```

---
## Config

Create a `xmodmap` config file, a custom key map/table, and load it when starting your X session:
```console
$ vi ~/.config/Xmodmap
    > ! this is a comment
    > ...

$ vi ~/.xinitrc
    > ...
    > [[ -f ~/.config/Xmodmap ]] && xmodmap ~/.config/Xmodmap &
    > ...
```

---
## Use

Print the current modifiers:
```console
$ xmodmap -pm
```

List keycodes and associated symbols (key map table formatted into expressions):
```console
$ xmodmap -pke
```

!!! Note "Note"
    Each keycode is followed by the keysym it is mapped to. For example `keycode  57 = n N`
    indicates that the keycode `57` is mapped to the lowercase `n`, while the uppercase `N` is
    mapped to keycode `57` + ++shift++.

    Each keysym column in the table corresponds to a particular combination of modifier keys:

    - `Key`
    - `Shift+Key`
    - `Mode_switch+Key`
    - `Mode_switch+Shift+Key`
    - `ISO_Level3_Shift+Key`
    - `ISO_Level3_Shift+Shift+Key`

    Not all keysyms have to be set, but to assign only a latter keysym, use the `NoSymbol` value.

!!! Tip "Tip"
    To identify X11 input keycodes, the `xev` utility can be used. Just run `$ xev` in your
    terminal and enter the key in order to get its details. A lot of information will be output,
    with the following command you can start `xev` and show only the relevant parts:
    ```console
    $ xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
    ```

!!! Tip "Tip"
    There are predefined descriptive keysyms for multimedia keys, e.g. `XF86AudioMute` or
    `XF86Mail`. These keysyms can be found in `/usr/include/X11/XF86keysym.h`. Many multimedia
    programs are designed to work with these keysyms out of the box, without the need to configure
    any third party application. Also, note that available keysyms depend on the `xkeyboard`
    settings (see `$ man xkeyboard-config` for more details).

Test temporary changes, e.g. replace `a` by `e` (`a` keysym will be translated to `e` keysym):
```console
$ xmodmap -e "keysym a = e"
```

Test temporary changes, e.g. assign different keysyms to keycode `24`:
```console
$ xmodmap -e "keycode 24 = e E e E"
```

Test remanent changes, i.e. changes made in the `xmodmap` config file:
```console
$ vi ~/.config/Xmodmap
    > ...
    > keysym a = e
    > keycode 24 = e E e E
    > ...
$ xmodmap ~/.config/Xmodmap
```

### Examples

* Reverse pointer buttons:
  ```console
  $ xmodmap -e "pointer = 3 2 1"
  ```

* Reverse scrolling:
  ```console
  $ xmodmap -e "pointer = 1 2 3 5 4 7 6 8 9 10 11 12"
  ```

* Turn caps lock into control:
  ```console
  $ vi ~/.config/Xmodmap
    > ...
    > clear lock
    > clear control
    > keycode 66 = Control_L
    > add control = Control_L Control_R
    > ...
  ```

* See `xmodmap` man page's example section for more examples.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
