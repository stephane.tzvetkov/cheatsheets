---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Redshift

Redshift is an automatic color temperature adjustment to help reduce monitor eye strain induced by
working in low-light conditions.

???+ Note "Reference(s)"
    - <https://wiki.archlinux.org/index.php/Redshift>
    - <https://wiki.gentoo.org/wiki/Redshift>
    - <http://jonls.dk/redshift/>
    - <https://github.com/jonls/redshift>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a x11-misc/redshift
        ```

    === "pacman"
        ```console
        # pacman -S redshift
        ```

    === "apt"
        ```console
        # apt install redshift
        ```

    === "yum"
        ```console
        # yum install redshift
        ```

    === "dnf"
        ```console
        # dnf install redshift
        ```


---
## Config

E.g. manually configure `redshift` to be located in Paris:
```console
$ mkdir -p ~/.config/redshift
$ vi ~/.config/redshift/redshift.conf
    > [redshift]
    > ; Set the location-provider: 'geoclue2', 'manual'
    > ; type 'redshift -l list' to see possible values.
    > ; The location provider settings are in a different section.
    > location-provider=manual
    >
    > [manual]
    > ; Paris:
    > lat=48.86
    > lon=2.34
```


---
## Use

Invoke `redshift` help:
```console
$ redshift -h
```

Get `redshift` up and running with configured setup (in `~/.config/redshift/redshift.conf`):
```console
$ redshift
```

Get `redshift` up and running with a basic setup:
```console
$ redshift -l LATITUDE:LONGITUDE
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
