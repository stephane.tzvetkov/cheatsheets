---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `xrandr`

!!! Note "Prerequisite(s)"
    * [Xorg graphic server](./xorg.md)

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Xrandr>
    * <https://wiki.archlinux.org/index.php/Xrandr>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)
* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [`xinerama` vs `xrandr`](#xinerama-vs-xrandr)
    * [Troubleshooting](#troubleshooting)
        * [Cursor not moving](#cursor-not-moving)

<!-- vim-markdown-toc -->

---
## TODO

* <https://bbs.archlinux.org/viewtopic.php?pid=1329375#p1329375>


---
## Install

Install `xandr`:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a xrandr
        ```

    === "pacman"
        ```console
        # pacman -S xorg-xrandr
        ```

    === "apt"
        ```console
        ```

    === "yum"
        ```console
        ```

    === "dnf"
        ```console
        ```


---
## Config

Detect devices:
```console
# xrandr -q # to detect devices names and states
```


---
## Use

* Turn the specified monitor output on (e.g. `HDMI-1`) and set the preferred resolution
  automatically (default to max resolution):
```console
$ xrandr --output HDMI-1 --auto
```

* Change the resolution of the specified monitor and change it's refresh rate:
```console
$ xrandr --output HDMI-1 --mode 1920x1080 --rate 60
```

* Duplicate main monitor (e.g. `eDP1`) to second monitor (e.g. `DP1`):
```console
$ xrandr --output DP1 --auto --same-as eDP1 --auto
```

* Turn off main monitor (e.g. `eDP1`) but not second monitor (e.g. `DP1`):
```console
xrandr --output eDP1 --off --output DP1 --auto
```

* Turn on main monitor (e.g. `eDP1`) and second monitor (e.g. `DP1`):
```console
xrandr --output eDP1 --primary --auto --output DP1 --right-of eDP1 --auto.
```

### `xinerama` vs `xrandr`

TODO

### Troubleshooting

#### Cursor not moving

If the cursor moves with `startx` run as root but not when run as a user:

* <https://wiki.gentoo.org/wiki/Non_root_Xorg>

If not, it might be related to this problem:

* <https://www.reddit.com/r/linuxquestions/comments/9l4pah/touchpad_not_working_on_gentoo/>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
