---
tags:
  - Graphical
  - Display Manager
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# awesome

Awesome is a highly configurable, next generation framework window manager for X.

???+ Note "Reference(s)"
    * <https://github.com/awesomeWM/awesome>
    * <https://awesomewm.org/index.html>
    * <https://awesomewm.org/recipes/>
    * <https://awesomewm.org/doc/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
