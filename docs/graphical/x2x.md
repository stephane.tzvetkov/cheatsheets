---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `x2x`

`x2x` allows the keyboard and mouse on one ("from") X display to be used to control another ("to")
X display.

???+ Note "Reference(s)"
    - <https://www.youtube.com/watch?v=umC_zUPGrp4>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install (on target computer only)](#install-on-target-computer-only)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install (on target computer only)

!!! Note ""

    === "apk"
        TODO

    === "apt"
        ```console
        # apt install x2x
        ```

    === "dnf"
        TODO

    === "emerge"
        ```console
        # emerge -a x2x
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.x2x
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.x2x
            ```

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/x2x-git.git
        $ cd x2x-git
        $ makepkg -is
        ```

    === "yum"
        TODO

    === "xbps"
        TODO

    === "zypper"
        TODO


---
## Config

Enable ssh X11 forwarding:
```console
# vi /etc/ssh/ssh_config
    > ...
    > Host *
    >   ...
    >   ForwardX11 yes
    > ...
```


---
## Use

```console
$ ssh -YC username@123.123.123.123 x2x -west -to :0.0
```
> `-Y` option for trusted X11 forwarding (no security control) and `-C` option for data compression


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
