---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Dash

Dash is a small, fast, secure, and POSIX compliant implementation of `/bin/sh`. Dash is not Bash
compatible: be careful of bashisms.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Dash>
    * <https://wiki.archlinux.org/index.php/Pacman#Hooks>
    * <https://aur.archlinux.org/cgit/aur.git/tree/dashbinsh.hook?h=dashbinsh>
    * <https://wiki.gentoo.org/wiki/Dash>
    * <https://github.com/koalaman/shellcheck>
    * <https://wiki.gentoo.org/wiki/Shell>
    * <https://wiki.archlinux.org/index.php/Command-line_shell>
    * <https://wiki.archlinux.org/index.php/Command-line_shell#Changing_your_default_shell>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-shells/dash
        ```

    === "pacman"
        ```console
        # pacman -S dash
        ```

---
## Config

Dash can be _optionally_ defined as the default shell for the system.

In this case, one can identify the scripts containing features of Bash that aren't supported in
Dash ('bashisms'), those scripts will not work without being explicitly pointed to `/bin/bash`.

* Install `checkbashisms`:

    !!! Note ""

        === "apk"
            ```console
            # apk add checkbashisms
            ```

        === "apt"
            TODO
            ```console
            ```

        === "dnf"
            TODO
            ```console
            ```

        === "emerge"
            ```console
            # emerge -a dev-util/checkbashisms
            ```

        === "nix"

            === "on NixOS"
                ```console
                # nix-env -iA nixos.checkbashisms
                ```

            === "on non-NixOS"
                ```console
                # nix-env -iA nixpkgs.checkbashisms
                ```

        === "pacman"
            ```console
            # pacman -S checkbashisms
            ```

        === "yum"
            TODO
            ```console
            ```

        === "xbps"
            ```console
            # xbps-install -S checkbashisms
            ```

        === "zypper"
            TODO
            ```console
            ```


* Check installed scripts with a `#!/bin/sh` shebang that may need modification:
    ```console
    # pacman -S checkbashisms
    $ find /usr/bin/ -type f -perm -o=r -print0 | xargs -0 gawk '/^#!.*( |[/])sh/{printf "%s\0", FILENAME} {nextfile}' | xargs -0 checkbashisms
    $ pacman -Qlq # can be used to list all pacman-installed files.
    ```

Finally, switch the system default shell:

!!! Note ""

    === "Gentoo-based"
        ```console
        # su
        # eselect sh list
        # eselect sh set dash
        # exit
        ```

    === "Arch-based"
        ```console
        # ln -sfT dash /usr/bin/sh
        ```
        <br/>
        Updates of Bash will overwrite `/bin/sh` with the default symlink. To
        prevent this, use the following `pacman` hook, which will relink `/bin/sh`
        after every affected update:
        ```console
        # sudo vi /etc/pacman.d/hooks/dashbinsh.hook
            > [Trigger]
            > Type = Package
            > Operation = Install
            > Operation = Upgrade
            > Target = bash
            >
            > [Action]
            > Description = Re-pointing /bin/sh symlink to dash...
            > When = PostTransaction
            > Exec = /usr/bin/ln -sfT dash /usr/bin/sh
            > Depends = dash
        ```
        (see <https://aur.archlinux.org/cgit/aur.git/tree/dashbinsh.hook?h=dashbinsh>)

---
## Use

* Let's consider the following script:

    ```console
    $ touch yourscript.whatever
    $ echo "echo hello" > yourscript.whatever
    ```
    
    In order to execute it with Dash, run:
    
    ```console
    $ dash yourscript.whatever
    ```
    
    If you want to execute it directly, then make sure your script starts with `#!/bin/dash` (or
    `#!/bin/sh` if `$ ls -la /bin/sh` points to `/bin/dash`), make this script executable (`$ chmod +x
    yourscript.whatever`), and run:
    
    ```console
    $ ./yourscript.whatever
    ```

!!! Tip "Tip"
    You can check your scripts for errors, warnings and suggestions with
    [`shellcheck`](https://github.com/koalaman/shellcheck).

* How to create a dash configuration file like 
  `$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever`:

    ```console
    $ cd
    $ touch .dashrc
    $ vi .profile
        > ...
      + > ENV=$HOME/.dashrc; export ENV
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
