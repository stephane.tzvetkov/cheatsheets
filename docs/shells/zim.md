---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `zim`

`zim` is a Zsh configuration framework with blazing speed and modular extensions.

!!! Warning "Prerequisite(s)"
    * [XDG](./xdg.md)
    * [Zsh](./zsh.md)

???+ Note "Reference(s)"
    * <https://github.com/zimfw/zimfw>
    * <https://github.com/zimfw/zimfw/wiki>
    * <https://zimfw.sh/>
    * <https://zimfw.sh/docs/>
    * <https://zimfw.sh/docs/FAQ/>
    * <https://zimfw.sh/docs/commands/>
    * <https://zimfw.sh/docs/cheatsheet/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

* Define a `zim` home (in `$ZDOTDIR`):
  ```console
  $ vi $HOME/.zshenv
    > ...
    > # ZSH & ZIM
    > export ZDOTDIR="$XDG_CONFIG_HOME/zdotdir"
  + > export ZIM_HOME="$ZDOTDIR/.zim"
    > ...
  $ source $HOME/.zshenv
  ```

* Install `zim`:
  ```console
  $ curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh
  ```

* Start a new terminal or log out (`$ logout`) and log back in.

---
## Config

* You can select an other theme: <https://github.com/zimfw/zimfw/wiki/Themes>

* Change theme (e.g. `s1ck94` instead of `steeef`, and add its `prompt-pwd` dependency):
  ```console
  $ vi $ZDOTDIR/.zimrc
      > ...
      > #
      > # Prompt
      > #
      > # Exposes git repository status information to prompts.
      > zmodule git-info
    + > # Formats the path to the current working directory to be used by prompts.
    + > zmodule prompt-pwd
    ~ > # s1ck94 prompt theme.
    ~ > zmodule s1ck94
      >
      > ...
  $ zimfw install # install prompt-pwd and s1ck94
  $ zimfw uninstall # uninstall no more used modules
  ```
  Now restart your terminal and enjoy your new theme.

* You also can refer to the wiki: <https://github.com/zimfw/zimfw/wiki>

* You might dislike the "fish like auto suggestions", in this case you can disable it:
  ```console
  $ vi $ZDOTDIR/.zimrc
      > ...
      > # Fish-like autosuggestions for Zsh.
    ~ > #zmodule zsh-users/zsh-autosuggestions
      > ...
  ```

* You also might dislike the default Zsh editor key map to Emacs and want it rather to `vi`:
  ```console
  $ vi $ZDOTDIR/.zshrc
      > ...
    + >
    + > #
    + > # Keymap
    + > #
    + >
    + > # Set editor default keymap to vi (`-v`), and not emacs (`-e`)
    + > bindkey -v
  ```

* Finally, if everything is working like you want, you might want to remove the Bash dotfiles of
  your home folder.

---
## Use

**TODO**:

* <https://zimfw.sh/docs/commands/>
* <https://zimfw.sh/docs/cheatsheet/>

* Upgrade/update:
```console
$ zimfw upgrade # upgrade zimfw and compile Zsh files
$ zimfw update # update current modules
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
