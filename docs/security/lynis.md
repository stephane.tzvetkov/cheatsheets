---
tags:
  - Security
  - Auditing Programs
  - Health Scan
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Lynis

Lynis is a open-source security tool for systems running Linux, MacOS, or any Unix based operating
system. It performs an extensive health scan of your systems to support system hardening and
compliance testing.

???+ Note "Reference(s)"
    * <https://cisofy.com/lynis/>
    * <https://linuxsecurity.expert/tools/lynis/>
    * <https://wiki.gentoo.org/wiki/User:Maffblaster/Drafts/lynis>
    * <https://www.geeek.org/audit-securite-serveur-linux-lynis/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
