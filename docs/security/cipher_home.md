---
tags:
  - Security
  - Cryptography
  - Cipher
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Cipher your home directory

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=vSZlN105kvw>
    * <https://www.howtogeek.com/116032/how-to-encrypt-your-home-folder-after-installing-ubuntu/>
    * <https://www.techrepublic.com/article/2-ways-to-better-secure-your-linux-home-directory/>
    * <https://www.addictivetips.com/ubuntu-linux-tips/encrypt-the-home-folder-on-linux/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
