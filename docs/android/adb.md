---
tags:
  - Android
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Android Debug Bridge

ADB is a versatile CLI tool that lets you communicate with an android device. The `adb` command
facilitates a variety of device actions, such as installing and debugging apps, and it provides
access to a Unix shell that you can use to run a variety of commands.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Android>
    * <https://developer.android.com/studio/debug/dev-options#enable>
    * <https://developer.android.com/studio/releases/platform-tools>
    * <https://aur.archlinux.org/packages/android-sdk-platform-tools/>
    * <https://developer.android.com/studio/command-line/adb>
    * <https://technastic.com/adb-shell-commands-list/>

---
## table of contents

<!-- vim-markdown-toc GitLab -->

* [android prerequisites](#android-prerequisites)
* [install](#install)
* [use](#use)
* [troubleshooting](#troubleshooting)
    * [device unauthorized](#device-unauthorized)

<!-- vim-markdown-toc -->


---
## android prerequisites

In your android device, do the following:

* Enable developer options: <https://developer.android.com/studio/debug/dev-options#enable>
  (go in settings -> about phone -> build number -> tap the build number option 7 times)

* Enable ADB: <https://developer.android.com/studio/debug/dev-options#enable>
  (go in settings -> system -> developer options -> Android debugging on)


!!! Tip "access android services"
    go in settings -> system -> developer options -> running services

!!! Tip "access android memory stats"
    go in settings -> system -> developer options -> memory

!!! Tip "access android local terminal app"
    go in settings -> system -> developer options -> local terminal


---
## install

Install the android SDK platform tools:

!!! Note ""

    === "Arch based"
        On Arch based distros, install `android-sdk-platform-tools` with
        [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/android-sdk-platform-tools.git
        $ cd android-sdk-platform-tools
        $ makepkg -si # --syncdeps to auto-install deps, --install to install after building
        ```
        The tools will be found in `/opt/android-sdk/platform-tools/`.

    === "Other"
        Install android-sdk-platform-tools from
        [here](https://developer.android.com/studio/releases/platform-tools), in
        `/opt/android-sdk/platform-tools/`.


---
## use

???+ Note "Reference(s)"
    * <https://technastic.com/adb-shell-commands-list/>

* Now if you connect your android device to your computer via USB, you will be able to run `adb`
  commands after running the ADB server:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb kill-server
  $ sudo /opt/android-sdk/platform-tools/adb start-server
  ```

* E.g. for the android [finder
  application](https://github.com/Seva-coder/Finder#activate-location-automatically).

* Enter ADB shell:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb shell
  ```

* Exit ADB shell:
  ```console
  ...
  > exit
  ```

* Print current user:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb shell am get-current-user
  ```

* Print installed packages:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb shell pm list packages # all apps
  $ sudo /opt/android-sdk/platform-tools/adb shell pm list packages -3 # third party apps
  $ sudo /opt/android-sdk/platform-tools/adb shell pm list packages -s # system apps
  ```

* Print local storage location:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb shell echo $EXTERNAL_STORAGE
  ```

* Extract photos:
  ```console
  $ sudo /opt/android-sdk/platform-tools/adb shell echo $EXTERNAL_STORAGE
    > /sdcard
  $ sudo /opt/android-sdk/platform-tools/adb shell ls $EXTERNAL_STORAGE
    > ...
  $ sudo /opt/android-sdk/platform-tools/adb shell ls /sdcard/DCIM # this location may vary depending on your device
    > ...
  $ sudo /opt/android-sdk/platform-tools/adb pull /sdcard/DCIM/OpenCamera /path/to/your/download/folder
  ```


---
## troubleshooting

### device unauthorized

If you get the following error when running `adb`:
```console
adb: device unauthorized.
This adb server's $ADB_VENDOR_KEYS is not set
Try 'adb kill-server' if that seems wrong.
Otherwise check for a confirmation dialog on your device.
```

* Restart the ADB server: `adb kill-server && adb start-server`

* Next time you use `adb` to access the device, a pop-up will appear on the device asking to allow
  to connect to the `adb` server.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
