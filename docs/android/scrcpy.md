---
tags:
  - Android
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `scrcpy`

`scrcpy` provides display and control of Android devices connected via USB (or over TCP/IP).

???+ Note "Reference(s)"
    * <https://github.com/Genymobile/scrcpy>
    * <https://aur.archlinux.org/packages/scrcpy/>
    * <https://aur.archlinux.org/scrcpy.git>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
