---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `lm_sensors`

`lm_sensors` is a hardware monitoring utility package.

???+ Note "Reference(s)"
    * <https://hwmon.wiki.kernel.org/faq?s%5b%5d=lm_sensors>
    * <https://wiki.gentoo.org/wiki/Lm_sensors>
    * <https://wiki.archlinux.org/index.php/Lm_sensors>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig
            >
            > Device Drivers  --->
            >     I2C support  --->
            >         -*- I2C support # Symbol: I2C [=y]
            >         ...
            >         <*>   I2C device interface # Symbol: I2C_CHARDEV [=y]
            >
            > Device Drivers  --->
            >     <*> Hardware Monitoring support  ---> # Symbol: HWMON [=y]
            #
            # Select a driver, e.g.:
            #
            >         [*] Intel Core/Core2/Atom temperature sensor (coretemp) # Symbol: SENSORS_CORETEMP [=y]
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge"
        ```console
        # emerge -a lm_sensors
        ```

    === "pacman"
        ```console
        # pacman -S lm_sensors
        ```

    === "apt"
        ```console
        # apt install lm-sensors
        ```

    === "yum"
        ```console
        # yum install lm_sensors
        ```

    === "dnf"
        ```console
        # dnf install lm_sensors
        ```


---
## Config

!!! Note ""

    === "emerge - portage"
        Append `lm_sensors` to the USE variable in your `make.conf`:
        ```console
        # euse -E lm_sensors
        # vi /etc/portage/make.conf # check that the use flag "lm_sensors" has been append:
            > ...
            > USE="... lm_sensors"
            > ...
        ```

        !!! Warning "Warning"
            After modifying the `USE` variable in your `make.conf`, don't forget to update the
            system using the following command so the changes take effect: `# emerge --ask
            --changed-use --deep @world`


---
## Use

* Run a sensors detection:
```console
# sensors-detect # read carfully, you might not want to probe I2C/SMBus : it's more risky
```

* Optionally, add `lm_sensors` to your init system start it:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add lm_sensors default
        # rc-services lm_sensors start
        ```

    === "Runit"
        ```console
        # sv up cronie
        # ln -s /etc/runit/sv/lm_sensors /run/runit/service/
        ```

    === "SysVinit"
        ```console
        # service lm_sensors start
        # chkconfig lm_sensors on
        ```

    === "SystemD"
        ```console
        # systemctl start lm_sensors
        # systemctl enable lm_sensors
        ```

* Finally, you can run it like so:
```console
# sensors
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
