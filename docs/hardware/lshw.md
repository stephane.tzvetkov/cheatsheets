---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# lshw

lshw is a small tool to extract detailed information on the hardware configuration of the machine.
It can report exact memory configuration, firmware version, mainboard configuration, CPU version
and speed, cache configuration, bus speed, etc. on DMI capable x86 or IA-64 systems and on some
PowerPC machines.

???+ Note "Reference(s)"
    * `man lshw`


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add lshw
        ```

    === "apt"
        ```console
        # apt install lshw
        ```

    === "dnf"
        ```console
        # dnf install lshw
        ```

    === "emerge"
        ```console
        # emerge -a sys-apps/lshw
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.lshw
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.lshw
            ```

    === "pacman"
        ```console
        # pacman -S lshw
        ```

    === "yum"
        ```console
        # yum install lshw
        ```

    === "xbps"
        ```console
        # xbps-install -S lshw
        ```

    === "zypper"
        ```console
        # zypper install lshw
        ```


---
## Use

* Print help:
    ```console
    $ lshw --help
    ```

* Output memory-related hardware:
    ```console
    $ lshw -class memory           
    ```

* Output cpu-related hardware:
    ```console
    $ lshw -class cpu 
    ```

* Output disk-related (CD, DVD, ...) hardware:
    ```console
    $ lshw -class disk
    ```

* Output storage-related hardware:
    ```console
    $ lshw -class storage
    ```

* Ouput availables class:
    ```console
    $ lshw -short
    ```

* Output hardware tree as HTML:
    ```console
    $ lshw -html           
    ```

* Output hardware tree as XML:
    ```console
    $ lshw -xml
    ```

* Output hardware tree as JSON:
    ```console
    $ lshw -json
    ```

* Remove sensitive information from output (like serial numbers, etc.):
    ```console
    $ lshw -sanitize
    ```

* Exclude volatile attributes (timestamps) from output
    ```console
    $ lshw -notime
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
