---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Power management

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Power_management/Guide>
    * <https://wiki.gentoo.org/wiki/USB_Power_Saving>
    * <https://01.org/powertop>
    * <https://www.thinkwiki.org/wiki/How_to_reduce_power_consumption>
    * <https://wiki.archlinux.org/index.php/Laptop_Mode_Tools>
    * <https://wiki.archlinux.org/index.php/Power_management>
    * <https://wiki.archlinux.org/index.php/Category:Power_management>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
