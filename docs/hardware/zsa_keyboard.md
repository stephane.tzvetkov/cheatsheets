---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# ZSA keyboard

???+ Note "Reference(s)"
    * <https://ergodox-ez.com/>
    * <https://configure.zsa.io>
    * <https://bepo.fr/wiki/ErgoDox>
    * <https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration>
    * <https://github.com/zsa/wally-cli>
    * <https://github.com/zsa/wally/wiki/Linux-install>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Debug](#debug)

<!-- vim-markdown-toc -->


---
## Install

!!! Note ""

    === "emerge"
        ```console
        $ sudo emerge -a app-misc/wally-cli
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.wally-cli
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.wally-cli
            ```

    === "pacman"
        ```console
        $ sudo pacman -S zsa-wally-cli
        ```

    === "xbps"
        ```console
        # xbps-install -S wally-cli
        ```

    === "other"
        See <https://github.com/zsa/wally/wiki/Linux-install>.


---
## Config

```console
$ sudo vi /etc/udev/rules.d/50-zsa.rules
  + > # Rules for Oryx web flashing and live training
  + > KERNEL=="hidraw*", ATTRS{idVendor}=="16c0", MODE="0664", GROUP="plugdev"
  + > KERNEL=="hidraw*", ATTRS{idVendor}=="3297", MODE="0664", GROUP="plugdev"
  + > 
  + > # Legacy rules for live training over webusb (Not needed for firmware v21+)
  + >   # Rule for all ZSA keyboards
  + >   SUBSYSTEM=="usb", ATTR{idVendor}=="3297", GROUP="plugdev"
  + >   # Rule for the Moonlander
  + >   SUBSYSTEM=="usb", ATTR{idVendor}=="3297", ATTR{idProduct}=="1969", GROUP="plugdev"
  + >   # Rule for the Ergodox EZ
  + >   SUBSYSTEM=="usb", ATTR{idVendor}=="feed", ATTR{idProduct}=="1307", GROUP="plugdev"
  + >   # Rule for the Planck EZ
  + >   SUBSYSTEM=="usb", ATTR{idVendor}=="feed", ATTR{idProduct}=="6060", GROUP="plugdev"
  + > 
  + > # Wally Flashing rules for the Ergodox EZ
  + > ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", ENV{ID_MM_DEVICE_IGNORE}="1"
  + > ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789A]?", ENV{MTP_NO_PROBE}="1"
  + > SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789ABCD]?", MODE:="0666"
  + > KERNEL=="ttyACM*", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", MODE:="0666"
  + > 
  + > # Wally Flashing rules for the Moonlander and Planck EZ
  + > SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", \
  + >     MODE:="0666", \
  + >     SYMLINK+="stm32_dfu"

$ sudo groupadd plugdev
$ sudo usermod -aG plugdev $USER
```

Then log out and log back in.

---
## Use

* Print `wally-cli` version:
    ```console
    $ wally-cli -version
    ```

* Find or create a keyboard layout that you enjoy: <https://configure.zsa.io> (you will retrieve a
  `.hex` firmware file).

* Flash your ergodox keyboard with a firmware file (be prepared to push the reset button):
    ```console
    $ wally-cli /path/to/firmware/file.hex
    ```

* Optionally train on <https://configure.zsa.io/train>.


### Debug

```console
$ setxkbmap -print -verbose 10
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
