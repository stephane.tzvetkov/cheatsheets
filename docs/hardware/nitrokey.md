---
tags:
  - Hardware
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Nitrokey

Nitrokey is an open source/open hardware USB key supporting secure encryption and signing.

???+ Note "Reference(s)"
    * <https://www.nitrokey.com/>
    * <https://www.nitrokey.com/#comparison>
    * <https://github.com/Nitrokey/nitrokey-app>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
