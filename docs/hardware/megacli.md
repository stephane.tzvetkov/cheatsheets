---
tags:
  - Hardware
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# MegaCLI

???+ Note "Reference(s)"
    * <https://web.archive.org/web/20200814101139/https://www.admin-linux.fr/memo-megacli-utilisation-de-controleur-raid-delllsi/>
    * <https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=megacli>
    * <https://raid.wiki.kernel.org/index.php/Hardware_Raid_Setup_using_MegaCli>
    * <https://fedoraproject.org/wiki/EPEL>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install


!!! Note ""

    === "apk"
        TODO

    === "apt"
        TODO: <https://askubuntu.com/questions/488612/how-do-i-install-the-lsi-megacli-in-ubuntu>

    === "dnf (with EPEL)"
        TODO

    === "dnf (by hand)"
        Download the latest version of MegaCLI (`8.07.14` at the time of writing, check it on
        [repology](https://repology.org/project/megacli/versions)):
        ```console
        $ wget https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/8-07-14_MegaCLI.zip
        $ unzip -d megacli 8-07-14_MegaCLI.zip
        $ sudo dnf localinstall ./megacli/Linux/MegaCli-8.07.14-1.noarch.rpm
        ```

    === "emerge"
        ```console
        # emerge -a sys-block/megacli
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.megacli
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.megacli
            ```

    === "~~pacman~~ AUR (by hand)"
        TODO: <https://aur.archlinux.org/packages/megacli/>

    === "yum (with EPEL)"
        TODO

    === "yum (by hand)"
        Download the latest version of MegaCLI (`8.07.14` at the time of writing, check it on
        [repology](https://repology.org/project/megacli/versions)):
        ```console
        $ wget https://docs.broadcom.com/docs-and-downloads/raid-controllers/raid-controllers-common-files/8-07-14_MegaCLI.zip
        $ unzip -d megacli 8-07-14_MegaCLI.zip
        $ sudo yum localinstall ./megacli/Linux/MegaCli-8.07.14-1.noarch.rpm
        ```

    === "xbps"
        TODO

    === "zypper"
        TODO


---
## Use

See <https://web.archive.org/web/20200814101139/https://www.admin-linux.fr/memo-megacli-utilisation-de-controleur-raid-delllsi/>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
