---
tags:
  - Messengers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `signald`

`signald` is a signal daemon that facilitates communication over Signal. It is unofficial,
unapproved, and not nearly as secure as the real Signal clients.

???+ Note "Reference(s)"
    * <https://signald.org/>
    * <https://signald.org/articles/clients/>
    * <https://signald.org/articles/getting-started/>
    * <https://gitlab.com/signald/signald>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
