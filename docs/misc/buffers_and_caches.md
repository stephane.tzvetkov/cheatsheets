---
tags:
  - Standard Unix Programs
  - Memory
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Buffer and caches

???+ Note "Reference(s)"
    * <https://www.linuxatemyram.com/>
    * <https://www.linuxatemyram.com/play.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

```console
$ free -tm
$ man free
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
