---
tags:
  - Markup Converter
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Pandoc

???+ Note "Reference(s)"
    * <https://pandoc.org/MANUAL.html#pandocs-markdown>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

* Convert `.md` file to `.html` (see <https://stackoverflow.com/a/18841>):
```console
$ pandoc -f markdown input_example.md > output_example.html
```

* Convert `.tex` file to `.md` (see <https://tex.stackexchange.com/a/357789>):
```console
$ pandoc -s input_example.tex -o output_example.md
```

* Convert `.html` file to `.md`:
```console
$ pandoc -t html input_example.html -o output_example.pdf
```

* Convert `.docx` file to `.md`:
```console
$ pandoc -s input_example.docx -o output_example.md
```

* Convert `.odt` file to `.md`:
```console
$ pandoc -s input_example.odt -o output_example.md
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
