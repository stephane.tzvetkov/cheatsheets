---
tags:
  - Misc
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Misc

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Gentoo Kernel Asus Laptop Extras](#gentoo-kernel-asus-laptop-extras)
* [Gentoo Kernel `Cool n Quiet` / `PowerNow` / `SpeedStep` features](#gentoo-kernel-cool-n-quiet-powernow-speedstep-features)
* [Driver tip](#driver-tip)
* [Install BEAR on Artix without packaged `grpc`](#install-bear-on-artix-without-packaged-grpc)
* [Server tricks](#server-tricks)
* [Other](#other)

<!-- vim-markdown-toc -->

---
## Gentoo Kernel Asus Laptop Extras

If you are using an Asus laptop, consider activate "Asus Laptop Extras":
<https://www.mjmwired.net/kernel/Documentation/laptops/asus-laptop.txt>

It might enable the following:

* `Fn` key combinations
* Bluetooth enable and disable
* WLAN enable and disable
* GPS enable and disable
* Video output switching
* Ambient Light Sensor on and off
* LED control
* LED Display control
* LCD brightness control
* LCD on and off

Modify your [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) like so:
```console
$ cd /usr/src/linux
# make nconfig # or `# make menuconfig`

    > Device driver --->
    >     [*] X86 Platform Specific Device Drivers---> # Symbol: X86_PLATFORM_DEVICES [=y]
    >         <*> Asus Laptop Extras # Symbol: ASUS_LAPTOP [=n]
```

!!! Warning " "
    After configuring the kernel don't forget to do a [kernel make and
    rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!


---
## Gentoo Kernel `Cool n Quiet` / `PowerNow` / `SpeedStep` features

See
<https://wiki.gentoo.org/wiki/AMD64/FAQ#How_do_I_use_Cool.27n.27Quiet.2FPowerNow.2FSpeedStep_features.3F>


---
## Driver tip

If you are looking for a hardware driver to activate in the kernel but you don't know which driver,
run:
```console
$ hwinfo | less
```
and look for the driver lines...


---
## Install BEAR on Artix without packaged `grpc`

???+ Note "Reference(s)"
    * <https://aur.archlinux.org/packages/bear/>

How to install bear from AUR without the `gprc` dependency already installed (typically because the
Artix `gprc` package is not available)? The following method will allow `gprc` to be cloned and
build locally (this can be long):
```console
$ git clone https://aur.archlinux.org/bear.git
$ cd bear
$ vi PKGBUILD
    > ...
  ~ > #depends=('grpc' 'fmt' 'spdlog')
  + > depends=('fmt' 'spdlog')
    > ...
$ makepkg
$ sudo pacman -U bear-*-x86_64.pkg.tar.zst
```


---
## Server tricks

* Disable graphic card (on my server), but not unpowered:
```console
# echo -n "auto" > /sys/devices/pci0000:00/0000:00:03.1/0000:1d:00.0/power/control
# echo -n "0000:1d:00.0" > /sys/bus/pci/drivers/amdgpu/unbind
```

* Enable graphic card (on my server):
```console
# echo -n "0000:1d:00.0" > /sys/bus/pci/drivers/amdgpu/bind
```


---
## Other

!!! Tip "Tip"
    * [Rosetta](https://www.linuxtricks.fr/wiki/tableau-comparatif-des-outils-urpm-yum-zypper-emerge-et-apt-rosetta)


* Set a shared directory for multiple Linux users (see
  <https://www.tecmint.com/create-a-shared-directory-in-linux/>):
```console
$ sudo groupadd groupname # create a group called "groupname" for the shared folder
$ sudo usermod -a -G groupname user1 # add user1 to the group (he might need to logback in)
$ sudo usermod -a -G groupname user2 # add user2 to the group (he might need to logback in)
$ sudo chgrp -R groupname /path/to/shared/folder/ # apply the group to the shared folder
$ sudo chmod -R 2775 /path/to/shared/folder/ # gives rwx permissions for owner and group, rx for others
```

* Commands to launch from time to time:
```console
$ fstrim -av # see <https://www.linuxtricks.fr/wiki/optimiser-linux-pour-un-pc-portable#paragraph_activer-trim-sur-un-ssd>
```

* Hardware detection (<https://wiki.gentoo.org/wiki/Hardware_detection>):
```console
$ lspci
$ lsusb
```
For example:
```console
$ lspci | grep -i VGA
```

* Play a video in ASCII art mode:
```console
$ CACA_DRIVER=ncurses mpv videotest.mkv --vo=caca
```

* Print big lolcat style:
```console
$ man man | figlet | lolcat
```

* Print weather:
```console
$ curl wttr.in/Paris
```

* Hide files into images: <https://www.ostechnix.com/hide-files-inside-images-linux/>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
