---
tags:
  - Documentation
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Read the Docs and MkDocs

Read the Docs simplifies software documentation by automating building, versioning, and hosting of
your docs for you.

MkDocs is a simple static site generator that's geared towards building project documentation.
Documentation source files are written in Markdown, and configured with a single YAML configuration
file.

???+ Note "Reference(s)"
    * <https://squidfunk.github.io/mkdocs-material/reference/abbreviations/#usage>
    * <https://readthedocs.org/>
    * <https://docs.readthedocs.io/en/latest/intro/getting-started-with-mkdocs.html>
    * <https://www.mkdocs.org/>
    * <https://www.mkdocs.org/user-guide/writing-your-docs/>
    * <https://daringfireball.net/projects/markdown/syntax>
    * <https://squidfunk.github.io/mkdocs-material/extensions/admonition/>
    * <https://squidfunk.github.io/mkdocs-material/extensions/pymdown/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Read the Docs](#read-the-docs)
* [MkDocs](#mkdocs)
    * [Install](#install)
    * [Config](#config)
    * [Use](#use)
    * [Examples](#examples)

<!-- vim-markdown-toc -->

---
## Read the Docs

* create a profile on readthedocs : <https://readthedocs.org/accounts/signup/>

* connect your GitLab (or GitHub or whatever is supported) account: "settings" -> "connected
  services"

* import your project: "my projects" -> "import a project"

* build it

---
## MkDocs

This section assumes your project has a `docs` folder in which the documentation you want to build
is located:
```console hl_lines="3"
$ cd /path/to/your/project
$ ls
    > docs # ⚠️
    > src
    > tests
    > LICENSE.md
    > README.md
    > ...
```

### Install

Create a Python [virtual
environment](../dev/python/python.md#python-virtual-environment-venv-good-practices) in the git
project you want to document and install `mkdocs`:
```console
$ python -m venv .venv
$ source .venv/bin/activate
(.venv) $ which pip && which python # make sure those binaries are in your virtual environment
(.venv) $ pip install --upgrade pip
(.venv) $ pip install mkdocs
```

!!! Note "Note"
    Don't forget to create a relevant `.gitignore` to avoid committing the `.venv`, e.g.:
    <https://www.toptal.com/developers/gitignore/api/linux,macos,windows,python,vim,emacs>

### Config

Setup your `mkdocs` project:
```console
(.venv) $ mkdocs new . # creates mkdocs.yml (holds MkDocs config), and docs/index.md (doc entry point)
(.venv) $ mv mkdocs.yml .mkdocs.yml # optional: change mkdocs config file name
```

Redirect your `docs/index.md` to your `README.md` (optional):
```console
(.venv) $ cd docs
(.venv) $ ln -s ../README.md ./index.md
(.venv) $ cd ..
```

Create a `readthedocs-requirements.txt` file, for the nice plugins to come:
```console
(.venv) $ vi docs/readthedocs-requirements.txt
    > mkdocs-material
    > mkdocs-material-extensions
    > pymdown-extensions
    > mkdocs-awesome-pages-plugin
```

Install the plugins requirements:
```console
(.venv) $ pip install -r docs/readthedocs-requirements.txt
```

Create a readthedocs config file:
```console
(.venv) $ vi .readthedocs.yml
    > # .readthedocs.yml
    > # Read the Docs configuration file
    > # See <https://docs.readthedocs.io/en/stable/config-file/v2.html> for details
    >
    > # Required
    > version: 2
    >
    > # Build the documentation with mkdocs
    > mkdocs:
    >   configuration: .mkdocs.yml
    >
    > # Build docs in additional formats such as PDF and ePub
    > formats: all
    >
    > # Set requirements required to build the documentation
    > python:
    >     install:
    >         # <https://github.com/Python-Markdown/markdown/wiki/Third-Party-Extensions>
    >         # <https://facelessuser.github.io/pymdown-extensions/installation/>
    >         - requirements: docs/readthedocs-requirements.txt
```

Create some dummy file (for the example) and modify the `.mkdocs.yml` config file:
```console
(.venv) $ mkdir -p docs/Test1
(.venv) $ mkdir -p docs/Test2
(.venv) $ touch docs/Test1/test1.0.md
(.venv) $ touch docs/Test1/test1.1.md
(.venv) $ touch docs/Test2/test2.md
(.venv) $ vi .mkdocs.yml
    >
    > # Project information
    > site_name: Your site name
    > site_description: A short description of your site.
    > site_author: Your name
    > site_url: Your site url (e.g. the url of your project repo)
    >
    > # Repository
    > repo_name: the name of your git repo
    > repo_url: https://your-repo-url
    >
    > # [OPTIONAL with awesome-pages plugin] Navigation
    > nav:
    >     - Home: index.md
    >     - 'Test1':
    >         - Test1.0: Test1/test1.0.md
    >         - Test1.1: Test1/test1.1.md
    >     - 'Test2':
    >         - Test2: Test2/test2.md
    >
    > # Configuration
    > theme:
    >     # <https://github.com/squidfunk/mkdocs-material>
    >     name: material
    >     language: en
    >     palette:
    >         scheme: slate
    >         primary: indigo
    >         accent: indigo
    >     font:
    >         #text: Ubuntu
    >         text: Hack
    >         #code: Ubuntu Mono
    >         code: Hack Mono
    >
    > # Plugins
    > plugins:
    >     - search
    >     - awesome-pages # <https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin>
    >
    > # Markdown extensions
    > markdown_extensions:
    >     - toc:
    >         permalink: true
    >     - abbr
    >     - admonition
    >     - codehilite
    >     - pymdownx.arithmatex
    >     - pymdownx.betterem:
    >         smart_enable: all
    >     - pymdownx.caret
    >     - pymdownx.critic
    >     - pymdownx.details
    >     - pymdownx.inlinehilite
    >     - pymdownx.keys
    >     - pymdownx.magiclink
    >     - pymdownx.mark
    >     - pymdownx.smartsymbols
    >     - pymdownx.superfences
    >     - pymdownx.snippets
    >     - pymdownx.tasklist:
    >         custom_checkbox: false
    >     - pymdownx.tabbed
    >     - pymdownx.tilde
    >     #- pymdownx.emoji:
    >     #   emoji_index: !!python/name:materialx.emoji.twemoji
    >     #   emoji_generator: !!python/name:materialx.emoji.to_svg
    >     # <https://facelessuser.github.io/pymdown-extensions/extensions/arithmatex/>
```

### Use

* See `mkdocs` help:
```console
(.venv) $ mkdocs -h
```

* Build:
```console
(.venv) $ mkdocs build -f .mkdocs.yml
```

* Build and Serve:
```console
(.venv) $ mkdocs serve -f .mkdocs.yml
            > ...
            > INFO    -  Serving on http://127.0.0.1:8000
            > ...
```

!!! Tip "Tip"
    You can use [`linkchecker`](https://linkchecker.github.io/linkchecker/) in order to make sure
    there is no broken links on the generated and running website:
    `$ linkchecker http://127.0.0.1:8000`

### Examples

* Abbreviations, e.g. HTML
    * see <https://squidfunk.github.io/mkdocs-material/reference/abbreviations>

* Admonitions
    * see <https://squidfunk.github.io/mkdocs-material/reference/admonitions/>

    !!! Note
        Example note.

    ???+ Note "collapsible note"
        Example note.

    ??? Note "already collapsed note"
        Example note.

    !!! Note ""
        Example note.

    !!! Note " "
        Example note.

    !!! Note
        Example note.

        ``` python
        def bubble_sort(items):
            for i in range(len(items)):
                for j in range(len(items) - 1 - i):
                    if items[j] > items[j + 1]:
                        items[j], items[j + 1] = items[j + 1], items[j]
        ```

        With code block.

    ??? Abstract
        Example abstract.

    ??? Summary
        Example summary.

    ??? Tldr
        Example TLDR.

    ??? Info
        Example info.

    ??? Todo
        Example TODO.

    ??? Tip
        Example tip.

    ??? Hint
        Example hint.

    ??? Important
        Example important.

    ??? Success
        Example success.

    ??? Check
        Example check.

    ??? Done
        Example done.

    ??? Question
        Example question.

    ??? Help
        Example help.

    ??? Faq
        Example FAQ.

    ??? Warning
        Example Warning.

    ??? Caution
        Example caution.

    ??? Attention
        Example attention.

    ??? Failure
        Example failure.

    ??? Fail
        Example fail.

    ??? Missing
        Example missing.

    ??? Danger
        Example danger.

    ??? Error
        Example error.

    ??? Bug
        Example bug.

    ??? Example
        Example example.

    ??? Quote
        Example quote.

    ??? Cite
        Example cite.

* Code blocks
    * see <https://squidfunk.github.io/mkdocs-material/reference/code-blocks>

    ``` python
    import tensorflow as tf
    ```

    ``` python linenums="1"
    def bubble_sort(items):
        for i in range(len(items)):
            for j in range(len(items) - 1 - i):
                if items[j] > items[j + 1]:
                    items[j], items[j + 1] = items[j + 1], items[j]
    ```

    ``` python hl_lines="2 4"
    def bubble_sort(items):
        for i in range(len(items)):
            for j in range(len(items) - 1 - i):
                if items[j] > items[j + 1]:
                    items[j], items[j + 1] = items[j + 1], items[j]
    ```

    ``` python hl_lines="2-5"
    def bubble_sort(items):
        for i in range(len(items)):
            for j in range(len(items) - 1 - i):
                if items[j] > items[j + 1]:
                    items[j], items[j + 1] = items[j + 1], items[j]
    ```

    The `#!python range()` function is used to generate a sequence of numbers.

    ++ctrl+alt+del++

<br/>

* Annotations in code blocks:

    * Annotation with and without comment:

    ``` bash
    #!/bin/bash

    ################################################################################
    # Safe shell settings:
    ######################
    # see https://sipb.mit.edu/doc/safe-shell/
    set -euf -o pipefail

    # Put the rest of your script here
    # ...
    # (1)

    ```

    1.   I'm a code annotation! I can contain `code`, __formatted text__, images, ... Basically
         anything that can be written in Markdown.

    ``` bash
    #!/bin/bash

    ################################################################################
    # Safe shell settings:
    ######################
    # see https://sipb.mit.edu/doc/safe-shell/
    set -euf -o pipefail

    # Put the rest of your script here
    # ...
    # (1)!

    ```

    1.   I'm a code annotation! I can contain `code`, __formatted text__, images, ... Basically
         anything that can be written in Markdown.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
