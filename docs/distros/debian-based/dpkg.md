---
tags:
  - Linux Distributions
  - Distros
  - Debian Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `dpkg`

`dpkg` (Debian package manager), is a tool to install, build, remove and manage Debian packages.
The primary and more user-friendly front end for `dpkg` is `apt`.


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

---
## Use

* Print dependencies of a `.deb` package:

    ```console
    $ dpkg -I /path/to/your/packagename.deb | grep -E "Depends|Recommends|Suggests|Pre\-Depends" | tr -d "|," | sed "s/([^)]*)/()/g" | tr -d "()" | tr " " "\n" | grep -Ev "Depends|Recommends|Suggests|Pre\-Depends" | xargs
    ```

* Check if some packages are installed (e.g. the list of dependencies printed by the previous
  command):

    ```console
    $ dpkg-query -l packagename1 packagename2 packagename3
    ```
    or
    ```console
    $ dpkg -s packagename1 packagename2 packagename3
    ```

* Install a `.deb` package:

    ```console
    $ dpkg -i packagename
    ```

* Find what package owns a command:

    ```console
    $ dpkg -S `which <command>`
    ```



---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
