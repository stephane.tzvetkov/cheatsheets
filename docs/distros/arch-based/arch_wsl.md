TODO

---
See <https://github.com/yuk7/ArchWSL>
See <https://learn.microsoft.com/en-us/windows/wsl/>

---
## Prerequisites:

* Windows 10 1709 FCU x64 or later / Windows 11 x64
* [Enable virtualization support on your machine](https://web.archive.org/web/20231103172057/https://support.salad.com/article/270-how-to-enable-virtualization-support-on-your-machine)


---
## Install

* Run `Command Prompt` (hit <Win+P> then enter "cmd") as an administrator, then execute the following
  commands:

    ```console
    # dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
    # dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
    ```

* [Download the latest Linux kernel update package and install it](https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)

* Now restart Windows.

* Set WSL 2 as the default WSL version (after running `Command Prompt` as an administrator):

    ```console
    # wsl --set-default-version 2
    ```

* [Then follow the ArchWSL documentation](https://wsldl-pg.github.io/ArchW-docs/How-to-Setup/#how-to-set-up-archwsl), i.e.:

    * [Download the `Arch.zip` file in the assests of the lastest release](https://github.com/yuk7/ArchWSL/releases/)
    * Extract the `Arch.zip` into a "Arch" directory: that directory must be located somewhere you
      have write permissions (whitout being asked for administrator privileges), e.g. not `C:\Program Files\Arch` but rather `C:\Users\YourUserName\Arch`.
    * Inside `C:\Users\YourUserName\Arch`, run `Arch.exe` .


---
## Config

* After running `Arch.exe` for the first time, run the following commands:

    ```console
    # passwd # set a root password
    # echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/wheel # allow all Arch users to run commands as root
    # useradd -m -G wheel -s /bin/bash your-user-name # create your user
    # passwd your-user-name # set your user password
    # exit
    ```

* Run ArchWSL with your newly created user by default (after running `Command Prompt` as an administrator):

    ```console
    # Arch.exe config --default-user your-user-name
    ```


---
## Use

* Just run `Arch.exe` (you can pin it to your task bar, or to start, or wherever).
* See <https://wsldl-pg.github.io/ArchW-docs/How-to-Use/>.

