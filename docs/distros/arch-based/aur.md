---
tags:
  - Linux Distributions
  - Distros
  - Arch Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Arch User Repository

The AUR, is a major part of the Arch Linux ecosystem. It’s a community driven repository for the
Arch Linux system that hosts a number of packages outside the official Arch Linux package database.

???+ Note "Reference(s)"
    * <https://aur.archlinux.org/>
    * <https://aur.archlinux.org/packages/>
    * <https://linuxhint.com/aur_arch_linux/>
    * <https://wiki.archlinux.org/index.php/Arch_User_Repository>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Config](#config)
* [Use](#use)
    * [Manually](#manually)
    * [Yay](#yay)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Config

Update your system and install the needed packages:
```console
# pacman -Syu
# pacman -S git base-devel
```

Create a dedicated AUR directory:
```console
$ mkdir -p ~/apps/aur-apps
```


---
## Use

* Search available packages here: <https://aur.archlinux.org/packages/>

### Manually

* Install an AUR package:
```console
$ cd ~/apps/aur-apps
$ git clone https://aur.archlinux.org/<package-name>.git
$ cd <package-name>
$ makepkg -si # --syncdeps to auto-install deps, --install to install after building
```
<!-- `# pacman -U <package-name>.tar.xz` -->

* Uninstall an AUR package and it's dependencies (which are not required by any other installed
  package):
```console
# pacman -Rs <package-name>
```

* Update an AUR package:
```console
$ cd ~/apps/aur-apps/<package-name>
$ git pull
$ makepkg -si # --syncdeps to auto-install deps, --install to install after building
```

### With yay

Yay is an AUR helper (written in Go): see [yay.md](./yay.md).

### Troubleshooting

* when using `$ makepkg`, if the following error appears: `==> ERROR: One or more PGP signatures
  could not be verified!`, then you can run it again like this: `$ makepkg --skippgpcheck`


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
