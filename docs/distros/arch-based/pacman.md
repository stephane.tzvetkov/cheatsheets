---
tags:
  - Linux Distributions
  - Distros
  - Arch Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `pacman`

`pacman` is the Arch Linux package manager. It combines a simple binary package format with an
easy to use build system. The goal of `pacman` is to make it possible to easily manage packages,
whether they are from the official repositories or the user's own builds.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Pacman#Removing_packages>
    * <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks> **TODO**
    * <https://wiki.archlinux.org/index.php/Pacman/Rosetta>
    * <https://wiki.archlinux.org/index.php/Meta_package_and_package_group>
    * <https://www.archlinux.org/groups/>
    * <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.8>
    * <https://gitea.artixlinux.org/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [`pacman-contrib`](#pacman-contrib)
    * [informant](#informant)
    * [`ancient-packages`](#ancient-packages)
* [Config](#config)
    * [Hooks](#hooks)
* [Use](#use)
    * [Installing packages](#installing-packages)
    * [Maintain a list of installed packages](#maintain-a-list-of-installed-packages)
    * [Updating packages](#updating-packages)
    * [Updating system](#updating-system)
    * [Removing packages](#removing-packages)
    * [Searching packages](#searching-packages)
    * [Removing packages cache](#removing-packages-cache)
    * [`paccache`](#paccache)
    * [`makepkg`?](#makepkg)
    * [`pactree`](#pactree)
    * [`checkupdates`](#checkupdates)
    * [Informant](#informant-1)
    * [`ancient-packages`](#ancient-packages-1)
    * [Hooks](#hooks-1)
    * [Mirrors](#mirrors)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

### `pacman-contrib`

[OPTIONAL]

This package contains contributed scripts and tools for `pacman` systems (e.g. `pactree` and
`checkupdates`).

You can optionally install it to use those tools:
    ```console
    # pacman -S pacman-contrib
    ```

**TODO**: run `$ pacman -Ql pacman pacman-contrib | grep -E 'bin/.+'` to see the full list of
`pacman-contrib` tools

### informant

[OPTIONAL]

An Arch Linux news reader designed to also be used as a `pacman` hook, which prevents you from
updating if there is fresh Arch news that you have not read since the last update ran.

* <https://github.com/bradford-smith94/informant>
* <https://aur.archlinux.org/packages/informant/>

### `ancient-packages`

[OPTIONAL]

* <https://aur.archlinux.org/packages/ancient-packages/>


---
## Config

See <https://wiki.archlinux.org/index.php/Pacman#Configuration>
<br/>
See <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.conf.5>

* Edit the `pacman` config file as you wish:
    ```console
    # vi /etc/pacman.conf
        > ...
    ```

* Skip a specific package from being upgrading during a system update:
    ```console
    # vi /etc/pacman.conf
        > ...
     +  > IgnorePkg=package_name
        > ...
    ```

* Skip a package group from being upgrading during a system update:
    ```console
    # vi /etc/pacman.conf
        > ...
      + > IgnoreGroup=group_name
        > ...
    ```

### Hooks

**WIP**

See <https://wiki.archlinux.org/index.php/Pacman#Hooks>
<br/>
See <https://www.archlinux.org/pacman/alpm-hooks.5.html>

`pacman` can run pre and post transaction hooks from the `/usr/share/libalpm/hooks/` directory.
More directories can be specified with the `HookDir` option in `pacman.conf`, which defaults to
`/etc/pacman.d/hooks`. Hook file names must be suffixed with `.hook`.

E.g. `pacman` post transaction hook to be notified if a transaction orphaned a package. This can be
useful for being notified when a package has been dropped from a repository, since any dropped
package will also be orphaned on a local installation, unless it was explicitly installed (see
<https://wiki.archlinux.org/index.php/Pacman#Querying_package_databases>):
    ```console
    # vi /etc/pacman.d/hooks/orphans.hook
        > [Trigger]
        > Operation = Remove
        > Operation = Install
        > Operation = Upgrade
        > Type = Package
        > Target = *
        >
        > [Action]
        > Description = Be notified if a transaction orphaned a package.
        > When = PostTransaction
        > Exec = /usr/bin/bash -c "/usr/bin/pacman -Qtd || /usr/bin/echo '=> None found.'"
    ```

Other example:
<https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages>


---
## Use

### Installing packages

* Install one or multiple specific packages:
    ```console
    # pacman -S package_name1 package_name2
    ```

* Install a package as a dependency (in opposition to explicitly installed):
    ```console
    # pacman -S --asdeps package_name
    ```

* Check the "reason" of an installed package:
    ```console
    $ pacman -Qi package_name | grep "Install Reason"
    ```

* Change the "reason" of an installed package, from explicitly to dependency:
    ```console
    # pacman -D --asdeps package_name
    ```

* Change the "reason" of an installed package, from dependency to explicitly:
    ```console
    # pacman -D --asexplicit package_name
    ```

* Install a package group:
    ```console
    # pacman -S package_group # e.g. "gnome"
    ```

* Print what packages belong to a package group:
    ```console
    # pacman -Sg package_group # e.g. "gnome"
    ```

### Maintain a list of installed packages

* <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages>
* <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Install_packages_from_a_list>

### Updating packages

* Update a package:
    ```console
    # pacman -Syu package_name
    ```
(NOTE: in practice, do not run `# pacman -Sy package_name` but `# pacman -Syu package_name` in
order to avoid dependency issues)

### Updating system

* Print for how long the system hasn't been updated:
    ```console
    $ sync_date=$(egrep 'pacman -Syu' /var/log/pacman.log | tail -1 | grep -Eo '[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}T[[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}') # https://stackoverflow.com/questions/26881104/extract-date-from-a-file-name-in-unix-using-shell-scripting
    $ secs=$(( $(date -d now +%s) - $(date -d $sync_date +%s) ))
    $ days=$(echo "$secs / (60 * 60 *24)" | bc)
    $ echo $days
    ```

* Read the news:

    !!! Note ""

        === "Arch Linux"
            * Check the latest news [here](https://www.archlinux.org/news/).
              <br/>
              **OR**
              <br/>
            * [OPTIONAL] Check the latest news with informant (see above [informant installation
              section](#informant) and below [informant usage section](#informant_1)).

        === "Artix Linux"
            * Check the latest Arch Linux news [here](https://www.archlinux.org/news/).
            * Check the latest artix linux news [here](https://artixlinux.org/news.php).
            * Check the lastest artic linux announcements
              [here](https://forum.artixlinux.org/index.php/board,4.0.html)
              <br/>
              **OR**
              <br/>
            * [OPTIONAL] Check the latest news with informant (see above [informant installation
              section](#informant) and below [informant usage section](#informant_1)).
            * Check the latest artix linux news [here](https://artixlinux.org/news.php).
            * Check the lastest artic linux announcements
              [here](https://forum.artixlinux.org/index.php/board,4.0.html)

* [OPTIONAL] Check updates (see above [`pacman-contrib` installation section](#pacman-contrib) and
  below [`checkupdates` usage section](#checkupdates)).

* Update the system:
    ```console
    # pacman -Syu
    ```

* See the bellow [troubleshooting section](#troubleshooting) if any problem is encountered.

* [Immediately act on alerts if
  any.](https://wiki.archlinux.org/index.php/System_maintenance#Act_on_alerts_during_an_upgrade)

* [Promptly deal with new configuration files if
  any.](https://wiki.archlinux.org/index.php/System_maintenance#Deal_promptly_with_new_configuration_files)

* `# reboot`

* Remove orphans (packages that were installed as a dependency, but no other packages depend on
  them after the system update):
    ```console
    # pacman -Qtd # this command will print orphans
    # pacman -Rns $(pacman -Qtdq) # this command will remove orphans (see <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Removing_unused_packages_(orphans)>)
    ```

* Remove dropped packages (packages that no longer are in the remote repositories, but still are on
  your local system):
    ```console
    # pacman -Qm # this command will print dropped packages (then remove them manually if any)
    ```

    !!! Warning "Warning"
        `# pacman -Qm` will also print manually installed packages (e.g. with [AUR](./aur.md)). To
        exclude packages that are (still) available on the AUR, use the ancient-packages tool (see
        above [ancient-packages installation section](#ancient-packages) and below
        [#ancient-packages usage section](#ancient-packages_1))..

* [OPTIONAL] Clean package cache (see above [`pacman-contrib` installation
  section](#pacman-contrib) and below [`paccache` usage section](#paccache)).

### Removing packages

* Remove a package and its dependencies which are not required by any other installed package:
    ```console
    # pacman -Rs package_name
    ```

* Remove a single package, leaving all of its dependencies installed:
    ```console
    # pacman -R package_name
    ```

* Remove a package and its dependencies (which are not required by any other installed package)
  **and** prevent the creation of it's configuration files backup (`*.pacsave` files):
    ```console
    # pacman -Rsn package_name
    ```

    !!! Info "Info"
        `pacman` default behavior is to backup configuration files instead of deleting them. But
        even when told to delete them (preventing the creation of backups) `pacman` can't remove
        configuration files that the application created itself (e.g. "dotfiles" in the home
        folder).

### Searching packages

* Search for packages in the database, for both packages' names and descriptions containing
  `string1` and `string2`:
    ```console
    $ pacman -Ss string1 string2 ...
    ```

* Search for already installed packages' names and descriptions containing `string1` and `string2`:
    ```console
    $ pacman -Qs string1 string2 ...
    ```

* Search for package file names in "remote packages" (packages not from a repository stated in
  `pacman`'s configuration files):
    ```console
    # pacman -Fy # [OPTIONAL] sync the files database
    $ pacman -F string1 string2 ...
    ```

* Print extensive information about a given package:
    ```console
    $ pacman -Si package_name
    ```

* Print extensive information about a locally installed packages:
    ```console
    $ pacman -Qi package_name
    ```

    !!! Tip "Tip"
        Passing two `-i` flags will also display the list of backup files and their modification
        states:
        ```console
        $ pacman -Qii package_name
        ```

* Print locally installed packages containing matching a string:
    ```console
    $ pacman -Q | grep string-to-match
    ```

* Print the list of files installed by a package:
    ```console
    $ pacman -Ql package_name
    ```
    
* Print binary (or binaries) associated to a package,
    in order to find out what command(s) to use with a package:
    ```console
    $ pacman -Ql package_name | grep /bin/
    ```

* Print the list of files installed by a "remote package" (package not from a repository stated in
  `pacman`'s configuration files):
    ```console
    $ pacman -Fl package_name
    ```

* Check the presence of the files installed by a package:
    ```console
    $ pacman -Qk package_name
    ```

    !!! Tip "Tip"
        Passing two `-k` flag will perform a more thorough check:
        ```console
        $ pacman -Qkk package_name
        ```

* Search which package a file in the file system belongs to:
    ```console
    $ pacman -Qo /path/to/file_name
    ```

* Search which "remote package" (package not from a repository stated in `pacman`'s configuration
  files) a file belongs to:
    ```console
    $ pacman -F /path/to/file_name
    ```

* List all packages no longer required as dependencies (orphans):
    ```console
    $ pacman -Qdt
    ```

* List all packages explicitly installed:
    ```console
    $ pacman -Q
    ```

* List all packages explicitly installed:
    ```console
    $ pacman -Qe
    ```

* List all packages explicitly installed and not required as dependencies:
    ```console
    $ pacman -Qet
    ```

### Removing packages cache

* Remove all the cached packages that are not currently installed and the unused sync database:
    ```console
    # pacman -Sc
    ```

    !!! Tip "Tip"
        Passing two `-c` flag will remove all files from the cache, this is the most aggressive
        approach and will leave nothing in the cache folder:
        ```console
        # pacman -Scc
        ```

### `paccache`

From the `pacman-contrib` package (see
<https://wiki.archlinux.org/index.php/Pacman#Cleaning_the_package_cache> and
<https://jlk.fjfi.cvut.cz/arch/manpages/man/paccache.8>)

* Delete all cached versions of installed and uninstalled packages, except for the most recent 3
  (by default):
    ```console
    # paccache -r
    ```

* Define how many recent versions you want to keep when deleting cached versions. E.g. to retain
  only one past version use:
    ```console
    # paccache -rk1
    ```

* Remove all cached versions of uninstalled (`-u`) packages:
    ```console
    # paccache -ruk0
    # paccache -r
    ```

### `makepkg`?

* <https://wiki.archlinux.org/index.php/Makepkg>

### `pactree`

From the `pacman-contrib` package (see <https://wiki.archlinux.org/index.php/Pacman#Pactree>)

* Print the dependency tree of a package:
    ```console
    $ pactree package_name
    ```

* Print the dependent tree of a package:
    ```console
    $ pactree -r package_name
    ```

### `checkupdates`

From the `pacman-contrib` package:

* <https://wiki.archlinux.org/index.php?title=Checkupdates&redirect=no>

### Informant

* <https://github.com/bradford-smith94/informant>
* <https://aur.archlinux.org/packages/informant/>

### `ancient-packages`

* <https://aur.archlinux.org/packages/ancient-packages/>

### Hooks

* TODO: <https://www.reddit.com/r/archlinux/comments/dsnu81/hear_ye_archers_share_your_pacman_hooks/>

!!! Tip "Tip"
    `/usr/share/libalpm/hooks` is for system managed hooks, and `/etc/pacman.d/hooks` for user
    overrides.

### Mirrors

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Mirrors>
    * <https://www.archlinux.org/mirrors/>
    * <https://www.archlinux.org/mirrors/status/>

### Troubleshooting

* <https://wiki.archlinux.org/index.php/Pacman#Troubleshooting>

* If getting the following error when installing a package (see
  <https://forum.artixlinux.org/index.php/topic,952.0.html>):

    ```console
    ...
    ... requested URL returned error: 404
    ... requested URL returned error: 404
    ... requested URL returned error: 404
    warning: failed to retrieve some files
    error: failed to commit transaction (failed to retrieve some files)
    Errors occurred, no packages were upgraded.
    ```

    Then you might have to update your mirrors located in `/etc/pacman.d/mirrorlist` (e.g. for
    Artix see
    <https://gitea.artixlinux.org/packages/artix-mirrorlist/src/branch/master/mirrorlist>).

    Then [update the system](#updating-system) with `# pacman -Syyu` (passing two `-y`, or
    `--refresh`, flags will force a refresh of all package lists even if they appear to be up to
    date).

* If you get this kind of error (see <https://bbs.archlinux.org/viewtopic.php?id=272835>):
    ```console
    error: package-name: signature from "..." is unknown trust
    ```
    Then this probably means your system hasn't been updated for a little while, in this case you
    might just have to update `archlinux-keyring`:
    ```console
    $ sudo pacman -S archlinux-keyring
    ```
    After that, you can try to `sudo pacman -Syu` again.

    !!! Note ""
        Note for Artix users, you might have to update `artix-keyring`:
        ```console
        $ sudo pacman -S artix-keyring
        ```

* If you get this kind of error:
    ```console
    error: package-name: signature from "..." is marginal trust
    :: File /path/to/package-name.pkg.tar.zst is corrupted (invalid or corrupted package (PGP signature)).
    ```
    Then this probably means your system hasn't been updated for a little while, in this case you
    might just have to update `archlinux-keyring`:
    ```console
    $ sudo pacman -S archlinux-keyring
    ```
    After that, you can try to `sudo pacman -Syu` again.

    !!! Note ""
        Note for Artix users, you might have to update `artix-keyring`:
        ```console
        $ sudo pacman -S artix-keyring
        ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
