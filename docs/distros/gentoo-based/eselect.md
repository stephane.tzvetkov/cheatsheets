---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `eselect`

`eselect` is a tool for administration and configuration on Gentoo systems. It will modify the
system’s behavior and should be used with care by the system administrator. `eselect` is a modular
framework for writing configuration utilities. It consists of:

* A main program called `eselect`.
* Various modules (`*.eselect` files) which carry out different tasks.
* Several libraries which help ensure consistent behavior and simplify the creation of new modules.

A module provides several actions. Actions typically either display some information ("list" and
"show" actions are common) or update the system somehow (for example, "set" and "update"). Each
module also provides "help" and "usage" actions which explain how to use the module.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Eselect>
    * <https://wiki.gentoo.org/wiki/Project:Eselect/User_guide>
    * <https://wiki.gentoo.org/wiki/Eselect/Repository>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Base#Choosing_the_right_profile>
    * <https://wiki.gentoo.org/wiki/Project:Python/python-exec#eselect-python>
    * <https://wiki.gentoo.org/wiki/Fontconfig#Gentoo_specific>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)
    * [modules](#modules)
    * [repositories](#repositories)
    * [kernel](#kernel)
    * [news](#news)
    * [sh](#sh)
    * [python](#python)
    * [pip](#pip)

<!-- vim-markdown-toc -->

---
## Install

`eselect` should be already installed by default on Gentoo:
```console
# emerge -na app-admin/eselect
```

---
## Use

See how to use `eslect`:
```console
$ eselect help
```

### modules

List all the available `eselect` modules already installed on your system:
```console
$ eselect modules list
```

!!! Tip
    `$ emerge -s eselect` to see more available `eselect` modules.

### repositories

See [overlays](./overlays.md#eselect-repositories)

### kernel

```console
$ eselect kernel list
```

```console
$ eselect kernel set 42
```

### news

```console
$ eselect news list
```

```console
$ eselect news read
```

### sh

```console
$ eselect sh list
```

### python

```console
$ eselect python list
```

### pip

```console
$ eselect pip list
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
