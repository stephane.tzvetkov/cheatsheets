---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `elogv`

`elogv` is a curses based tool that parses the contents of `elogs` created by Portage.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Elogv>
    * [Let's Learn About `elogs` and `elogv`](https://www.youtube.com/watch?v=_rooRjtCY9k)


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

```console
# emerge -a app-portage/elogv
```


---
## Config

To use `elogv`, the Portage `elog` system needs to be configured in `/etc/portage/make.conf` with
the following addition:
```console
# vi /etc/portage/make.conf
    > ...
    > # Logging
    > PORTAGE_ELOG_CLASSES="warn error log"
    > PORTAGE_ELOG_SYSTEM="save"
    > ...
```

For additional variable definitions see the `/usr/share/portage/config/make.conf.example` file.


---
## Use

Run `elogv`:
```console
$ elogv --help
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
