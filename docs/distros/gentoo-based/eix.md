---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `eix`

`eix` is a set of utilities for searching and diffing local `ebuild` repositories using a binary
cache. It is made to be more efficient and flexible than the `$ emerge --search` command.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Eix>
    * <https://www.youtube.com/watch?vNlDQornnKE>
    * <https://www.youtube.com/watch?v=rMQ5NBR64aI>
    * `$ man eix`


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Auto-update `eix` cache](#auto-update-eix-cache)
    * [Allow search all overlays (not only installed ones)](#allow-search-all-overlays-not-only-installed-ones)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

```console
# emerge -a app-portage/eix
```

After the installation has finished, it is important to update the cache to index all packages on
the system. Running following command will update the local `eix` cache:
```console
# eix-update
```


---
## Config

* Available configuration files:
    * `/etc/eix-sync.conf`: stores commands and configurations for `eix-sync`.
    * `/etc/eixrc`: `eix`'s global configuration file.
    * `~/.eixrc`: `eix`'s per-user configuration file.

* Available environment variables:
    * `EIXRC`: If this variable can set a new location for `eix`'s global configuration file (see
      above).
    * `EIX_SYNC_OPTS`, `EIX_SYNC_CONF`, `EIX_REMOTE_OPTS`, `EIX_LAYMAN_OPTS`,
      `EIX_TEST_OBSOLETE_OPTS`: Security relevant variables (see the man page for more details).

### Auto-update `eix` cache

After each update of the Portage tree the `eix` cache needs to be updated. Here is how to update it
automatically:

Create the following script, allowing to run all executable scripts in the
`/etc/portage/repo.postsync.d/` directory, after each sync:
```console
# vi /etc/portage/repo.postsync.d/eix
    > #!/usr/bin/env bash
    > if [[ -e /var/cache/eix/portage.eix ]]; then
    >     cp -a /var/cache/eix/portage.eix /var/cache/eix/previous.eix;
    > fi
    > eix-update
    > if [[ -e /var/cache/eix/previous.eix ]]; then
    >     eix-diff;
    > fi

# chmod +x /etc/portage/repo.postsync.d/eix
```

If additional ebuild repositories are used, `eix-update` can become quite slow, as many of them do
not provide pre-generated caches. To speed up the process of `eix-update`, one can hook portage to
regenerate the cache for each repository after each sync. An example for this is provided in the
`/etc/portage/repo.postsync.d/example` file. This example can be copied to
`/etc/portage/repo.postsync.d/egencache` and made executable. Note the `--jobs` argument to
`egencache`: setting this to the output of `nproc` (as shown below) causes it to use all available
system cores. If less impact is desired, of course this can be reduced to a smaller number.
```console
# vi /etc/portage/repo.postsync.d/egencache
    > #!/bin/sh
    >
    > # The repository name.
    > repository_name=${1}
    > # The URI to which the repository was synced.
    > sync_uri=${2}
    > # The path to the repository.
    > repository_path=${3}
    >
    > # Portage assumes that a hook succeeded if it exits with 0 code. If no
    > # explicit exit is done, the exit code is the exit code of last spawned
    > # command. Since our script is a bit more complex, we want to control
    > # the exit code explicitly.
    > ret=0
    >
    > if [ -n "${repository_name}" ]; then
    >         # Repository name was provided, so we're in a post-repository hook.
    >         echo "* In post-repository hook for ${repository_name}"
    >         echo "** synced from remote repository ${sync_uri}"
    >         echo "** synced into ${repository_path}"
    >
    >         # Gentoo comes with pregenerated cache but the other repositories
    >         # usually don't. Generate them to improve performance.
    >         if [ "${repository_name}" != "gentoo" ]; then
    >                 if ! egencache --update --repo="${repository_name}" --jobs=$(nproc)
    >                 then
    >                         echo "! ! ! egencache failed!"
    >                         ret=1
    >                 fi
    >         fi
    > fi
    >
    > # Return explicit status.
    > exit "${ret}"

# chmod +x /etc/portage/repo.postsync.d/egencache
```

### Allow search all overlays (not only installed ones)

Fetch the caches of the most popular overlays and store them locally (by default it fetches
`eix` caches from <http://gpo.zugaina.org>):
```console
# eix-remote update
```

To have the above command be run automatically on each `eix-sync`, a solution is to use a post-sync
hook:
```
$ sudo vi /etc/portage/repo.postsync.d/eix
    > #!/usr/bin/env bash
    > if [[ -e /var/cache/eix/portage.eix ]]; then
    >     cp -a /var/cache/eix/portage.eix /var/cache/eix/previous.eix;
    > fi
    > eix-update
    > if [[ -e /var/cache/eix/previous.eix ]]; then
    >     eix-diff;
    > fi
  + > eix-remote update
```

After that you will be able to search all overlays (not only installed ones) using `-R` option,
e.g. lets search for the Neovim text editor:
```console
$ eix -R neovim
```


---
## Use

* Search for installed packages (e.g. `kernel` package):
  ```console
  $ eix -I kernel
  ```

* Search in package descriptions (e.g. for `browser`):
  ```console
  $ eix -S browser
  ```

* Search in package descriptions (e.g. for `browser`) with more compact results:
  ```console
  $ eix -S -c browser
  ```

* Search in package categories (e.g. for for `connman` in the `net-misc` category):
  ```console
  $ eix -C net-misc connman
  ```

* Search by license (e.g. for the End-User License Agreement - EULA):
  ```console
  $ eix -L "EULA"
  ```

* Search by excluding results (e.g. excluding the EULA license from the search):
  ```console
  $ eix --not -L "EULA"
  ```

* Search for installed obsolete packages on the system
  ```console
  $ eix-test-obsolete
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
