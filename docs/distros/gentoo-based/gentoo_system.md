---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Gentoo system

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki//etc/portage/make.conf>
    * <https://wiki.gentoo.org/wiki/Gentoolkit>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Features>
    * <https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Portage/Files>
    * <https://wiki.gentoo.org/wiki/OpenRC>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Initscripts>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Important portage files](#important-portage-files)
* [OpenRC and init scripts](#openrc-and-init-scripts)
* [`gentoolkit`](#gentoolkit)
* [USE flags](#use-flags)
    * [Global USE flags](#global-use-flags)
    * [Local USE flags](#local-use-flags)
    * [USE flags common commands](#use-flags-common-commands)
* [KEY words](#key-words)
    * [Global KEY word](#global-key-word)
    * [Local KEY word](#local-key-word)
* [Licenses](#licenses)
    * [Global license](#global-license)
    * [Local license](#local-license)
* [Portage features](#portage-features)
* [`make.conf`](#makeconf)
    * [USE: Use flags system wide](#use-use-flags-system-wide)
    * [`ACCEPT_KEYWORDS`: stable vs testing system wide](#accept_keywords-stable-vs-testing-system-wide)
    * [`ACCEPT_LICENSE`: license system wide](#accept_license-license-system-wide)
    * [`CFLAGS`: Optimize CPU](#cflags-optimize-cpu)
    * [`CCACHE`: Optimize builds](#ccache-optimize-builds)

<!-- vim-markdown-toc -->

---
## Important portage files

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Portage/Files>

**WIP**

* `/etc/env.d` - d: files containing global variables for the apps in their name

* `/etc/portage/make.conf` - f: global settings (USE flags, compiler options...)
* `/etc/portage/make.profile` - d: portage profile symlink directory

* `/etc/portage/package.accept_keywords` - d: keyword individual packages
* `/etc/portage/package.license` - Accepted licenses
* `/etc/portage/package.use` - d: USE flags of individual packages
* `/etc/portage/package.mask` -
* `/etc/portage/package.unmask` -

* `/usr/share/portage/config/make.conf` - f: portage config file having precedence
* `/usr/share/portage/config/make.globals` - f: portage default config file
* `/usr/share/portage/config/make.defaults`

* `/var/lib/portage/world` - d: list of explicitly installed package atoms

* `/var/db/pkg` - d: contains a files set about the installation of every package

* `/var/lib/portage/world` - f: the selected packages set contains the user selected `@world`
  packages


---
## OpenRC and init scripts

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/OpenRC>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Initscripts>

`OpenRC` is a dependency based init system that maintains compatibility with the system provided
init program.

* Print the status of every services:
```console
$ rc-status --all
```

* Print the level of every services:
```console
$ rc-status --all
```

TODO


---
## `gentoolkit`

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Gentoolkit#euse>


---
## USE flags

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/USE_flag>
    * <https://wiki.gentoo.org/wiki/Handbook:Parts/Working/USE>
    * <https://wiki.gentoo.org/wiki//etc/portage/package.use>
    * <https://www.gentoo.org/support/use-flags/>

A USE flag is a variable that embodies support and dependency information for a certain concept. If
you add a USE flag (e.g. `kde`), Portage will compile the related packages accordingly (e.g. with
`kde` support). On the contrary you can specify to remove a USE flag (e.g. `-kde`) to not support
it. Of course this also alters the dependency information for a package.

Check USE flags of the system:
```console
$ emerge --info | grep ^USE # list currently active USE settings
$ less /usr/portage/profiles/use.desc # full desc. on the available USE flags
```

Check USE flags of a specific package:
```console
$ emerge -pv pkg_name # check, among other things, available use flags of a pkg
or
$ equery uses pkg_name
```

### Global USE flags

See [USE: Use flags system wide](#use-use-flags-system-wide).

### Local USE flags

One can change local USE flags in `/etc/portage/package.use`

* See `/usr/portage/profiles/use.local.desc`
* See <https://www.gentoo.org/support/use-flags/#local>

E.g. enabling the `bluray` USE flag for `vlc`, and disabling `bzip2` for `php`:
```console
# echo "media-video/vlc bluray" >> /etc/portage/package.use/vlc
# echo "dev-lang/php -bzip2" >> /etc/portage/package.use/php
```

After modifying a local (specific to a package) USE flag: update the related package
```console
# emerge -a specific-package-to-update
```

### USE flags common commands

```console
$ man euse # gather more information on euse by reading its manual page

$ euse -i png # obtain descriptions and usage of png USE flag

$ euse -i | grep png # obtain a description of png USE flag

$ equery hasuse png # show what packages have png USE flag

$ eix --installed-with-use png # show what packages are built with png USE flag

$ equery uses package_name # show what USE flags are available for a package
```


---
## KEY words

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/KEYWORDS>
    * <https://wiki.gentoo.org/wiki/ACCEPT_KEYWORDS>
    * <https://wiki.gentoo.org/wiki/Knowledge_Base:Accepting_a_keyword_for_a_single_package>
    * <https://wiki.gentoo.org/wiki//etc/portage/package.accept_keywords>

`KEYWORDS` variable informs in which architectures the package is stable or still in testing phase.

Examples of possible key words values: `alpha`, `amd64`, `arm`, `arm64` ...

The prefix `~` (tilde character) placed in front of an architecture means that it is in a "testing
phase" and is not ready for production usage.

### Global KEY word

See [`ACCEPT_KEYWORDS`: stable vs testing system
wide](#accept_keywords-stable-vs-testing-system-wide)

### Local KEY word

If your system is an `amd64` based architecture and you just want the last version of a specific
package, not available on the stable branch (e.g. with the `rust` package):
```console
# echo "dev-lang/rust ~amd64" >> /etc/portage/package.accept_keywords
```

---
## Licenses

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Portage#Licenses>
    * <https://wiki.gentoo.org/wiki/License_groups>
    * <https://projects.gentoo.org/council/meeting-logs/20190210-summary.txt>
    * <https://bugs.gentoo.org/676248>
    * <https://www.gentoo.org/glep/glep-0023.html>

Since `2019-05-23` (<https://www.gentoo.org/support/news-items/2019-05-23-accept_license.html>) the
default accepted license has changed, now only free software and documentation will be installable.

### Global license

See [`ACCEPT_LICENSE`: license system wide](#accept_license-license-system-wide)

### Local license

It also can be specified on a per package basis in `/etc/portage/package.license`.

For example, to allow the `app-arch/unrar` and `sys-kernel/linux-firmware` packages to be
installed, the following lines would have to be added to `/etc/portage/package.license`:
```console
# vi /etc/portage/package.license
    > ...
    > app-arch/unrar unRAR
    > sys-kernel/linux-firmware @BINARY-REDISTRIBUTABLE
    > ...
```

Other `/etc/portage/package.license` examples:
```console
# vi /etc/portage/package.license
    > ...
    > # Accepting google-chrome license for www-client/google-chrome for version equal or greater
    > # than 42.0.2311.90_p1:
    > >=www-client/google-chrome-42.0.2311.90_p1 google-chrome
    >
    > # Accepting google-chrome license for any version of www-client/google-chrome:
    > www-client/google-chrome google-chrome
    >
    > # Accepting google-chrome license for every www-client package at any version:
    > www-client/* google-chrome
    >
    > # Accepting google-chrome license for every package at any version:
    > */* google-chrome
    >
    > # Accepting every license for every package at any version (not a good idea):
    > */*  *
```


---
## Portage features

Full overview of portage features :
```console
$ man make.conf
```

!!! Note ""
    Portage features includes `distcc`, `ccache`, `prebuilt-packages`...


---
## `make.conf`

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki//etc/portage/make.conf>

### USE: Use flags system wide

One can change global USE flags in `/etc/portage/make.conf`

* See `/usr/portage/profiles/use.desc`
* See <https://www.gentoo.org/support/use-flags/#global>

E.g. enabling `ldap` USE flag, and disabling `kde` and `gnome` all system wild:
```console
# euse -D kde gnome
# euse -E ldap
$ grep USE /etc/portage/make.conf # check that the use flags have been append:
    > USE="... -kde -gnome ldap"
```

!!! Warning "Warning"
    After modifying the USE variable in your `make.conf`, don't forget to update the system using
    the following command so the changes take effect: `# emerge --ask --changed-use --deep @world`

### `ACCEPT_KEYWORDS`: stable vs testing system wide

If your system is an `amd64` based architecture and you want all your packages as bleeding edge as
possible:
```console
# vi /etc/portage/make.conf
    > ...
    > ACCEPT_KEYWORDS="~amd64"
    > ...
```

!!! Warning "Warning"
    After modifying the `ACCEPT_KEYWORDS` variable in your `make.conf`, don't forget to do a [full
    update of your system](./gentoo_packages.md#update-all-system) for the changes to take effect.

### `ACCEPT_LICENSE`: license system wide

The `ACCEPT_LICENSE` variable can be modified in `/etc/portage/make.conf` for a global impact,
e.g.:
```console
# sudo vi /etc/portage/make.conf
    > ...
    > ACCEPT_LICENSE="* -@EULA"  # any license exept EULA
    > ...
```

### `CFLAGS`: Optimize CPU

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/GCC_optimization>
    * <https://wiki.gentoo.org/wiki/Upgrading_GCC>
    * <https://wiki.gentoo.org/wiki/Safe_CFLAGS>
    * <https://wiki.gentoo.org/wiki/CPU_FLAGS_X86>
    * <https://github.com/InBetweenNames/gentooLTO> (if one want to go far further the optimization road)

TODO

Install and run `app-portage/cpuid2cpuflags`

```console
$ sudo -i
# echo "*/* $(cpuid2cpuflags)" >> /etc/portage/package.use/00cpuflags
```

### `CCACHE`: Optimize builds

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Ccache>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Features#Caching_compilation_objects>

TODO

The `ccache` directory might involve some space to be useful (the default configuration limit its
space to 100 Go)...


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
