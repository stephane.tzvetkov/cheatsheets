---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# q applets

The q applets are a collection of small, fast Portage query utilities written in C. These are meant
to offer a faster but more limited alternative to their Gentoolkit counterparts (but they were not
created to replace Gentoolkit).

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Q_applets>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

```console
# emerge --ask app-portage/portage-utils
```

---
## Use

List of related q applets cheat sheets:

* [`qlop`](./qlop.md)
* **TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
