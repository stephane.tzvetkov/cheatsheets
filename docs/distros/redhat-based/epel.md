---
tags:
  - Linux Distributions
  - Distros
  - Red Hat Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# EPEL

**TODO**

EPEL is an extra software package repository for RedHat based distros, made by the Fedora team.
They provide free and open source packages that are not available in the official package
repository. EPEL packages are very high quality and very stable.

???+ Note "Reference(s)"
    * <https://fedoraproject.org/wiki/EPEL>
    * <https://linuxhint.com/epel_centos/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [On CentOS 7](#on-centos-7)

<!-- vim-markdown-toc -->

---
## On CentOS 7

If the extra repository is already enabled:
```console
$ sudo yum repolist | grep "extras/7/x86_64"
```

Then you can just run:
```console
$ sudo yum install epel-release
```

**WIP**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
