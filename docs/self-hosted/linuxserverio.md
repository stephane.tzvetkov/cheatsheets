---
tags:
  - Self-hosted
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `linuxserver.io`

`linuxserver.io` is an organization building and maintaining community containers images (Docker
images). Their primary goal is to provide easy-to-use and streamlined Docker images with clear and
concise documentation.


???+ Note "Reference(s)"
    * <https://www.linuxserver.io/>
    * <https://docs.linuxserver.io/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

---
## Use

List of related `linuxserver.io` Docker images cheat sheets: **TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
