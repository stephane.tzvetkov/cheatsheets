---
tags:
  - Self-hosted
  - RSS
  - Feed Reader
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Tiny Tiny RSS

Tiny Tiny RSS is a free and open source web-based news feed (RSS/Atom) reader and aggregator

???+ Note "Reference(s)"
    * <https://tt-rss.org/>
    * <https://tt-rss.org/wiki/InstallationNotes>
    * <https://git.tt-rss.org/fox/tt-rss.git>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
