---
tags:
  - Storage
---


<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# pCloud

pCloud is a cloud storage that provides access to all your digital content including images, video,
audio, documents, etc.

pCloud Console Client (`pcldoudcc`) is the console line interface tool used to access pCloud from
your terminal. This is the application that will be detailed here.

???+ Note "Reference(s)"
    * <https://github.com/pcloudcom/console-client>
    * <https://www.pcloud.com/features/file-management.html>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Tips and tricks](#tips-and-tricks)
        * [Custom FUSE options](#custom-fuse-options)
    * [Troubleshooting](#troubleshooting)
        * [Error `libpcloudcc_lib.so: cannot open shared object file`](#error-libpcloudcc_libso-cannot-open-shared-object-file)

<!-- vim-markdown-toc -->


---
## Install

!!! Note ""

    === "apk"
        TODO

    === "apt"
        ```console
        $ sudo apt-get install cmake zlib1g-dev libboost-system-dev libboost-program-options-dev libpthread-stubs0-dev libfuse-dev libudev-dev fuse build-essential git mkdir console-client
        $ git clone https://github.com/pcloudcom/console-client.git ./console-client/
        $ cd ./console-client/pCloudCC/
        $ cd lib/pclsync/
        $ make clean
        $ make fs
        $ cd ../mbedtls/
        $ cmake .
        $ make clean
        $ make
        $ cd ../..
        $ cmake .
        $ make
        $ sudo make install
        $ sudo ldconfig
        $ pcloudcc -u username -p
        ```

    === "dnf"
        TODO

    === "emerge"
        TODO

    === "nix"
        TODO

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/pcloudcc-git.git
        $ cd pcloudcc-git
        $ makepkg -is # --syncdeps to auto-install deps, --install to install after building
        ```

    === "yum"
        TODO

    === "xbps"
        TODO

    === "zypper"
        TODO

---
## Config


* Create a dedicated pCloud user:

    ```console
    $ sudo useradd --create-home pcloud
    ```

* For any user who will have to use the pcloud mount point:

    ```console
    $ sudo usermod -aG pcloud $USER
    $ newgrp pcloud
    $ groups
    ```

* Connect a first time:

    ```console
    $ sudo su pcloud
    $ pcloudcc -s -u username -p # connect, then Ctrl-C
    $ exit
    ```

    From now on, only `$ pcloudcc -u <username>` will be needed for the `pcloud` user. Indeed, the
    password has been saved in the `/home/pcloud/.pcloud` directory, thanks to the `-s` option).

* Create a mount point:

    ```console
    $ sudo mkdir /path/to/mount/point/for/pcloud
    $ sudo chown pcloud:pcloud /path/to/mount/point/for/pcloud
    $ sudo chmod og=rwx,o=x /path/to/mount/point/for/pcloud
    $ sudo chmod g+s /path/to/mount/point/for/pcloud # set setgid bit for files/dirs under /path/to/mount/point/for/pcloud to inherit group rights
    ```

* Create a systemd service:

    ```console
    $ sudo vi /usr/lib/systemd/system/pcloud.service

        > [Install]
        > WantedBy=multi-user.target
        >
        > [Unit]
        > After=network.target
        > Description=pCloud Console Client systemd service
        >
        > [Service]
        > ExecStart=/bin/pcloudcc -d -u username -m /path/to/mount/point/for/pcloud # or `/usr/local/bin/pcloudcc` if you build it yourself
        > User=plcoud
        > Group=plcoud

    $ sudo systemctl daemon-reload
    $ sudo systemctl enable pcloud.service
    $ sudo systemctl start pcloud.service

    $ sudo systemctl status pcloud.service
    $ sudo journalctl -u pcloud.service
    ```

* <https://github.com/zcalusic/dockerfiles/tree/master/pcloud>

* <https://github.com/jlloyola/pcloudcc_docker>

---
## Use

* Print help:

    ```console
    $ pcloudcc -h
        pCloud console client v.2.0.1
        Allowed options:
          -h [ --help ]             produce help message
          -u [ --username ] arg     pCloud account name
          -p [ --password ]         Ask pCloud account password
          -c [ --crypto ]           Ask crypto password
          -y [ --passascrypto ] arg Use user password as crypto password also.
          -d [ --daemonize ]        Daemonize the process.
          -o [ --commands  ]        Parent stays alive and processes commands.
          -m [ --mountpoint ] arg   Mount point where drive to be mounted.
          -k [ --commands_only ]    Daemon already started pass only commands
          -n [ --newuser ]          Switch if this is a new user to be registered.
          -s [ --savepassword ]     Save password in database.
    ```

### Tips and tricks

#### Custom FUSE options

pCloudCC is based on [FUSE](https://wiki.archlinux.org/title/FUSE)
([fusermount3](https://man.archlinux.org/man/fusermount3.1)).

At the moment (06/2023), if you want to specify your own FUSE option, e.g. by adding the
`allow_other` option (see <https://github.com/pcloudcom/console-client/issues/56>), then you'll
have to patch the code base and (re)build pCloudCC :

```console
$ git clone https://github.com/pcloudcom/console-client.git ./console-client/
$ cd ./console-client
$ vi pCloudCC/lib/pclsync/pfs.c
    > ...
    > #if defined(P_OS_LINUX)
    >   fuse_opt_add_arg(&args, "argv");
    >   fuse_opt_add_arg(&args, "-oauto_unmount");
  + >   fuse_opt_add_arg(&args, "-oallow_other");
    > ...

$ sudo vi /etc/fuse.conf
    > # The file /etc/fuse.conf allows for the following parameters:
    > #
    > # user_allow_other - Using the allow_other mount option works fine as root, in
    > # order to have it work as user you need user_allow_other in /etc/fuse.conf as
    > # well. (This option allows users to use the allow_other option.) You need
    > # allow_other if you want users other than the owner to access a mounted fuse.
    > # This option must appear on a line by itself. There is no value, just the
    > # presence of the option.
    >
  ~ > user_allow_other
    >
    >
    > # mount_max = n - this option sets the maximum number of mounts.
    > # Currently (2014) it must be typed exactly as shown
    > # (with a single space before and after the equals sign).
    >
    > #mount_max = 1000
```

Then (re)build pCloudCC (e.g. like described in the [install section](#install) for `apt` users.


### Troubleshooting

#### Error `libpcloudcc_lib.so: cannot open shared object file`

If you build pCloudCC yourself, and you encounter the error `libpcloudcc_lib.so: cannot open shared
object file` when runnging `pcloudcc`, then: <https://stackoverflow.com/a/17892806>.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
