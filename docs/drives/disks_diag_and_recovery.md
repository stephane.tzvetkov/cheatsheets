---
tags:
  - Drives
  - Disks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Disks diagnostics and recovery

**TODO**

???+ Note "Reference(s)"
    * <https://www.cgsecurity.org/wiki/TestDisk>
    * <https://www.cgsecurity.org/wiki/PhotoRec>
    * <http://safecopy.sourceforge.net/>
    * <https://www.gnu.org/software/ddrescue/ddrescue.html>
    * <https://sourceforge.net/projects/ext4magic/>
    * <https://wiki.archlinux.org/index.php/File_recovery>
    * <https://wiki.archlinux.org/index.php/Post_recovery_tasks>
    * <https://wiki.gentoo.org/wiki/TestDisk>
    * <https://wiki.gentoo.org/wiki/Ddrescue>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Smartmontools](#smartmontools)
* [TestDisk and PhotoRec](#testdisk-and-photorec)
* [`fsck`](#fsck)

<!-- vim-markdown-toc -->


---
## Smartmontools

See [smartmontools](./smartmontools.md).

---
## TestDisk and PhotoRec

Install TestDisk and PhotoRec:

!!! Note ""

    === "pacman"
        ```console
        # pacman -S testdisk
        ```

**TODO**

---
## `fsck`

* Just check a disk:
```console
$ sudo umount /dev/sdb2
$ sudo fsck -n -r -C -v -V -f /dev/sdb2
```

* Check and eventually repair when asked:
```console
$ sudo umount /dev/sdb2
$ sudo fsck -r -C -v -V -f /dev/sdb2
```

* Check and automatically repair a disk
```console
$ sudo umount /dev/sdb2
$ sudo fsck -p -r -C -v -V -f /dev/sdb2
```

    * `-V`: produce verbose output (at `fsck` level)
    * `-v`: produce verbose output (at `e2fsck` level)
    * `-n`: non interactive mode without correcting errors (at `e2fsck` level)
    * `-p`: non interactive mode auto correcting errors (at `e2fsck` level)
    * `-r`: report some statistics after `fsck` completes (like exit status) (at `fsck` level)
    * `-C`: display a completion/progress bar (at `fsck` level)
    * `-f`: force checking even if the file system seems clean (at `e2fsck` level)


```console
fsck from util-linux 2.36.1
[/usr/bin/fsck.ext4 (1) -- /dev/sdb2] fsck.ext4 -v -f -n -C0 /dev/sdb2
e2fsck 1.45.6 (20-Mar-2020)
Pass 1: Checking inodes, blocks, and sizes
Inode 14942216 extent tree (at level 1) could be narrower.  Optimize? no

Inode 14942232 extent tree (at level 2) could be narrower.  Optimize? no

Inode 14942238 extent tree (at level 2) could be narrower.  Optimize? no

Inode 14942276 extent tree (at level 2) could be narrower.  Optimize? no

Inodes that were part of a corrupted orphan linked list found.  Fix? no

Inode 16780528 was part of the orphaned inode list.  IGNORED.
Deleted inode 29098109 has zero dtime.  Fix? no

Deleted inode 29098225 has zero dtime.  Fix? no

Pass 2: Checking directory structure
Pass 3: Checking directory connectivity
Pass 4: Checking reference counts
Pass 5: Checking group summary information
Block bitmap differences:  -134783328 -161286176
Fix? no

Inode bitmap differences:  -16780528 -29098109 -29098225
Fix? no


/dev/sdb2: ********** WARNING: Filesystem still has errors **********


     1014068 inodes used (1.68%, out of 60514304)
        2568 non-contiguous files (0.3%)
         582 non-contiguous directories (0.1%)
             # of inodes with ind/dind/tind blocks: 0/0/0
             Extent depth histogram: 986384/291/3
    77964626 blocks used (32.21%, out of 242027264)
           0 bad blocks
          45 large files

      861319 regular files
      121120 directories
         171 character device files
           0 block device files
          10 fifos
      111311 links
       31409 symbolic links (27171 fast symbolic links)
          27 sockets
------------
     1125367 files
/dev/sdb2: status 4, rss 50412, real 58.238740, user 7.831017, sys 2.334391
```

---

Sometimes you might end up on a case like this:

<https://unix.stackexchange.com/questions/598087/ext4-bad-magic-number-in-super-block>

I.e. your are trying to repair a super block error (like described here:
<https://linuxexpresso.wordpress.com/2010/03/31/repair-a-broken-ext4-superblock-in-ubuntu/>), but it
doesn't work...

If this is a GPT disk, you might want to take a look with `gdisk`.

But first, **backup your partition table**:
```console
# sfdisk -d /dev/disk > disk.dump
```

Then use `gdisk`:
```console
# gdisk /dev/disk
    > p # print partition table
```

You will probably find a bad partition table !


---

```console
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.9.10-artix1-1] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

== START OF READ SMART DATA SECTION ==
SMART Error Log Version: 1
No Errors Logged

[latitude] ❯❯❯❯ sudo smartctl -d sat -l selftest /dev/sdb                                                                                  ~
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.9.10-artix1-1] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

== START OF READ SMART DATA SECTION ==
SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Extended offline    Completed: read failure       40%     32017         1374022944
# 2  Short offline       Completed without error       00%     32016         -
# 3  Short offline       Completed without error       00%     32012         -
# 4  Short offline       Completed without error       00%     32012         -
# 5  Short offline       Completed without error       00%         0         -
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
