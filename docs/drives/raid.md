---
tags:
  - Drives
  - Disks
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Software RAID

Redundant Array of Inexpensive Disks (RAID) is a technology to combine multiple disks in order to
improve their reliability and/or performance. There is hardware RAID, implemented by the
controller on your motherboard or specific extension cards, and there is software RAID, implemented
by the kernel.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/RAID>
    * <https://wiki.gentoo.org/wiki/Complete_Handbook/Software_RAID>
    * <https://en.wikipedia.org/wiki/Standard_RAID_levels>
    * <https://en.wikipedia.org/wiki/RAID>
    * <http://www.raid-calculator.com/default.aspx>
    * <https://www.zdnet.com/article/why-raid-6-stops-working-in-2019/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Software RAID](#software-raid)
* [Hardware RAID](#hardware-raid)

<!-- vim-markdown-toc -->

---
## Software RAID

There is at least 3 ways of doing software RAID:

1. [`mdadm`](./mdadm.md)
2. [LVM](./lvm.md)
3. file systems RAID (e.g. [Btrfs](./btrfs.md), [ZFS](./zfs.md) etc)

---
## Hardware RAID

* [MegaCLI](./../hardware/megacli.md)
* [StorCLI](./../hardware/storcli.md)
* [PercCLI](./../hardware/perccli.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
