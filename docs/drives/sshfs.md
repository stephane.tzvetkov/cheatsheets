---
tags:
  - Drives
  - Disks
  - File Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# SSHFS

SSHFS is a FUSE based file system client for mounting remote directories over a Secure Shell
connection.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/SSHFS>
    * <https://wiki.archlinux.org/index.php/SSHFS>
    * <https://en.wikipedia.org/wiki/SSHFS>

---
## Table of contents

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        $ sudo make nconfig # or `$ sudo make menuconfig`

            # Activate Btrfs in the kernel
            # Double check here: <https://wiki.gentoo.org/wiki/SSHFS#Kernel>
            #
            > File systems  ->
            >     [*] FUSE (Filesystem in Userspace) support
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge"
        ```console
        $ sudo emerge -a net-fs/sshfs
        ```

    === "pacman"
        ```console
        $ sudo pacman -S sshfs
        ```

    === "apt"
        ```console
        $ sudo apt install sshfs
        ```

    === "yum"
        ```console
        $ sudo yum install sshfs
        ```

    === "dnf"
        ```console
        $ sudo dnf install sshfs
        ```

---
## Config

You may want to restrict a specific user to a specific directory on the remote system. This can be
done by editing `/etc/ssh/sshd_config`:
```console
$ sudo vi /etc/ssh/sshd_config
    > ...
    > Match User someuser
    >        ChrootDirectory /chroot/%u
    >        ForceCommand internal-sftp
    >        AllowTcpForwarding no
    >        X11Forwarding no
    > ...
```

---
## Use

* Mount remote folder via SSHFS:
```console
$ sshfs [user@]host:[/path/to/remote/folder] /path/to/mountpoint [options]
```

* Unmount:
```console
$ fusermount3 -u /path/to/mountpoint
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
