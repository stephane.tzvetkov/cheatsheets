---
tags:
  - Drives
  - Disks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `inodes`

???+ Note "Reference(s)"
    * <https://www.cyberciti.biz/tips/understanding-unixlinux-filesystem-inodes.html>
    * <https://www.howtogeek.com/465350/everything-you-ever-wanted-to-know-about-inodes-on-linux/>
    * <https://en.wikipedia.org/wiki/Inode>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
