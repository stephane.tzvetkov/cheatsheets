---
tags:
  - Drives
  - Disks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `zram`

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Zram>
    * <https://www.youtube.com/watch?v=7pcp4e8am_c>
    * <https://www.linuxtricks.fr/wiki/zram-compresser-la-ram-au-lieu-de-swapper-sur-linux>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
