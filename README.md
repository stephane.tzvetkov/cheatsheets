# README

This project is just a collection of personal cheat sheets, about various GNU/Linux related topics.

Most of them are still very much **TODO** or in a **Work In Progress** state.

**All of them should be treated with caution!**

---
## Check it online

Those cheat sheets are meant to be read [online](https://cheatsheets.stephane.plus) (alternatively,
a backup is also available [here](https://stephane-cheatsheets.readthedocs.io/en/latest/) on
<https://readthedocs.io>), they are not meant to be read directly from their [GitLab
repository](https://gitlab.com/stephane.tzvetkov/cheatsheets) because of the way this project will
enhance their rendering.

---
## How to use

* You can browse the cheat sheets [online](https://cheatsheets.stephane.plus) (alternatively, a
  backup is also available [here](https://stephane-cheatsheets.readthedocs.io/en/latest/) on
  <https://readthedocs.io>).

* You can also clone this project and look at the cheat sheets from your terminal:
  ```console
  $ git clone https://gitlab.com/stephane.tzvetkov/cheatsheets.git
  $ cd cheatsheets/docs
  $ ls
  ```

---
## How to contribute

### Context

The cheat sheets of this project are simply written with the [Markdown markup
language](https://www.markdownguide.org/) (they are located in the `./docs` directory, see all the
`*.md` files). Thus, they are very easy to read/write with any text editor.

Those cheat sheets are "build" into a simple static website, thanks to
[MkDocs](https://www.mkdocs.org/). This is a nice way of generating readable technical
documentation, and I find it very appropriated for this project.

Both the [Markdown markup language](https://www.markdownguide.org/) and
[MkDocs](https://www.mkdocs.org/) are greatly enhanced by [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/) (a theme/plugin for
[MkDocs](https://www.mkdocs.org/)). This makes it possible to add [*a lot* of
features](https://squidfunk.github.io/mkdocs-material/reference/) that I find very interesting,
especially for the cheat sheets format.

Also, I got access to [Material for MkDocs
insiders](https://squidfunk.github.io/mkdocs-material/insiders/), i.e. "premium" features of
Material for MkDocs (because I sponsor the project from time to time, in order to support their
work a bit). So, I made a *private* fork of their repository (as allowed by [their
terms](https://squidfunk.github.io/mkdocs-material/insiders/#terms)), this allows me to use their
advanced functionalities.

Note that, in order to build this project, you *don't* need to also be part of [Material for MkDocs
insiders](https://squidfunk.github.io/mkdocs-material/insiders/). If you are not part of it, then
after building, your locally generated static website - hosting the cheat sheets for you to see how
they render - will just miss a few features (which is not a big deal, really).

### Installation

0. ⚠️ Prerequisites ⚠️:
    * [Python 3](https://www.python.org/about/gettingstarted/)
    * [Poetry](https://python-poetry.org/docs/)
    * [`aspell`](https://repology.org/project/aspell/versions)

1. Clone this project:
   ```console
   $ git clone git@gitlab.com:stephane.tzvetkov/cheatsheets.git
   ```

2. Clone the [mkdocs-material](https://squidfunk.github.io/mkdocs-material/getting-started/)
   repository next to this README file (or where you want, but with a `mkdocs-material` symlink
   next to this file):
    * If you *are not* part of
      [mkdocs-material insiders](https://squidfunk.github.io/mkdocs-material/insiders/):
      ```console
      $ git clone https://github.com/squidfunk/mkdocs-material.git
      ```
    * If you *are currently* part of
      [mkdocs-material insiders](https://squidfunk.github.io/mkdocs-material/insiders/):
      ```console
      $ git clone git@github.com:squidfunk/mkdocs-material-insiders.git mkdocs-material
      ```
    * If you *were* part of
      [mkdocs-material insiders](https://squidfunk.github.io/mkdocs-material/insiders/)
      *and you made a private fork*:
      ```console
      $ git clone git@git-sever.com:username/private-fork-of-mkdocs-material-insiders.git mkdocs-material
      ```
   (See the [side note](#side-note) section at the end of this README for more details about why
   this extra step is needed and not handled by poetry)

3. Install the project dependencies (in a virtual environment) with `poetry`:
   ```console
   $ poetry install
   ```

Make sure the installation went well by following the below [Run](#run) section.

### Run

After editing some of the cheat sheets of this project, you might want to see how your contribution
will look like. In order to do so, just build and serve the cheat sheets to a local address (e.g.
`localhost:1234`):

* If you *have not* cloned a [mkdocs-material
  insiders](https://squidfunk.github.io/mkdocs-material/insiders/) repository (*nor* a forked one)
  in the previous [installation](#installation) section:
  ```console
  $ poetry run mkdocs serve --dev-addr localhost:1234 --config-file .mkdocs.yml
    > ...
    > INFO    -  Serving on http://127.0.0.1:1234 ...
    > ...
  ```

* If you *have* cloned a [mkdocs-material
  insiders](https://squidfunk.github.io/mkdocs-material/insiders/) repository (*or* a forked one)
  in the previous [installation](#installation) section:
  ```console
  $ poetry run mkdocs serve --dev-addr localhost:1234 --config-file .mkdocs-material-insiders.yml
    > ...
    > INFO    -  Serving on http://127.0.0.1:1234 ...
    > ...
  ```

Now just open <http://127.0.0.1:1234> in a web browser and make sure the rendering of your
modifications is OK.

Note that I use a private fork of [mkdocs-material
insiders](https://squidfunk.github.io/mkdocs-material/insiders/) at version `8.1.2-insiders-4.5.0`.
If you are not part of [mkdocs-material
insiders](https://squidfunk.github.io/mkdocs-material/insiders/), or if you a part of it and using
a much older/newer version, then you might not get the same rendering (and it's OK).

### Tests

Before submitting a merge/pull request, you can additionally execute the
`run-cheat-sheets-tests.bash` script, in order to check some formatting rules, spelling mistakes,
broken links etc.

* If you *have not* cloned a [mkdocs-material
  insiders](https://squidfunk.github.io/mkdocs-material/insiders/) repository (*nor* a forked one)
  in the previous [installation](#installation) section, then execute the following:
  ```console
  $ poetry run ./tests/run-cheat-sheets-tests.bash
  ```

* If you *have* cloned a [mkdocs-material
  insiders](https://squidfunk.github.io/mkdocs-material/insiders/) repository (*or* a forked one)
  in the previous [installation](#installation) section, then execute the following:
  ```console
  $ poetry run ./tests/run-cheat-sheets-tests.bash --insiders
  ```

<!--
🚧 WIP 🚧

```console
$ poetry run checks
```
-->

### Share

Once you are satisfied with your modifications, you can simply share them with a [merge/pull
request](https://docs.gitlab.com/ee/user/project/merge_requests/) (see how to create a merge
request on GitLab
[here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)).

### Project layout

```console
$ tree . -a -I ".git|.cache|.venv|site"
.
├── .abbreviations                   # list of all the abbreviations used in the cheat sheets
├── .aspell-personal-dict            # aspell dictionary extension, adding specific words present in some cheat sheets
├── docs                             # folder containing all the cheat sheets
│   └── ...
├── .gitignore                       # file specifying untracked files / files to be ignored by git
├── mkdocs-material                  # Material-for-MkDocs git repository
│   └── ...
├── .mkdocs-material-insiders.yml    # configuration file of MkDocs (for people part of Material-for-MkDocs-insiders)
├── .mkdocs.yml                      # configuration file of MkDocs
├── poetry.lock                      # lock file of poetry
├── pyproject.toml                   # configuration file of poetry (dependencies specification, project information, build system, etc)
├── README.md                        # current file
├── .readthedocs.yml                 # configuration file for readthedocs.org
├── tests                            # folder containing all tests scripts
│   ├── run-cheat-sheets-tests.bash  # main bash script, running all the others, checking for formatting rules, spelling mistakes, broken links, etc.
│   └── ...
└── TODO.md                          # TODO list
```

### Side notes

You might have noticed that the `poetry.lock` file is ignored by Git. Even if, according to the
[poetry
documentation](https://python-poetry.org/docs/basic-usage/#commit-your-poetrylock-file-to-version-control),
it is recommended to include the `poetry.lock` file in version control, in order to ensure
reproducibility.

I made this choice because a dependency to the `mkdocs-material-insiders` repository might prevent
you from successfully building the project if you are not part of the "Material for MkDocs
insiders" (see <https://squidfunk.github.io/mkdocs-material/insiders/>). I didn't find a way to
support multiple sources for the `mkdocs-material` dependency, e.g. one source for the
mkdocs-material "general" repository, and one for the `mkdocs-material` "insiders" repository
(keeping the insiders one private, as kindly asked [here](
https://squidfunk.github.io/mkdocs-material/insiders/#terms)). So, unless someone has a better
solution to propose: the `poetry.lock` file won't be tracked.

Note that this also explains why you have to clone the `mkdocs-material` dependency (insiders or
not) "by hand" and not let poetry automatically handle it.

