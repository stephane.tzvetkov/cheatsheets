from tests.utils import log, find_markdown_files

def check_dotfile_madness_references(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_dotfile_madness_reference: list = []
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            file_content = file.read()
            if "avoid dotfile madness" in file_content \
               and "avoid_dotfile_madness.md" not in file_content :
                files_containing_bad_dotfile_madness_reference.append(file_path)

    if files_containing_bad_dotfile_madness_reference:
        log.error("The following files mention how to avoid dotefile madness without referencing " \
                  "the associated cheat sheet:" \
                  f"\n{files_containing_bad_dotfile_madness_reference}")
        log.error("Please reference the `./admin/avoid_dotfile_madness.md` cheat sheet, " \
                  "e.g. like so:" \
                  "\n`see [how to handle SomeToolName related dotfiles]" \
                  "(../admin/avoid_dotfile_madness.md#SomeToolName)`\n")

    assert not files_containing_bad_dotfile_madness_reference

def check():
    log.debug("Checking dotfile madness references...")
    check_dotfile_madness_references("./docs")
    log.info("Dotfile madness references checked!")

if __name__=="__main__":
    check()
