# some spell checking modules to try

---
## spylls

* **Not** spylls (because <https://github.com/zverok/spylls/issues/8>)

```console
from spylls.hunspell import Dictionary

# en_US dictionary is distributed with spylls
# See docs to load other dictionaries
dictionary = Dictionary.from_files('en_US')

print(dictionary.lookup('spylls is bad'))
# False
#for suggestion in dictionary.suggest('spylls'):
#    print(suggestion)
# spells
# spills
```

---
## TextBlob

* **Not** TextBlob: because execution time is around 1mn per `.md`

```console
from textblob import Word
import re
import time

def check_textblob_word(word):

    word = Word(word)

    result = word.spellcheck()

    if word == result[0][0]:
        print(f'Spelling of "{word}" is correct!')
    else:
        print(f'Spelling of "{word}" is not correct!')
        #print(f'Correct spelling of "{word}": "{result[0][0]}" (with {result[0][1]} confidence).')

def check_textblob_sentence(sentence):

    words = sentence.split()

    words = [word.lower() for word in words]

    words = [re.sub(r'[^A-Za-z0-9]+', '', word) for word in words]
    for word in words:
        check_spelchek_word(word)

with open("../docs/admin/cron.md", "r") as f:
    start = time.time()
    for line in f:
        check_textblob_sentence(line)
    end = time.time()

print("\nExecution time:", end-start) # 55s
```

---
## spelchek

* spelchek seems to be a good and simple option: <https://github.com/theodox/spelchek>

```console
import spelchek
import os
import re
import time

is_ignoring: bool = False
toggle_ignore: list[str] = ["`"]
#start_ignore: list[str] = ["<"]
start_ignore: list[str] = ["<!--", "<--"]
#stop_ignore: list[str]  = ["/>"]
stop_ignore: list[str]  = ["-->"]
special_ignore: list[str]  = ["-", "--", "--8", "br", "vim-markdown-toc", "t"]

def check_spelchek_word(word: str, line_nb: int, file_path: str):
    known = spelchek.known(word)
    if not known:
        print(f"'{word}' is not correct or not known (file {file_path}, line {line_nb})")
        #print(f"guesses: {spelchek.guesses(word)}")


def check_spelchek_sentence(sentence: str, line_nb: int, file_path: str):
    global is_ignoring
    sentence = sentence.lower()
    sentence = sentence.replace("\n", "")
    sentence = re.sub('[\"\(\)\:\;\,\.\*\#\!\?\+\/]', '', sentence)
    sentence = re.sub('[\[\]\<\>\'\’]', ' ', sentence)
    sentence = re.sub('\=\=\=', '', sentence)
    sentence = re.sub('\-\-\-', '', sentence)
    sentence = re.sub('\?\?\?', '', sentence)
    sentence = re.sub('\!\!\!', '', sentence)

    for itoggle in toggle_ignore:
        sentence = sentence.replace(itoggle, " "+itoggle+" ")
    for starti in start_ignore:
        sentence = sentence.replace(starti, " "+starti+" ")
    for stopi in stop_ignore:
        sentence = sentence.replace(stopi, " "+stopi+" ")
    words = sentence.split(' ')

    for word in words:
        if not word:
            pass
        else:
            if word in toggle_ignore:
                is_ignoring = not is_ignoring
            if word in start_ignore:
                is_ignoring = True
            if word in stop_ignore:
                is_ignoring = False

            if is_ignoring \
                    or word in toggle_ignore \
                    or word in start_ignore \
                    or word in stop_ignore \
                    or word in special_ignore \
                    or word.startswith("http"):
                pass
            else :
                check_spelchek_word(word, line_nb, file_path)

    #words = [re.sub(r'[^A-Za-z0-9]+', ' ', word) for word in words]

def add_custom_dict():
    with open("custom_dict.txt", "rt") as new_dict:
        spelchek.update_dictionary(new_dict)

def find_markdown_files(path, extension) -> list:
    markdown_files: list = []
    for root, dirs, files in os.walk(path):
        for f in files:
            if f.endswith(extension):
                markdown_files.append(root+"/"+f)
    return markdown_files

start = time.time()
add_custom_dict()
md_files_path: list = find_markdown_files("../docs", ".md")
for file_path in md_files_path:
    with open(file_path, "r") as file:
        is_ignoring = False
        line_nb: int = 0
        for line in file:
            line_nb +=1
            check_spelchek_sentence(line, line_nb, file_path)
end = time.time()

print("\nExecution time:", end-start) # 0.009s
```

---
## Speller

* ?

```console
#from autocorrect import Speller
#print(b.correct())
#
#spell = Speller()
#spell("I'm not sleapy and tehre is no place I'm giong to.")
```

---
## symspellpy

* ?

```console
#import pkg_resources
#from symspellpy import SymSpell, Verbosity
#
#sym_spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)
#dictionary_path = pkg_resources.resource_filename(
#    "symspellpy", "frequency_dictionary_en_82_765.txt"
#)
## term_index is the column of the term and count_index is the
## column of the term frequency
#sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1)
#
## lookup suggestions for single-word input strings
#input_term = "memebers"  # misspelling of "members"
## max edit distance per lookup
## (max_edit_distance_lookup <= max_dictionary_edit_distance)
#suggestions = sym_spell.lookup(input_term, Verbosity.CLOSEST, max_edit_distance=2)
## display suggestion term, edit distance, and term frequency
#for suggestion in suggestions:
#    print(suggestion)
```

---
# misc

* <https://openbookproject.net/courses/python4fun/spellcheck.html>
* <https://codereview.stackexchange.com/questions/147155/basic-spellchecker-with-file-i-o>
* <https://python-bloggers.com/2022/02/spelling-checker-program-in-python/>

