#!/bin/bash

TESTS_DIR="$(dirname -- $0)"

echo "   linkchecker..."
echo ""

PWD=$( pwd )
cd "$TESTS_DIR"/..
mkdocs serve --dev-addr 127.0.0.1:4321 --no-livereload --config-file .mkdocs.yml &
pid=$!
cd "$PWD"
sleep 120

if linkchecker http://127.0.0.1:4321
then
    kill $pid
    exit 0
else
    kill $pid
    exit 1
fi
