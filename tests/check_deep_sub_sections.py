from tests.utils import log, find_markdown_files

expected_deep_sub_section_declaration = '''

### The title of your deep sub section here

'''

expected_deeper_sub_section_declaration = '''

#### The title of your deeper sub section here

'''

def check_deep_sub_sections(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_deep_sub_section: list = []
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            file_content = file.read()

            lines: list[str] = file_content.splitlines()
            good_deep_sub_section: bool = True
            prev_line: str = "/"
            prev_prev_line: str = "/"
            i: int = 0
            for current_line in lines :
                if i >= 1 :
                    prev_line = lines[i-1]
                if i >= 2 :
                    prev_prev_line = lines[i-2]

                if prev_line.startswith("### ") :
                    good_deep_sub_section = False
                    if (current_line == ""
                       and prev_prev_line == "") :
                        # "good" sub section if:
                        # - current line is a line return after the deep sub section
                        # - prev line is the sub section declaration
                        # - prev prev line is a line return before the sub section
                        good_deep_sub_section = True
                        break

                i += 1

            if not good_deep_sub_section :
                files_containing_bad_deep_sub_section.append(file_path)

    if files_containing_bad_deep_sub_section:
        log.error("The following files contains \"deep\" sub sections without the expected " \
                  f"format: \n{files_containing_bad_deep_sub_section}")
        log.error("The expected \"deep\" sub section declaration format (note that line " \
                  "returns matter) is e.g.:" \
                  f"\n```{expected_deep_sub_section_declaration}```" \
                  "\nor e.g.:" \
                  f"\n```{expected_deeper_sub_section_declaration}```\n")

    assert not files_containing_bad_deep_sub_section

def check():
    log.debug("Checking deep sub sections...")
    check_deep_sub_sections("./docs")
    log.info("Deep sub sections checked!")

if __name__=="__main__":
    check()
