#!/bin/sh

set -e

# Check spelling mistakes
find ./docs -type f -iname "*.md" | while read -r file
do
    # aspell --mode=markdown --lang=en --personal="$PWD/.aspell-personal-dict" --dont-backup check "$file"
    check=$(aspell --mode=markdown --lang=en --home-dir=./. --personal=./.aspell-personal-dict list < "$file")
    if [ -n "$check" ]; then
        echo "---"
        echo "$file"
        echo ""
        echo "$check"
        echo ""
    fi

done
