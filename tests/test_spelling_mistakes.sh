#!/bin/sh

set -e

# Check spelling mistakes
find ../docs -type f -iname "*.md" | while read -r file
do
    # aspell --mode=markdown --lang=en --personal="$PWD/.aspell-personal-dict" --dont-backup check "$file"
    check=$(aspell --mode=markdown --lang=en --home-dir=./.. --personal=./../.aspell-personal-dict list < "$file")
    if [ -n "$check" ]; then
        >&2 echo "❌ spelling mistakes"
        >&2 echo ""
        >&2 echo "According to Aspell, the file '$file' contains the following spelling "
        >&2 echo "mistake(s):"
        >&2 echo ""
        >&2 echo "$check"
        >&2 echo ""
        >&2 echo "If you think some of those words are spelled correctly, then you can add them to "
        >&2 echo "'$PWD/.aspell-personal-dict'."
        >&2 echo ""
        >&2 echo "Tip: you can run './tests/print_spelling_mistakes.sh'"
        >&2 echo "in order to get all spelling mistakes at once (and associated files)."
        return 1
    fi
done

echo "✅ spelling mistakes"
