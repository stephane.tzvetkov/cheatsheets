import os

from tests.utils import log, find_markdown_files

EXPECTED_HEADER: str = '''

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


'''

EXPECTED_HEADER_FORMAT: str = '''
⏎
<!-- Load abbreviations -->⏎
--8<-- ".abbreviations"⏎
⏎
!!! Warning " "⏎
    This document is a **WORK IN PROGRESS**.<br/>⏎
    This is just a quick personal cheat sheet: treat its contents with caution!⏎
---
⏎
⏎
'''

EXPECTED_FOOTER: str = '''


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
'''

EXPECTED_FOOTER_FORMAT: str = '''
⏎
⏎
---⏎
⏎
!!! Star " "⏎
    If this cheat sheet has been useful to you, then please consider leaving a star⏎
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
'''

allowed_files_without_headers_and_footers: list[str] = [
    "index.md",
    "tags.md",
]

def check_headers_and_footers(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_a_bad_header: list = []
    files_containing_a_bad_footer: list = []
    for file_path in md_files_path:
        file_basename: str = os.path.basename(file_path)
        if file_basename in allowed_files_without_headers_and_footers :
            continue  # skip files that do not require a header and a footer
        with open(file_path, "r") as file:
            file_content = file.read()
            if EXPECTED_HEADER not in file_content :
                files_containing_a_bad_header.append(file_path)
            if not file_content.endswith(EXPECTED_FOOTER) :
                files_containing_a_bad_footer.append(file_path)

    if files_containing_a_bad_header:
        log.error("The following files do not conform to the expected header format:" \
                  f"\n{files_containing_a_bad_header}")
        log.error("The expected header format (note that line returns matter) is:" \
                  f"\n```{EXPECTED_HEADER_FORMAT}```")
    if files_containing_a_bad_footer :
        log.error(f"The following files do not conform to the expected footer format: \
                  \n{files_containing_a_bad_footer}")
        log.error("The expected footer format (note that line returns matter) is:" \
                  f"\n```{EXPECTED_FOOTER_FORMAT}```\n")

    assert not files_containing_a_bad_header
    assert not files_containing_a_bad_footer

def check():
    log.debug("Checking headers and footers...")
    check_headers_and_footers("./docs")
    log.info("headers and footers checked!")

if __name__=="__main__":
    check()
