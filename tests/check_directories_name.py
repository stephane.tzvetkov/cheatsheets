from tests.utils import log, find_directories

def check_no_underscore_in_directory_name(directories: list[str]):

    banned_directories_name_with_underscore: list = []
    for directory_name in directories:
        if "_" in directory_name:
            banned_directories_name_with_underscore.append(directory_name)

    if banned_directories_name_with_underscore:
        log.error("The following directories name contain the underscore character (`_`) " \
                  "instead of the dash character (`-`):" \
                  f"\n{banned_directories_name_with_underscore}" \
                  "\nPlease replace `_` with `-` (the directory naming convention used here) " \
                  "in those directories name.\n")

    assert not banned_directories_name_with_underscore

def check_directories_name_syntax(docs_location: str):
    directories: list = find_directories(docs_location)
    check_no_underscore_in_directory_name(directories)

def check():
    log.debug("Checking directories name...")
    check_directories_name_syntax("./docs")
    log.info("directories name checked!")

if __name__=="__main__":
    check()
