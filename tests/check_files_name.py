"""
Script checking markdown files name format.
"""

import os

from tests.utils import log, find_markdown_files

# allowed_files_basename_with_dash: list = ["foo-.md", "bar_.md"]
allowed_files_basename_with_dash: list[str] = [
        "modprobed-db.md",
        "rich-cli.md",
        "ripgrep-all.md",
        "s-nail.md",
        "syslog-ng.md",
        "webapp-config.md"
    ]


def check_files_name_syntax(docs_location: str) -> None:
    """
    For each markdown file found, check the format of his name.
    """
    md_files_path: list[str] = find_markdown_files(docs_location, ".md")
    banned_files_basename_with_dash: list[str] = []

    for file_path in md_files_path:
        file_basename: str = os.path.basename(file_path)
        if "-" in file_basename and file_basename not in allowed_files_basename_with_dash:
            banned_files_basename_with_dash.append(file_basename)
            # log.debug(f"adding banned file: {file_basename}")

    if banned_files_basename_with_dash:
        log.error(f"The following files name contain the dash character (`-`) " \
                  "instead of the underscore character (`_`): " \
                  f"\n{banned_files_basename_with_dash}" \
                  "\nPlease replace `-` with `_` (the file naming convention used here) in " \
                  "those files name ." \
                  "\n\nNote that the following files are excepted, because in those cases the " \
                  "dash character (`-`) is part of the name of the tool the associated cheat " \
                  "sheet is about (and not used as a word separator): " \
                  f"\n{allowed_files_basename_with_dash}\n")

    assert not banned_files_basename_with_dash


def check() -> None:
    """
    Check markdown files name format.
    """
    log.debug("Checking file name...")
    check_files_name_syntax("./docs")
    log.info("Files name checked!")


if __name__ == "__main__":
    check()
