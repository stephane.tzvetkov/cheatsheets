#!/bin/sh

# Check 'avoid dotfile madness'
dotfile_files=$(
      /bin/grep -ri 'avoid dotfile madness' ../docs \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
check=""
for file in $dotfile_files
do
    check=$(
        /bin/grep -riL 'avoid_dotfile_madness.md#' --exclude="avoid_dotfile_madness.md" "$file"
    )
    if [ -n "$check" ]; then
        >&2 echo "❌ avoid dotfile madness references"
        >&2 echo ""
        >&2 echo "The following file speaks about how to avoid dotefile madness without "
        >&2 echo "referencing it. Please reference it clearly."
        >&2 echo "(e.g. see [Git cheat sheet](git.md#avoid-dotfile-madness))"
        >&2 echo ""
        >&2 echo "$check"
        >&2 echo ""
        return 1
    fi
done

echo "✅ avoid dotfile madness references"
