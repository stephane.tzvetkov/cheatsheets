#!/bin/sh

# Check every URL is enclosed by `<>` or `()`:
check=$( \
      /bin/grep -r "http[^ ]*" ../docs \
    | /bin/grep -v "(http[^ ]*)" \
    | /bin/grep -v "HTTPS[^ ]*" \
    | /bin/grep -v "<http[^ ]*>" \
    | /bin/grep -v "/var/log/httpd" \
    | /bin/grep -v "https_proxy" \
    | /bin/grep -v "git clone http[^ ]*" \
    | /bin/grep -v "wget http[^ ]*" \
    | /bin/grep -v "curl http[^ ]*" \
    | /bin/grep -v "export http[^ ]*" \
    | /bin/grep -v "linkchecker http[^ ]*" \
    | /bin/grep -v "\`http\`" \
    | /bin/grep -v "\`https\`" \
    | /bin/grep -v "xmlns\=\"http" \
    | /bin/grep -E -v ".md: +#" \
    | /bin/grep -E -v ".md:#" \
    | /bin/grep -E -v ".md: +>" \
    | /bin/grep -E -v ".md:>" \
    | /bin/grep -E -v '.md: +\$' \
    | /bin/grep -E -v '.md:\$' \
    | /bin/grep -E -v ".md: +\+" \
    | /bin/grep -E -v ".md:\+" \
    | /bin/grep -E -v ".md: +-" \
    | /bin/grep -E -v ".md:-" \
    | /bin/grep -E -v ".md: +~" \
    | /bin/grep -E -v ".md:~" \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ URLs"
    >&2 echo ""
    >&2 echo "The following files do contain URLs not enclosed by '< >' or '( )',"
    >&2 echo "please enclose those URLs."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ URLs"
